`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/01/20 17:37:01
// Design Name: 
// Module Name: sample_for4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sample_for4 #(
        parameter DATA_WIDTH = 32
    )
    (
        input wire clk,
        input wire rstn,
        
        input wire DST_VALID_1,
		input wire [DATA_WIDTH - 1:0] DST_DATA_1,
		output reg DST_ACK_1,
		input wire DST_FIN_1,
		output reg SRC_VALID_1,
		output reg [DATA_WIDTH - 1:0] SRC_DATA_1,
		input wire SRC_ACK_1,
		output reg SRC_FIN_1,
		
		input wire DST_VALID_2,
		input wire [DATA_WIDTH - 1:0] DST_DATA_2,
		output reg DST_ACK_2,
		input wire DST_FIN_2,
		output reg SRC_VALID_2,
		output reg [DATA_WIDTH - 1:0] SRC_DATA_2,
		input wire SRC_ACK_2,
		output reg SRC_FIN_2,
		
		input wire DST_VALID_3,
		input wire [DATA_WIDTH - 1:0] DST_DATA_3,
		output reg DST_ACK_3,
		input wire DST_FIN_3,
		output reg SRC_VALID_3,
		output reg [DATA_WIDTH - 1:0] SRC_DATA_3,
		input wire SRC_ACK_3,
		output reg SRC_FIN_3,
        
        input wire start,
        input wire [DATA_WIDTH - 1:0] din,
        output reg done,
        output reg [DATA_WIDTH - 1:0] dout
    )
    ;
    
    localparam
        CORE_NUM = 4,
        STATE_IDLE = 4'd0,
        STATE_WAIT = 4'd1,
        STATE_STORE = 4'd2,
        STATE_SEND = 4'd3,
        STATE_ACK_WAIT = 4'd4,
        STATE_FIN = 4'd5,
        STATE_CAL = 4'd6,
        STATE_DONE = 4'd7;
        
    reg [3:0] state;
    reg [CORE_NUM - 2:0] ack;
    reg [CORE_NUM - 2:0] fin;
    reg [CORE_NUM - 2:0] valid;
    wire ack_all;
    wire fin_all;
    
    assign ack_all = &ack;
    assign fin_all = &fin;
    
    // Buffer
    reg [DATA_WIDTH - 1:0] my_value;
    reg [(CORE_NUM - 1) * DATA_WIDTH - 1:0] others_value;
    
    // State Transition
    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin
            state <= STATE_IDLE;
        end
        else begin
            case(state) 
                STATE_IDLE: begin
                    state <= STATE_WAIT;
                end
                STATE_WAIT: begin
                    if(start) state <= STATE_STORE;
                end
                STATE_STORE: begin
                    if(din == 32'b11111111_11111111_11111111_11111111) state <= STATE_FIN;
                    else state <= STATE_SEND;
                end
                STATE_SEND: begin
                    state <= STATE_ACK_WAIT;
                end
                STATE_ACK_WAIT: begin
                    if(ack_all) state <= STATE_FIN; 
                end
                STATE_FIN: begin
                    if(fin_all) state <= STATE_CAL;
                end
                STATE_CAL: begin
                    state <= STATE_DONE;
                end
                STATE_DONE: begin
                
                end
            endcase
        end
    end    
    
    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin
            DST_ACK_1 <= 1'b0;
            SRC_VALID_1 <= 1'b0;
            SRC_DATA_1 <= {DATA_WIDTH{1'b0}};
            SRC_FIN_1 <= 1'b0;
            DST_ACK_2 <= 1'b0;
            SRC_VALID_2 <= 1'b0;
            SRC_DATA_2 <= {DATA_WIDTH{1'b0}};
            SRC_FIN_2 <= 1'b0;
            DST_ACK_3 <= 1'b0;
            SRC_VALID_3 <= 1'b0;
            SRC_DATA_3 <= {DATA_WIDTH{1'b0}};
            SRC_FIN_3 <= 1'b0;
            done <= 1'b0;
            dout <= {DATA_WIDTH{1'b0}};
            my_value <= {DATA_WIDTH{1'b0}};
            others_value <= {((CORE_NUM - 1) * DATA_WIDTH){1'b0}};
            ack <= {(CORE_NUM - 1){1'b0}};
            fin <= {(CORE_NUM - 1){1'b0}};
            valid <= {(CORE_NUM - 1){1'b0}};
        end
        else begin
            case(state)
                STATE_IDLE: begin
                    //DST_ACK_1 <= 1'b0;
                    SRC_VALID_1 <= 1'b0;
                    SRC_DATA_1 <= {DATA_WIDTH{1'b0}};
                    SRC_FIN_1 <= 1'b0;
                    //DST_ACK_2 <= 1'b0;
                    SRC_VALID_2 <= 1'b0;
                    SRC_DATA_2 <= {DATA_WIDTH{1'b0}};
                    SRC_FIN_2 <= 1'b0;
                    //DST_ACK_3 <= 1'b0;
                    SRC_VALID_3 <= 1'b0;
                    SRC_DATA_3 <= {DATA_WIDTH{1'b0}};
                    SRC_FIN_3 <= 1'b0;
                    done <= 1'b0;
                    dout <= {DATA_WIDTH{1'b0}};
                    my_value <= {DATA_WIDTH{1'b0}};
                    others_value <= {((CORE_NUM - 1) * DATA_WIDTH){1'b0}};
                    ack <= {(CORE_NUM - 1){1'b0}};
                    fin <= {(CORE_NUM - 1){1'b0}};
                    valid <= {(CORE_NUM - 1){1'b0}};
                end
                STATE_WAIT: begin
                    
                end
                STATE_STORE: begin
                    if(din == 32'b11111111_11111111_11111111_11111111) my_value <= {DATA_WIDTH{1'b0}};
                    else my_value <= din;
                end
                STATE_SEND: begin
                    SRC_VALID_1 <= 1'b1;
                    SRC_VALID_2 <= 1'b1;
                    SRC_VALID_3 <= 1'b1;
                    SRC_DATA_1 <= my_value;
                    SRC_DATA_2 <= my_value;
                    SRC_DATA_3 <= my_value;
                end
                STATE_ACK_WAIT: begin
                    SRC_VALID_1 <= 1'b0;
                    SRC_VALID_2 <= 1'b0;
                    SRC_VALID_3 <= 1'b0;
                    if(SRC_ACK_1) ack[0] <= 1'b1;
                    if(SRC_ACK_2) ack[1] <= 1'b1;
                    if(SRC_ACK_3) ack[2] <= 1'b1;
                end
                STATE_FIN: begin
                    SRC_FIN_1 <= 1'b1;
                    SRC_FIN_2 <= 1'b1;
                    SRC_FIN_3 <= 1'b1;
                end
                STATE_CAL: begin
                    dout <= my_value + others_value[DATA_WIDTH - 1 -: DATA_WIDTH] + others_value[2*DATA_WIDTH - 1 -: DATA_WIDTH] + others_value[3*DATA_WIDTH - 1 -: DATA_WIDTH];
                end
                STATE_DONE: begin
                    done <= 1'b1;
                end
            endcase
            
            // actions that are independent of state
            if(DST_VALID_1) begin
                others_value[DATA_WIDTH - 1 -: DATA_WIDTH] <= DST_DATA_1;
                valid[0] <= 1'b1;
            end
            else if(DST_FIN_1) begin
                fin[0] <= 1'b1;
                if(valid[0] == 0) begin
                    others_value[DATA_WIDTH - 1 -: DATA_WIDTH] <= {DATA_WIDTH{1'b0}};
                end
            end
            
            if(DST_VALID_2) begin
                others_value[2*DATA_WIDTH - 1 -: DATA_WIDTH] <= DST_DATA_2;
                valid[1] <= 1'b1;
            end
            else if(DST_FIN_2) begin
                fin[1] <= 1'b1;
                if(valid[1] == 0) begin
                    others_value[2*DATA_WIDTH - 1 -: DATA_WIDTH] <= {DATA_WIDTH{1'b0}};
                end
            end
            
            if(DST_VALID_3) begin
                others_value[3*DATA_WIDTH - 1 -: DATA_WIDTH] <= DST_DATA_3;
                valid[2] <= 1'b1;
            end
            else if(DST_FIN_3) begin
                fin[2] <= 1'b1;
                if(valid[2] == 0) begin
                    others_value[3*DATA_WIDTH - 1 -: DATA_WIDTH] <= {DATA_WIDTH{1'b0}};
                end
            end
            
            /*
            others_value[DATA_WIDTH - 1 -: DATA_WIDTH] <= (DST_VALID_1) ? DST_DATA_1 : others_value[DATA_WIDTH - 1 -: DATA_WIDTH];
            others_value[2*DATA_WIDTH - 1 -: DATA_WIDTH] <= (DST_VALID_2) ? DST_DATA_2 : others_value[2*DATA_WIDTH - 1 -: DATA_WIDTH];
            others_value[3*DATA_WIDTH - 1 -: DATA_WIDTH] <= (DST_VALID_3) ? DST_DATA_3 : others_value[3*DATA_WIDTH - 1 -: DATA_WIDTH];
            */
            DST_ACK_1 <= DST_VALID_1 ? 1'b1 : 1'b0;
            DST_ACK_2 <= DST_VALID_2 ? 1'b1 : 1'b0;
            DST_ACK_3 <= DST_VALID_3 ? 1'b1 : 1'b0;
            /*
            if(DST_FIN_1) fin[0] <= 1'b1;
            if(DST_FIN_2) fin[1] <= 1'b1;
            if(DST_FIN_3) fin[2] <= 1'b1;
            */
        end
    end
    
endmodule
