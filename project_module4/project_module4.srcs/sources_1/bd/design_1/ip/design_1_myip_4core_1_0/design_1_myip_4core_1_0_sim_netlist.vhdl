-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Tue Jan 21 16:47:33 2020
-- Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_myip_4core_1_0 -prefix
--               design_1_myip_4core_1_0_ design_1_myip_4core_0_1_sim_netlist.vhdl
-- Design      : design_1_myip_4core_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu9p-fsgd2104-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_myip_4core_1_0_sample_for4 is
  port (
    DST_ACK_1 : out STD_LOGIC;
    DST_ACK_2 : out STD_LOGIC;
    DST_ACK_3 : out STD_LOGIC;
    SRC_VALID_1 : out STD_LOGIC;
    SRC_FIN_1 : out STD_LOGIC;
    done : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_DATA_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_VALID_1 : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    DST_VALID_2 : in STD_LOGIC;
    DST_VALID_3 : in STD_LOGIC;
    SRC_VALID_1_reg_0 : in STD_LOGIC;
    SRC_FIN_1_reg_0 : in STD_LOGIC;
    done_reg_0 : in STD_LOGIC;
    \my_value_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_FIN_1 : in STD_LOGIC;
    DST_FIN_2 : in STD_LOGIC;
    DST_FIN_3 : in STD_LOGIC;
    DST_DATA_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_DATA_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_DATA_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[0]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    \axi_rdata_reg[1]\ : in STD_LOGIC;
    \axi_rdata_reg[2]\ : in STD_LOGIC;
    \axi_rdata_reg[3]\ : in STD_LOGIC;
    \axi_rdata_reg[4]\ : in STD_LOGIC;
    \axi_rdata_reg[5]\ : in STD_LOGIC;
    \axi_rdata_reg[6]\ : in STD_LOGIC;
    \axi_rdata_reg[7]\ : in STD_LOGIC;
    \axi_rdata_reg[8]\ : in STD_LOGIC;
    \axi_rdata_reg[9]\ : in STD_LOGIC;
    \axi_rdata_reg[10]\ : in STD_LOGIC;
    \axi_rdata_reg[11]\ : in STD_LOGIC;
    \axi_rdata_reg[12]\ : in STD_LOGIC;
    \axi_rdata_reg[13]\ : in STD_LOGIC;
    \axi_rdata_reg[14]\ : in STD_LOGIC;
    \axi_rdata_reg[15]\ : in STD_LOGIC;
    \axi_rdata_reg[16]\ : in STD_LOGIC;
    \axi_rdata_reg[17]\ : in STD_LOGIC;
    \axi_rdata_reg[18]\ : in STD_LOGIC;
    \axi_rdata_reg[19]\ : in STD_LOGIC;
    \axi_rdata_reg[20]\ : in STD_LOGIC;
    \axi_rdata_reg[21]\ : in STD_LOGIC;
    \axi_rdata_reg[22]\ : in STD_LOGIC;
    \axi_rdata_reg[23]\ : in STD_LOGIC;
    \axi_rdata_reg[24]\ : in STD_LOGIC;
    \axi_rdata_reg[25]\ : in STD_LOGIC;
    \axi_rdata_reg[26]\ : in STD_LOGIC;
    \axi_rdata_reg[27]\ : in STD_LOGIC;
    \axi_rdata_reg[28]\ : in STD_LOGIC;
    \axi_rdata_reg[29]\ : in STD_LOGIC;
    \axi_rdata_reg[30]\ : in STD_LOGIC;
    \axi_rdata_reg[31]_0\ : in STD_LOGIC;
    SRC_ACK_3 : in STD_LOGIC;
    SRC_ACK_2 : in STD_LOGIC;
    SRC_ACK_1 : in STD_LOGIC
  );
end design_1_myip_4core_1_0_sample_for4;

architecture STRUCTURE of design_1_myip_4core_1_0_sample_for4 is
  signal \FSM_onehot_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[7]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[7]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[7]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[6]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \SRC_DATA_1[0]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[10]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[11]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[12]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[13]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[14]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[15]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[16]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[17]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[18]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[19]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[1]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[20]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[21]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[22]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[23]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[24]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[25]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[26]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[27]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[28]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[29]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[2]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[30]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[31]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[31]_i_2_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[3]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[4]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[5]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[6]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[7]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[8]_i_1_n_0\ : STD_LOGIC;
  signal \SRC_DATA_1[9]_i_1_n_0\ : STD_LOGIC;
  signal \ack[0]_i_1_n_0\ : STD_LOGIC;
  signal \ack[1]_i_1_n_0\ : STD_LOGIC;
  signal \ack[2]_i_1_n_0\ : STD_LOGIC;
  signal \ack_reg_n_0_[0]\ : STD_LOGIC;
  signal \ack_reg_n_0_[1]\ : STD_LOGIC;
  signal \ack_reg_n_0_[2]\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \^done\ : STD_LOGIC;
  signal dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \dout0__2_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_13_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_14_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_15_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_16_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_17_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_18_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_19_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_20_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_21_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_22_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_23_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_24_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_1\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_2\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_3\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_4\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_5\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_6\ : STD_LOGIC;
  signal \dout0__2_carry__0_n_7\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_13_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_14_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_15_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_16_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_17_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_18_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_19_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_20_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_21_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_22_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_23_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_24_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_1\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_2\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_3\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_4\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_5\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_6\ : STD_LOGIC;
  signal \dout0__2_carry__1_n_7\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_15_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_16_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_17_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_18_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_19_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_20_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_21_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_22_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_23_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_24_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_1\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_2\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_3\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_4\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_5\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_6\ : STD_LOGIC;
  signal \dout0__2_carry__2_n_7\ : STD_LOGIC;
  signal \dout0__2_carry_i_10_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_11_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_12_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_13_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_14_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_15_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_16_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_17_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_18_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_19_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_1_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_20_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_21_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_2_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_3_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_4_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_5_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_6_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_7_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_8_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_i_9_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_n_0\ : STD_LOGIC;
  signal \dout0__2_carry_n_1\ : STD_LOGIC;
  signal \dout0__2_carry_n_2\ : STD_LOGIC;
  signal \dout0__2_carry_n_3\ : STD_LOGIC;
  signal \dout0__2_carry_n_4\ : STD_LOGIC;
  signal \dout0__2_carry_n_5\ : STD_LOGIC;
  signal \dout0__2_carry_n_6\ : STD_LOGIC;
  signal \dout0__2_carry_n_7\ : STD_LOGIC;
  signal \dout[0]_i_1_n_0\ : STD_LOGIC;
  signal \dout[10]_i_1_n_0\ : STD_LOGIC;
  signal \dout[11]_i_1_n_0\ : STD_LOGIC;
  signal \dout[12]_i_1_n_0\ : STD_LOGIC;
  signal \dout[13]_i_1_n_0\ : STD_LOGIC;
  signal \dout[14]_i_1_n_0\ : STD_LOGIC;
  signal \dout[15]_i_1_n_0\ : STD_LOGIC;
  signal \dout[16]_i_1_n_0\ : STD_LOGIC;
  signal \dout[17]_i_1_n_0\ : STD_LOGIC;
  signal \dout[18]_i_1_n_0\ : STD_LOGIC;
  signal \dout[19]_i_1_n_0\ : STD_LOGIC;
  signal \dout[1]_i_1_n_0\ : STD_LOGIC;
  signal \dout[20]_i_1_n_0\ : STD_LOGIC;
  signal \dout[21]_i_1_n_0\ : STD_LOGIC;
  signal \dout[22]_i_1_n_0\ : STD_LOGIC;
  signal \dout[23]_i_1_n_0\ : STD_LOGIC;
  signal \dout[24]_i_1_n_0\ : STD_LOGIC;
  signal \dout[25]_i_1_n_0\ : STD_LOGIC;
  signal \dout[26]_i_1_n_0\ : STD_LOGIC;
  signal \dout[27]_i_1_n_0\ : STD_LOGIC;
  signal \dout[28]_i_1_n_0\ : STD_LOGIC;
  signal \dout[29]_i_1_n_0\ : STD_LOGIC;
  signal \dout[2]_i_1_n_0\ : STD_LOGIC;
  signal \dout[30]_i_1_n_0\ : STD_LOGIC;
  signal \dout[31]_i_1_n_0\ : STD_LOGIC;
  signal \dout[31]_i_2_n_0\ : STD_LOGIC;
  signal \dout[3]_i_1_n_0\ : STD_LOGIC;
  signal \dout[4]_i_1_n_0\ : STD_LOGIC;
  signal \dout[5]_i_1_n_0\ : STD_LOGIC;
  signal \dout[6]_i_1_n_0\ : STD_LOGIC;
  signal \dout[7]_i_1_n_0\ : STD_LOGIC;
  signal \dout[8]_i_1_n_0\ : STD_LOGIC;
  signal \dout[9]_i_1_n_0\ : STD_LOGIC;
  signal fin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \fin[0]_i_1_n_0\ : STD_LOGIC;
  signal \fin[1]_i_1_n_0\ : STD_LOGIC;
  signal \fin[2]_i_1_n_0\ : STD_LOGIC;
  signal in7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \my_value[0]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_2_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_3_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_4_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_5_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_6_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_7_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_8_n_0\ : STD_LOGIC;
  signal \my_value[0]_i_9_n_0\ : STD_LOGIC;
  signal \my_value[10]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[11]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[12]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[13]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[14]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[15]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[16]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[17]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[18]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[19]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[1]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[20]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[21]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[22]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[23]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[24]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[25]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[26]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[27]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[28]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[29]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[2]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[30]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[31]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[31]_i_2_n_0\ : STD_LOGIC;
  signal \my_value[3]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[4]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[5]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[6]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[7]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[8]_i_1_n_0\ : STD_LOGIC;
  signal \my_value[9]_i_1_n_0\ : STD_LOGIC;
  signal \my_value_reg_n_0_[0]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[10]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[11]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[12]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[13]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[14]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[15]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[16]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[17]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[18]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[19]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[1]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[20]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[21]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[22]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[23]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[24]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[25]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[26]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[27]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[28]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[29]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[2]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[30]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[31]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[3]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[4]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[5]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[6]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[7]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[8]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[9]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[0]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[10]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[11]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[12]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[13]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[14]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[15]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[16]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[17]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[18]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[19]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[1]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[20]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[21]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[22]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[23]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[24]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[25]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[26]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[27]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[28]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[29]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[2]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[30]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[31]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[3]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[4]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[5]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[6]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[7]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[8]\ : STD_LOGIC;
  signal \others_value_reg_n_0_[9]\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_0_in0_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_6_out : STD_LOGIC_VECTOR ( 95 downto 31 );
  signal p_6_out0_in : STD_LOGIC_VECTOR ( 95 downto 0 );
  signal \valid[0]_i_1_n_0\ : STD_LOGIC;
  signal \valid[1]_i_1_n_0\ : STD_LOGIC;
  signal \valid[2]_i_1_n_0\ : STD_LOGIC;
  signal \valid_reg_n_0_[0]\ : STD_LOGIC;
  signal \valid_reg_n_0_[1]\ : STD_LOGIC;
  signal \valid_reg_n_0_[2]\ : STD_LOGIC;
  signal \NLW_dout0__2_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_6\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_7\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_8\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_9\ : label is "soft_lutpair6";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[4]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[5]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[6]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[7]\ : label is "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000";
  attribute SOFT_HLUTNM of \SRC_DATA_1[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \SRC_DATA_1[10]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \SRC_DATA_1[11]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \SRC_DATA_1[12]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \SRC_DATA_1[13]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \SRC_DATA_1[14]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \SRC_DATA_1[15]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \SRC_DATA_1[16]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \SRC_DATA_1[17]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \SRC_DATA_1[18]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \SRC_DATA_1[19]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \SRC_DATA_1[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \SRC_DATA_1[20]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \SRC_DATA_1[21]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \SRC_DATA_1[22]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \SRC_DATA_1[23]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \SRC_DATA_1[24]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \SRC_DATA_1[25]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \SRC_DATA_1[26]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \SRC_DATA_1[27]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \SRC_DATA_1[28]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \SRC_DATA_1[29]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \SRC_DATA_1[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \SRC_DATA_1[31]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \SRC_DATA_1[3]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \SRC_DATA_1[4]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \SRC_DATA_1[5]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \SRC_DATA_1[6]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \SRC_DATA_1[7]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \SRC_DATA_1[8]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \SRC_DATA_1[9]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \dout0__2_carry__2_i_16\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dout0__2_carry__2_i_22\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \dout0__2_carry__2_i_23\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \dout[0]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \dout[10]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \dout[11]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \dout[12]_i_1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \dout[13]_i_1\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \dout[14]_i_1\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \dout[15]_i_1\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \dout[16]_i_1\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \dout[17]_i_1\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \dout[18]_i_1\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \dout[19]_i_1\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \dout[1]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \dout[20]_i_1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \dout[21]_i_1\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \dout[22]_i_1\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \dout[23]_i_1\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \dout[24]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \dout[25]_i_1\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \dout[26]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \dout[27]_i_1\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \dout[28]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \dout[29]_i_1\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \dout[2]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \dout[30]_i_1\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \dout[31]_i_2\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \dout[3]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \dout[4]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \dout[5]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \dout[6]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \dout[7]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \dout[8]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \dout[9]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \fin[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \fin[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \fin[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \my_value[0]_i_5\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \my_value[0]_i_6\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \my_value[0]_i_8\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \my_value[0]_i_9\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \others_value[0]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \others_value[10]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \others_value[11]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \others_value[12]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \others_value[13]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \others_value[14]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \others_value[15]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \others_value[16]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \others_value[17]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \others_value[18]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \others_value[19]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \others_value[1]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \others_value[20]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \others_value[21]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \others_value[22]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \others_value[23]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \others_value[24]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \others_value[25]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \others_value[26]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \others_value[27]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \others_value[28]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \others_value[29]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \others_value[2]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \others_value[30]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \others_value[31]_i_2\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \others_value[32]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \others_value[33]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \others_value[34]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \others_value[35]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \others_value[36]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \others_value[37]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \others_value[38]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \others_value[39]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \others_value[3]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \others_value[40]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \others_value[41]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \others_value[42]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \others_value[43]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \others_value[44]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \others_value[45]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \others_value[46]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \others_value[47]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \others_value[48]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \others_value[49]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \others_value[4]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \others_value[50]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \others_value[51]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \others_value[52]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \others_value[53]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \others_value[54]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \others_value[55]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \others_value[56]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \others_value[57]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \others_value[58]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \others_value[59]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \others_value[5]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \others_value[60]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \others_value[61]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \others_value[62]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \others_value[63]_i_2\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \others_value[64]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \others_value[65]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \others_value[66]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \others_value[67]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \others_value[68]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \others_value[69]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \others_value[6]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \others_value[70]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \others_value[71]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \others_value[72]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \others_value[73]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \others_value[74]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \others_value[75]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \others_value[76]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \others_value[77]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \others_value[78]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \others_value[79]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \others_value[7]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \others_value[80]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \others_value[81]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \others_value[82]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \others_value[83]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \others_value[84]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \others_value[85]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \others_value[86]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \others_value[87]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \others_value[88]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \others_value[89]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \others_value[8]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \others_value[90]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \others_value[91]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \others_value[92]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \others_value[93]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \others_value[94]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \others_value[95]_i_2\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \others_value[9]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \valid[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \valid[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \valid[2]_i_1\ : label is "soft_lutpair2";
begin
  Q(4 downto 0) <= \^q\(4 downto 0);
  done <= \^done\;
DST_ACK_1_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => DST_VALID_1,
      Q => DST_ACK_1
    );
DST_ACK_2_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => DST_VALID_2,
      Q => DST_ACK_2
    );
DST_ACK_3_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => DST_VALID_3,
      Q => DST_ACK_3
    );
\FSM_onehot_state[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_5_n_0\,
      I2 => \FSM_onehot_state[5]_i_4_n_0\,
      I3 => \FSM_onehot_state[5]_i_3_n_0\,
      I4 => \FSM_onehot_state[5]_i_2_n_0\,
      O => \FSM_onehot_state[3]_i_1_n_0\
    );
\FSM_onehot_state[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAABAAAAAAAA"
    )
        port map (
      I0 => \^q\(2),
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \FSM_onehot_state_reg_n_0_[2]\,
      O => \FSM_onehot_state[5]_i_1_n_0\
    );
\FSM_onehot_state[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(21),
      I1 => \my_value_reg[31]_0\(20),
      I2 => \my_value_reg[31]_0\(23),
      I3 => \my_value_reg[31]_0\(22),
      I4 => \FSM_onehot_state[5]_i_6_n_0\,
      O => \FSM_onehot_state[5]_i_2_n_0\
    );
\FSM_onehot_state[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(29),
      I1 => \my_value_reg[31]_0\(28),
      I2 => \my_value_reg[31]_0\(30),
      I3 => \my_value_reg[31]_0\(31),
      I4 => \FSM_onehot_state[5]_i_7_n_0\,
      O => \FSM_onehot_state[5]_i_3_n_0\
    );
\FSM_onehot_state[5]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(5),
      I1 => \my_value_reg[31]_0\(4),
      I2 => \my_value_reg[31]_0\(7),
      I3 => \my_value_reg[31]_0\(6),
      I4 => \FSM_onehot_state[5]_i_8_n_0\,
      O => \FSM_onehot_state[5]_i_4_n_0\
    );
\FSM_onehot_state[5]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(13),
      I1 => \my_value_reg[31]_0\(12),
      I2 => \my_value_reg[31]_0\(15),
      I3 => \my_value_reg[31]_0\(14),
      I4 => \FSM_onehot_state[5]_i_9_n_0\,
      O => \FSM_onehot_state[5]_i_5_n_0\
    );
\FSM_onehot_state[5]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(18),
      I1 => \my_value_reg[31]_0\(19),
      I2 => \my_value_reg[31]_0\(16),
      I3 => \my_value_reg[31]_0\(17),
      O => \FSM_onehot_state[5]_i_6_n_0\
    );
\FSM_onehot_state[5]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(26),
      I1 => \my_value_reg[31]_0\(27),
      I2 => \my_value_reg[31]_0\(24),
      I3 => \my_value_reg[31]_0\(25),
      O => \FSM_onehot_state[5]_i_7_n_0\
    );
\FSM_onehot_state[5]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(2),
      I1 => \my_value_reg[31]_0\(3),
      I2 => \my_value_reg[31]_0\(0),
      I3 => \my_value_reg[31]_0\(1),
      O => \FSM_onehot_state[5]_i_8_n_0\
    );
\FSM_onehot_state[5]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(10),
      I1 => \my_value_reg[31]_0\(11),
      I2 => \my_value_reg[31]_0\(8),
      I3 => \my_value_reg[31]_0\(9),
      O => \FSM_onehot_state[5]_i_9_n_0\
    );
\FSM_onehot_state[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \FSM_onehot_state_reg_n_0_[2]\,
      I3 => \FSM_onehot_state[7]_i_2_n_0\,
      I4 => \FSM_onehot_state[7]_i_3_n_0\,
      O => \FSM_onehot_state[7]_i_1_n_0\
    );
\FSM_onehot_state[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF800080008000"
    )
        port map (
      I0 => \ack_reg_n_0_[0]\,
      I1 => \ack_reg_n_0_[2]\,
      I2 => \ack_reg_n_0_[1]\,
      I3 => \^q\(2),
      I4 => \axi_rdata_reg[31]\(0),
      I5 => \FSM_onehot_state_reg_n_0_[1]\,
      O => \FSM_onehot_state[7]_i_2_n_0\
    );
\FSM_onehot_state[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EAAAAAAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => fin(0),
      I2 => fin(2),
      I3 => fin(1),
      I4 => \^q\(3),
      O => \FSM_onehot_state[7]_i_3_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      D => '0',
      PRE => SR(0),
      Q => \^q\(0)
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \^q\(0),
      Q => \FSM_onehot_state_reg_n_0_[1]\
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \FSM_onehot_state_reg_n_0_[1]\,
      Q => \FSM_onehot_state_reg_n_0_[2]\
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \FSM_onehot_state[3]_i_1_n_0\,
      Q => \^q\(1)
    );
\FSM_onehot_state_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \^q\(1),
      Q => \^q\(2)
    );
\FSM_onehot_state_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \FSM_onehot_state[5]_i_1_n_0\,
      Q => \^q\(3)
    );
\FSM_onehot_state_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \^q\(3),
      Q => \FSM_onehot_state_reg_n_0_[6]\
    );
\FSM_onehot_state_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \FSM_onehot_state[7]_i_1_n_0\,
      CLR => SR(0),
      D => \FSM_onehot_state_reg_n_0_[6]\,
      Q => \^q\(4)
    );
\SRC_DATA_1[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[0]\,
      O => \SRC_DATA_1[0]_i_1_n_0\
    );
\SRC_DATA_1[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[10]\,
      O => \SRC_DATA_1[10]_i_1_n_0\
    );
\SRC_DATA_1[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[11]\,
      O => \SRC_DATA_1[11]_i_1_n_0\
    );
\SRC_DATA_1[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[12]\,
      O => \SRC_DATA_1[12]_i_1_n_0\
    );
\SRC_DATA_1[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[13]\,
      O => \SRC_DATA_1[13]_i_1_n_0\
    );
\SRC_DATA_1[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[14]\,
      O => \SRC_DATA_1[14]_i_1_n_0\
    );
\SRC_DATA_1[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[15]\,
      O => \SRC_DATA_1[15]_i_1_n_0\
    );
\SRC_DATA_1[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[16]\,
      O => \SRC_DATA_1[16]_i_1_n_0\
    );
\SRC_DATA_1[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[17]\,
      O => \SRC_DATA_1[17]_i_1_n_0\
    );
\SRC_DATA_1[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[18]\,
      O => \SRC_DATA_1[18]_i_1_n_0\
    );
\SRC_DATA_1[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[19]\,
      O => \SRC_DATA_1[19]_i_1_n_0\
    );
\SRC_DATA_1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[1]\,
      O => \SRC_DATA_1[1]_i_1_n_0\
    );
\SRC_DATA_1[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[20]\,
      O => \SRC_DATA_1[20]_i_1_n_0\
    );
\SRC_DATA_1[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[21]\,
      O => \SRC_DATA_1[21]_i_1_n_0\
    );
\SRC_DATA_1[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[22]\,
      O => \SRC_DATA_1[22]_i_1_n_0\
    );
\SRC_DATA_1[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[23]\,
      O => \SRC_DATA_1[23]_i_1_n_0\
    );
\SRC_DATA_1[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[24]\,
      O => \SRC_DATA_1[24]_i_1_n_0\
    );
\SRC_DATA_1[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[25]\,
      O => \SRC_DATA_1[25]_i_1_n_0\
    );
\SRC_DATA_1[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[26]\,
      O => \SRC_DATA_1[26]_i_1_n_0\
    );
\SRC_DATA_1[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[27]\,
      O => \SRC_DATA_1[27]_i_1_n_0\
    );
\SRC_DATA_1[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[28]\,
      O => \SRC_DATA_1[28]_i_1_n_0\
    );
\SRC_DATA_1[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[29]\,
      O => \SRC_DATA_1[29]_i_1_n_0\
    );
\SRC_DATA_1[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[2]\,
      O => \SRC_DATA_1[2]_i_1_n_0\
    );
\SRC_DATA_1[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[30]\,
      O => \SRC_DATA_1[30]_i_1_n_0\
    );
\SRC_DATA_1[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \SRC_DATA_1[31]_i_1_n_0\
    );
\SRC_DATA_1[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[31]\,
      O => \SRC_DATA_1[31]_i_2_n_0\
    );
\SRC_DATA_1[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[3]\,
      O => \SRC_DATA_1[3]_i_1_n_0\
    );
\SRC_DATA_1[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[4]\,
      O => \SRC_DATA_1[4]_i_1_n_0\
    );
\SRC_DATA_1[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[5]\,
      O => \SRC_DATA_1[5]_i_1_n_0\
    );
\SRC_DATA_1[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[6]\,
      O => \SRC_DATA_1[6]_i_1_n_0\
    );
\SRC_DATA_1[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[7]\,
      O => \SRC_DATA_1[7]_i_1_n_0\
    );
\SRC_DATA_1[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[8]\,
      O => \SRC_DATA_1[8]_i_1_n_0\
    );
\SRC_DATA_1[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \my_value_reg_n_0_[9]\,
      O => \SRC_DATA_1[9]_i_1_n_0\
    );
\SRC_DATA_1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[0]_i_1_n_0\,
      Q => SRC_DATA_1(0)
    );
\SRC_DATA_1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[10]_i_1_n_0\,
      Q => SRC_DATA_1(10)
    );
\SRC_DATA_1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[11]_i_1_n_0\,
      Q => SRC_DATA_1(11)
    );
\SRC_DATA_1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[12]_i_1_n_0\,
      Q => SRC_DATA_1(12)
    );
\SRC_DATA_1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[13]_i_1_n_0\,
      Q => SRC_DATA_1(13)
    );
\SRC_DATA_1_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[14]_i_1_n_0\,
      Q => SRC_DATA_1(14)
    );
\SRC_DATA_1_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[15]_i_1_n_0\,
      Q => SRC_DATA_1(15)
    );
\SRC_DATA_1_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[16]_i_1_n_0\,
      Q => SRC_DATA_1(16)
    );
\SRC_DATA_1_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[17]_i_1_n_0\,
      Q => SRC_DATA_1(17)
    );
\SRC_DATA_1_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[18]_i_1_n_0\,
      Q => SRC_DATA_1(18)
    );
\SRC_DATA_1_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[19]_i_1_n_0\,
      Q => SRC_DATA_1(19)
    );
\SRC_DATA_1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[1]_i_1_n_0\,
      Q => SRC_DATA_1(1)
    );
\SRC_DATA_1_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[20]_i_1_n_0\,
      Q => SRC_DATA_1(20)
    );
\SRC_DATA_1_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[21]_i_1_n_0\,
      Q => SRC_DATA_1(21)
    );
\SRC_DATA_1_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[22]_i_1_n_0\,
      Q => SRC_DATA_1(22)
    );
\SRC_DATA_1_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[23]_i_1_n_0\,
      Q => SRC_DATA_1(23)
    );
\SRC_DATA_1_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[24]_i_1_n_0\,
      Q => SRC_DATA_1(24)
    );
\SRC_DATA_1_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[25]_i_1_n_0\,
      Q => SRC_DATA_1(25)
    );
\SRC_DATA_1_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[26]_i_1_n_0\,
      Q => SRC_DATA_1(26)
    );
\SRC_DATA_1_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[27]_i_1_n_0\,
      Q => SRC_DATA_1(27)
    );
\SRC_DATA_1_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[28]_i_1_n_0\,
      Q => SRC_DATA_1(28)
    );
\SRC_DATA_1_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[29]_i_1_n_0\,
      Q => SRC_DATA_1(29)
    );
\SRC_DATA_1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[2]_i_1_n_0\,
      Q => SRC_DATA_1(2)
    );
\SRC_DATA_1_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[30]_i_1_n_0\,
      Q => SRC_DATA_1(30)
    );
\SRC_DATA_1_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[31]_i_2_n_0\,
      Q => SRC_DATA_1(31)
    );
\SRC_DATA_1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[3]_i_1_n_0\,
      Q => SRC_DATA_1(3)
    );
\SRC_DATA_1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[4]_i_1_n_0\,
      Q => SRC_DATA_1(4)
    );
\SRC_DATA_1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[5]_i_1_n_0\,
      Q => SRC_DATA_1(5)
    );
\SRC_DATA_1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[6]_i_1_n_0\,
      Q => SRC_DATA_1(6)
    );
\SRC_DATA_1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[7]_i_1_n_0\,
      Q => SRC_DATA_1(7)
    );
\SRC_DATA_1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[8]_i_1_n_0\,
      Q => SRC_DATA_1(8)
    );
\SRC_DATA_1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA_1[31]_i_1_n_0\,
      CLR => SR(0),
      D => \SRC_DATA_1[9]_i_1_n_0\,
      Q => SRC_DATA_1(9)
    );
SRC_FIN_1_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => SRC_FIN_1_reg_0,
      Q => SRC_FIN_1
    );
SRC_VALID_1_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => SRC_VALID_1_reg_0,
      Q => SRC_VALID_1
    );
\ack[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D5C0"
    )
        port map (
      I0 => \^q\(0),
      I1 => SRC_ACK_1,
      I2 => \^q\(2),
      I3 => \ack_reg_n_0_[0]\,
      O => \ack[0]_i_1_n_0\
    );
\ack[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D5C0"
    )
        port map (
      I0 => \^q\(0),
      I1 => SRC_ACK_2,
      I2 => \^q\(2),
      I3 => \ack_reg_n_0_[1]\,
      O => \ack[1]_i_1_n_0\
    );
\ack[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D5C0"
    )
        port map (
      I0 => \^q\(0),
      I1 => SRC_ACK_3,
      I2 => \^q\(2),
      I3 => \ack_reg_n_0_[2]\,
      O => \ack[2]_i_1_n_0\
    );
\ack_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \ack[0]_i_1_n_0\,
      Q => \ack_reg_n_0_[0]\
    );
\ack_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \ack[1]_i_1_n_0\,
      Q => \ack_reg_n_0_[1]\
    );
\ack_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \ack[2]_i_1_n_0\,
      Q => \ack_reg_n_0_[2]\
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^done\,
      I1 => dout(0),
      I2 => \axi_rdata_reg[0]\(1),
      I3 => \my_value_reg[31]_0\(0),
      I4 => \axi_rdata_reg[0]\(0),
      I5 => \axi_rdata_reg[31]\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(10),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(10),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(11),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(11),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(12),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(12),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(13),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(13),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(14),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(14),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(15),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(15),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(16),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(16),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(17),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(17),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(18),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(18),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(19),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(19),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(1),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(1),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(20),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(20),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(21),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(21),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(22),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(22),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(23),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(23),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(24),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(24),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(25),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(25),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(26),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(26),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(27),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(27),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(28),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(28),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(29),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(29),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(2),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(2),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(30),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(30),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(31),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(31),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(31),
      O => \axi_rdata[31]_i_2_n_0\
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(3),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(3),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(4),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(4),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(5),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(5),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(6),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(6),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(7),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(7),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(8),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(8),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(9),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \my_value_reg[31]_0\(9),
      I3 => \axi_rdata_reg[0]\(0),
      I4 => \axi_rdata_reg[31]\(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => \axi_rdata_reg[0]_0\,
      O => D(0),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[10]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => \axi_rdata_reg[10]\,
      O => D(10),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[11]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => \axi_rdata_reg[11]\,
      O => D(11),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[12]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => \axi_rdata_reg[12]\,
      O => D(12),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[13]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => \axi_rdata_reg[13]\,
      O => D(13),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[14]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => \axi_rdata_reg[14]\,
      O => D(14),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[15]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata_reg[15]\,
      O => D(15),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[16]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => \axi_rdata_reg[16]\,
      O => D(16),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[17]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => \axi_rdata_reg[17]\,
      O => D(17),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[18]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => \axi_rdata_reg[18]\,
      O => D(18),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[19]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => \axi_rdata_reg[19]\,
      O => D(19),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \axi_rdata_reg[1]\,
      O => D(1),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[20]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => \axi_rdata_reg[20]\,
      O => D(20),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[21]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => \axi_rdata_reg[21]\,
      O => D(21),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[22]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => \axi_rdata_reg[22]\,
      O => D(22),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[23]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => \axi_rdata_reg[23]\,
      O => D(23),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[24]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => \axi_rdata_reg[24]\,
      O => D(24),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[25]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => \axi_rdata_reg[25]\,
      O => D(25),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[26]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => \axi_rdata_reg[26]\,
      O => D(26),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[27]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => \axi_rdata_reg[27]\,
      O => D(27),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[28]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => \axi_rdata_reg[28]\,
      O => D(28),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[29]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => \axi_rdata_reg[29]\,
      O => D(29),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => \axi_rdata_reg[2]\,
      O => D(2),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[30]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => \axi_rdata_reg[30]\,
      O => D(30),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[31]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_2_n_0\,
      I1 => \axi_rdata_reg[31]_0\,
      O => D(31),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[3]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => \axi_rdata_reg[3]\,
      O => D(3),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \axi_rdata_reg[4]\,
      O => D(4),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[5]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => \axi_rdata_reg[5]\,
      O => D(5),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[6]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => \axi_rdata_reg[6]\,
      O => D(6),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[7]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => \axi_rdata_reg[7]\,
      O => D(7),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[8]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => \axi_rdata_reg[8]\,
      O => D(8),
      S => \axi_rdata_reg[0]\(2)
    );
\axi_rdata_reg[9]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => \axi_rdata_reg[9]\,
      O => D(9),
      S => \axi_rdata_reg[0]\(2)
    );
done_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => done_reg_0,
      Q => \^done\
    );
\dout0__2_carry\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \dout0__2_carry_n_0\,
      CO(6) => \dout0__2_carry_n_1\,
      CO(5) => \dout0__2_carry_n_2\,
      CO(4) => \dout0__2_carry_n_3\,
      CO(3) => \dout0__2_carry_n_4\,
      CO(2) => \dout0__2_carry_n_5\,
      CO(1) => \dout0__2_carry_n_6\,
      CO(0) => \dout0__2_carry_n_7\,
      DI(7) => \dout0__2_carry_i_1_n_0\,
      DI(6) => \dout0__2_carry_i_2_n_0\,
      DI(5) => \dout0__2_carry_i_3_n_0\,
      DI(4) => \dout0__2_carry_i_4_n_0\,
      DI(3) => \dout0__2_carry_i_5_n_0\,
      DI(2) => \dout0__2_carry_i_6_n_0\,
      DI(1) => \dout0__2_carry_i_7_n_0\,
      DI(0) => \my_value_reg_n_0_[0]\,
      O(7 downto 0) => in7(7 downto 0),
      S(7) => \dout0__2_carry_i_8_n_0\,
      S(6) => \dout0__2_carry_i_9_n_0\,
      S(5) => \dout0__2_carry_i_10_n_0\,
      S(4) => \dout0__2_carry_i_11_n_0\,
      S(3) => \dout0__2_carry_i_12_n_0\,
      S(2) => \dout0__2_carry_i_13_n_0\,
      S(1) => \dout0__2_carry_i_14_n_0\,
      S(0) => \dout0__2_carry_i_15_n_0\
    );
\dout0__2_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => \dout0__2_carry_n_0\,
      CI_TOP => '0',
      CO(7) => \dout0__2_carry__0_n_0\,
      CO(6) => \dout0__2_carry__0_n_1\,
      CO(5) => \dout0__2_carry__0_n_2\,
      CO(4) => \dout0__2_carry__0_n_3\,
      CO(3) => \dout0__2_carry__0_n_4\,
      CO(2) => \dout0__2_carry__0_n_5\,
      CO(1) => \dout0__2_carry__0_n_6\,
      CO(0) => \dout0__2_carry__0_n_7\,
      DI(7) => \dout0__2_carry__0_i_1_n_0\,
      DI(6) => \dout0__2_carry__0_i_2_n_0\,
      DI(5) => \dout0__2_carry__0_i_3_n_0\,
      DI(4) => \dout0__2_carry__0_i_4_n_0\,
      DI(3) => \dout0__2_carry__0_i_5_n_0\,
      DI(2) => \dout0__2_carry__0_i_6_n_0\,
      DI(1) => \dout0__2_carry__0_i_7_n_0\,
      DI(0) => \dout0__2_carry__0_i_8_n_0\,
      O(7 downto 0) => in7(15 downto 8),
      S(7) => \dout0__2_carry__0_i_9_n_0\,
      S(6) => \dout0__2_carry__0_i_10_n_0\,
      S(5) => \dout0__2_carry__0_i_11_n_0\,
      S(4) => \dout0__2_carry__0_i_12_n_0\,
      S(3) => \dout0__2_carry__0_i_13_n_0\,
      S(2) => \dout0__2_carry__0_i_14_n_0\,
      S(1) => \dout0__2_carry__0_i_15_n_0\,
      S(0) => \dout0__2_carry__0_i_16_n_0\
    );
\dout0__2_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[14]\,
      I1 => \dout0__2_carry__0_i_17_n_0\,
      I2 => p_0_in0_in(13),
      I3 => p_0_in(13),
      I4 => \others_value_reg_n_0_[13]\,
      O => \dout0__2_carry__0_i_1_n_0\
    );
\dout0__2_carry__0_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_2_n_0\,
      I1 => \dout0__2_carry__0_i_17_n_0\,
      I2 => \my_value_reg_n_0_[14]\,
      I3 => \others_value_reg_n_0_[13]\,
      I4 => p_0_in(13),
      I5 => p_0_in0_in(13),
      O => \dout0__2_carry__0_i_10_n_0\
    );
\dout0__2_carry__0_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_3_n_0\,
      I1 => \dout0__2_carry__0_i_18_n_0\,
      I2 => \my_value_reg_n_0_[13]\,
      I3 => \others_value_reg_n_0_[12]\,
      I4 => p_0_in(12),
      I5 => p_0_in0_in(12),
      O => \dout0__2_carry__0_i_11_n_0\
    );
\dout0__2_carry__0_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_4_n_0\,
      I1 => \dout0__2_carry__0_i_19_n_0\,
      I2 => \my_value_reg_n_0_[12]\,
      I3 => \others_value_reg_n_0_[11]\,
      I4 => p_0_in(11),
      I5 => p_0_in0_in(11),
      O => \dout0__2_carry__0_i_12_n_0\
    );
\dout0__2_carry__0_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_5_n_0\,
      I1 => \dout0__2_carry__0_i_20_n_0\,
      I2 => \my_value_reg_n_0_[11]\,
      I3 => \others_value_reg_n_0_[10]\,
      I4 => p_0_in(10),
      I5 => p_0_in0_in(10),
      O => \dout0__2_carry__0_i_13_n_0\
    );
\dout0__2_carry__0_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_6_n_0\,
      I1 => \dout0__2_carry__0_i_21_n_0\,
      I2 => \my_value_reg_n_0_[10]\,
      I3 => \others_value_reg_n_0_[9]\,
      I4 => p_0_in(9),
      I5 => p_0_in0_in(9),
      O => \dout0__2_carry__0_i_14_n_0\
    );
\dout0__2_carry__0_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_7_n_0\,
      I1 => \dout0__2_carry__0_i_22_n_0\,
      I2 => \my_value_reg_n_0_[9]\,
      I3 => \others_value_reg_n_0_[8]\,
      I4 => p_0_in(8),
      I5 => p_0_in0_in(8),
      O => \dout0__2_carry__0_i_15_n_0\
    );
\dout0__2_carry__0_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_8_n_0\,
      I1 => \dout0__2_carry__0_i_23_n_0\,
      I2 => \my_value_reg_n_0_[8]\,
      I3 => \others_value_reg_n_0_[7]\,
      I4 => p_0_in(7),
      I5 => p_0_in0_in(7),
      O => \dout0__2_carry__0_i_16_n_0\
    );
\dout0__2_carry__0_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(14),
      I1 => \others_value_reg_n_0_[14]\,
      I2 => p_0_in(14),
      O => \dout0__2_carry__0_i_17_n_0\
    );
\dout0__2_carry__0_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(13),
      I1 => \others_value_reg_n_0_[13]\,
      I2 => p_0_in(13),
      O => \dout0__2_carry__0_i_18_n_0\
    );
\dout0__2_carry__0_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(12),
      I1 => \others_value_reg_n_0_[12]\,
      I2 => p_0_in(12),
      O => \dout0__2_carry__0_i_19_n_0\
    );
\dout0__2_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[13]\,
      I1 => \dout0__2_carry__0_i_18_n_0\,
      I2 => p_0_in0_in(12),
      I3 => p_0_in(12),
      I4 => \others_value_reg_n_0_[12]\,
      O => \dout0__2_carry__0_i_2_n_0\
    );
\dout0__2_carry__0_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(11),
      I1 => \others_value_reg_n_0_[11]\,
      I2 => p_0_in(11),
      O => \dout0__2_carry__0_i_20_n_0\
    );
\dout0__2_carry__0_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(10),
      I1 => \others_value_reg_n_0_[10]\,
      I2 => p_0_in(10),
      O => \dout0__2_carry__0_i_21_n_0\
    );
\dout0__2_carry__0_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(9),
      I1 => \others_value_reg_n_0_[9]\,
      I2 => p_0_in(9),
      O => \dout0__2_carry__0_i_22_n_0\
    );
\dout0__2_carry__0_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(8),
      I1 => \others_value_reg_n_0_[8]\,
      I2 => p_0_in(8),
      O => \dout0__2_carry__0_i_23_n_0\
    );
\dout0__2_carry__0_i_24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(15),
      I1 => \others_value_reg_n_0_[15]\,
      I2 => p_0_in(15),
      O => \dout0__2_carry__0_i_24_n_0\
    );
\dout0__2_carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[12]\,
      I1 => \dout0__2_carry__0_i_19_n_0\,
      I2 => p_0_in0_in(11),
      I3 => p_0_in(11),
      I4 => \others_value_reg_n_0_[11]\,
      O => \dout0__2_carry__0_i_3_n_0\
    );
\dout0__2_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[11]\,
      I1 => \dout0__2_carry__0_i_20_n_0\,
      I2 => p_0_in0_in(10),
      I3 => p_0_in(10),
      I4 => \others_value_reg_n_0_[10]\,
      O => \dout0__2_carry__0_i_4_n_0\
    );
\dout0__2_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[10]\,
      I1 => \dout0__2_carry__0_i_21_n_0\,
      I2 => p_0_in0_in(9),
      I3 => p_0_in(9),
      I4 => \others_value_reg_n_0_[9]\,
      O => \dout0__2_carry__0_i_5_n_0\
    );
\dout0__2_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[9]\,
      I1 => \dout0__2_carry__0_i_22_n_0\,
      I2 => p_0_in0_in(8),
      I3 => p_0_in(8),
      I4 => \others_value_reg_n_0_[8]\,
      O => \dout0__2_carry__0_i_6_n_0\
    );
\dout0__2_carry__0_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[8]\,
      I1 => \dout0__2_carry__0_i_23_n_0\,
      I2 => p_0_in0_in(7),
      I3 => p_0_in(7),
      I4 => \others_value_reg_n_0_[7]\,
      O => \dout0__2_carry__0_i_7_n_0\
    );
\dout0__2_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[7]\,
      I1 => \dout0__2_carry_i_21_n_0\,
      I2 => p_0_in0_in(6),
      I3 => p_0_in(6),
      I4 => \others_value_reg_n_0_[6]\,
      O => \dout0__2_carry__0_i_8_n_0\
    );
\dout0__2_carry__0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__0_i_1_n_0\,
      I1 => \dout0__2_carry__0_i_24_n_0\,
      I2 => \my_value_reg_n_0_[15]\,
      I3 => \others_value_reg_n_0_[14]\,
      I4 => p_0_in(14),
      I5 => p_0_in0_in(14),
      O => \dout0__2_carry__0_i_9_n_0\
    );
\dout0__2_carry__1\: unisim.vcomponents.CARRY8
     port map (
      CI => \dout0__2_carry__0_n_0\,
      CI_TOP => '0',
      CO(7) => \dout0__2_carry__1_n_0\,
      CO(6) => \dout0__2_carry__1_n_1\,
      CO(5) => \dout0__2_carry__1_n_2\,
      CO(4) => \dout0__2_carry__1_n_3\,
      CO(3) => \dout0__2_carry__1_n_4\,
      CO(2) => \dout0__2_carry__1_n_5\,
      CO(1) => \dout0__2_carry__1_n_6\,
      CO(0) => \dout0__2_carry__1_n_7\,
      DI(7) => \dout0__2_carry__1_i_1_n_0\,
      DI(6) => \dout0__2_carry__1_i_2_n_0\,
      DI(5) => \dout0__2_carry__1_i_3_n_0\,
      DI(4) => \dout0__2_carry__1_i_4_n_0\,
      DI(3) => \dout0__2_carry__1_i_5_n_0\,
      DI(2) => \dout0__2_carry__1_i_6_n_0\,
      DI(1) => \dout0__2_carry__1_i_7_n_0\,
      DI(0) => \dout0__2_carry__1_i_8_n_0\,
      O(7 downto 0) => in7(23 downto 16),
      S(7) => \dout0__2_carry__1_i_9_n_0\,
      S(6) => \dout0__2_carry__1_i_10_n_0\,
      S(5) => \dout0__2_carry__1_i_11_n_0\,
      S(4) => \dout0__2_carry__1_i_12_n_0\,
      S(3) => \dout0__2_carry__1_i_13_n_0\,
      S(2) => \dout0__2_carry__1_i_14_n_0\,
      S(1) => \dout0__2_carry__1_i_15_n_0\,
      S(0) => \dout0__2_carry__1_i_16_n_0\
    );
\dout0__2_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[22]\,
      I1 => \dout0__2_carry__1_i_17_n_0\,
      I2 => p_0_in0_in(21),
      I3 => p_0_in(21),
      I4 => \others_value_reg_n_0_[21]\,
      O => \dout0__2_carry__1_i_1_n_0\
    );
\dout0__2_carry__1_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_2_n_0\,
      I1 => \dout0__2_carry__1_i_17_n_0\,
      I2 => \my_value_reg_n_0_[22]\,
      I3 => \others_value_reg_n_0_[21]\,
      I4 => p_0_in(21),
      I5 => p_0_in0_in(21),
      O => \dout0__2_carry__1_i_10_n_0\
    );
\dout0__2_carry__1_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_3_n_0\,
      I1 => \dout0__2_carry__1_i_18_n_0\,
      I2 => \my_value_reg_n_0_[21]\,
      I3 => \others_value_reg_n_0_[20]\,
      I4 => p_0_in(20),
      I5 => p_0_in0_in(20),
      O => \dout0__2_carry__1_i_11_n_0\
    );
\dout0__2_carry__1_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_4_n_0\,
      I1 => \dout0__2_carry__1_i_19_n_0\,
      I2 => \my_value_reg_n_0_[20]\,
      I3 => \others_value_reg_n_0_[19]\,
      I4 => p_0_in(19),
      I5 => p_0_in0_in(19),
      O => \dout0__2_carry__1_i_12_n_0\
    );
\dout0__2_carry__1_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_5_n_0\,
      I1 => \dout0__2_carry__1_i_20_n_0\,
      I2 => \my_value_reg_n_0_[19]\,
      I3 => \others_value_reg_n_0_[18]\,
      I4 => p_0_in(18),
      I5 => p_0_in0_in(18),
      O => \dout0__2_carry__1_i_13_n_0\
    );
\dout0__2_carry__1_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_6_n_0\,
      I1 => \dout0__2_carry__1_i_21_n_0\,
      I2 => \my_value_reg_n_0_[18]\,
      I3 => \others_value_reg_n_0_[17]\,
      I4 => p_0_in(17),
      I5 => p_0_in0_in(17),
      O => \dout0__2_carry__1_i_14_n_0\
    );
\dout0__2_carry__1_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_7_n_0\,
      I1 => \dout0__2_carry__1_i_22_n_0\,
      I2 => \my_value_reg_n_0_[17]\,
      I3 => \others_value_reg_n_0_[16]\,
      I4 => p_0_in(16),
      I5 => p_0_in0_in(16),
      O => \dout0__2_carry__1_i_15_n_0\
    );
\dout0__2_carry__1_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_8_n_0\,
      I1 => \dout0__2_carry__1_i_23_n_0\,
      I2 => \my_value_reg_n_0_[16]\,
      I3 => \others_value_reg_n_0_[15]\,
      I4 => p_0_in(15),
      I5 => p_0_in0_in(15),
      O => \dout0__2_carry__1_i_16_n_0\
    );
\dout0__2_carry__1_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(22),
      I1 => \others_value_reg_n_0_[22]\,
      I2 => p_0_in(22),
      O => \dout0__2_carry__1_i_17_n_0\
    );
\dout0__2_carry__1_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(21),
      I1 => \others_value_reg_n_0_[21]\,
      I2 => p_0_in(21),
      O => \dout0__2_carry__1_i_18_n_0\
    );
\dout0__2_carry__1_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(20),
      I1 => \others_value_reg_n_0_[20]\,
      I2 => p_0_in(20),
      O => \dout0__2_carry__1_i_19_n_0\
    );
\dout0__2_carry__1_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[21]\,
      I1 => \dout0__2_carry__1_i_18_n_0\,
      I2 => p_0_in0_in(20),
      I3 => p_0_in(20),
      I4 => \others_value_reg_n_0_[20]\,
      O => \dout0__2_carry__1_i_2_n_0\
    );
\dout0__2_carry__1_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(19),
      I1 => \others_value_reg_n_0_[19]\,
      I2 => p_0_in(19),
      O => \dout0__2_carry__1_i_20_n_0\
    );
\dout0__2_carry__1_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(18),
      I1 => \others_value_reg_n_0_[18]\,
      I2 => p_0_in(18),
      O => \dout0__2_carry__1_i_21_n_0\
    );
\dout0__2_carry__1_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(17),
      I1 => \others_value_reg_n_0_[17]\,
      I2 => p_0_in(17),
      O => \dout0__2_carry__1_i_22_n_0\
    );
\dout0__2_carry__1_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(16),
      I1 => \others_value_reg_n_0_[16]\,
      I2 => p_0_in(16),
      O => \dout0__2_carry__1_i_23_n_0\
    );
\dout0__2_carry__1_i_24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(23),
      I1 => \others_value_reg_n_0_[23]\,
      I2 => p_0_in(23),
      O => \dout0__2_carry__1_i_24_n_0\
    );
\dout0__2_carry__1_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[20]\,
      I1 => \dout0__2_carry__1_i_19_n_0\,
      I2 => p_0_in0_in(19),
      I3 => p_0_in(19),
      I4 => \others_value_reg_n_0_[19]\,
      O => \dout0__2_carry__1_i_3_n_0\
    );
\dout0__2_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[19]\,
      I1 => \dout0__2_carry__1_i_20_n_0\,
      I2 => p_0_in0_in(18),
      I3 => p_0_in(18),
      I4 => \others_value_reg_n_0_[18]\,
      O => \dout0__2_carry__1_i_4_n_0\
    );
\dout0__2_carry__1_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[18]\,
      I1 => \dout0__2_carry__1_i_21_n_0\,
      I2 => p_0_in0_in(17),
      I3 => p_0_in(17),
      I4 => \others_value_reg_n_0_[17]\,
      O => \dout0__2_carry__1_i_5_n_0\
    );
\dout0__2_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[17]\,
      I1 => \dout0__2_carry__1_i_22_n_0\,
      I2 => p_0_in0_in(16),
      I3 => p_0_in(16),
      I4 => \others_value_reg_n_0_[16]\,
      O => \dout0__2_carry__1_i_6_n_0\
    );
\dout0__2_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[16]\,
      I1 => \dout0__2_carry__1_i_23_n_0\,
      I2 => p_0_in0_in(15),
      I3 => p_0_in(15),
      I4 => \others_value_reg_n_0_[15]\,
      O => \dout0__2_carry__1_i_7_n_0\
    );
\dout0__2_carry__1_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[15]\,
      I1 => \dout0__2_carry__0_i_24_n_0\,
      I2 => p_0_in0_in(14),
      I3 => p_0_in(14),
      I4 => \others_value_reg_n_0_[14]\,
      O => \dout0__2_carry__1_i_8_n_0\
    );
\dout0__2_carry__1_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__1_i_1_n_0\,
      I1 => \dout0__2_carry__1_i_24_n_0\,
      I2 => \my_value_reg_n_0_[23]\,
      I3 => \others_value_reg_n_0_[22]\,
      I4 => p_0_in(22),
      I5 => p_0_in0_in(22),
      O => \dout0__2_carry__1_i_9_n_0\
    );
\dout0__2_carry__2\: unisim.vcomponents.CARRY8
     port map (
      CI => \dout0__2_carry__1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_dout0__2_carry__2_CO_UNCONNECTED\(7),
      CO(6) => \dout0__2_carry__2_n_1\,
      CO(5) => \dout0__2_carry__2_n_2\,
      CO(4) => \dout0__2_carry__2_n_3\,
      CO(3) => \dout0__2_carry__2_n_4\,
      CO(2) => \dout0__2_carry__2_n_5\,
      CO(1) => \dout0__2_carry__2_n_6\,
      CO(0) => \dout0__2_carry__2_n_7\,
      DI(7) => '0',
      DI(6) => \dout0__2_carry__2_i_1_n_0\,
      DI(5) => \dout0__2_carry__2_i_2_n_0\,
      DI(4) => \dout0__2_carry__2_i_3_n_0\,
      DI(3) => \dout0__2_carry__2_i_4_n_0\,
      DI(2) => \dout0__2_carry__2_i_5_n_0\,
      DI(1) => \dout0__2_carry__2_i_6_n_0\,
      DI(0) => \dout0__2_carry__2_i_7_n_0\,
      O(7 downto 0) => in7(31 downto 24),
      S(7) => \dout0__2_carry__2_i_8_n_0\,
      S(6) => \dout0__2_carry__2_i_9_n_0\,
      S(5) => \dout0__2_carry__2_i_10_n_0\,
      S(4) => \dout0__2_carry__2_i_11_n_0\,
      S(3) => \dout0__2_carry__2_i_12_n_0\,
      S(2) => \dout0__2_carry__2_i_13_n_0\,
      S(1) => \dout0__2_carry__2_i_14_n_0\,
      S(0) => \dout0__2_carry__2_i_15_n_0\
    );
\dout0__2_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[29]\,
      I1 => \dout0__2_carry__2_i_16_n_0\,
      I2 => p_0_in0_in(28),
      I3 => p_0_in(28),
      I4 => \others_value_reg_n_0_[28]\,
      O => \dout0__2_carry__2_i_1_n_0\
    );
\dout0__2_carry__2_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_2_n_0\,
      I1 => \dout0__2_carry__2_i_16_n_0\,
      I2 => \my_value_reg_n_0_[29]\,
      I3 => \others_value_reg_n_0_[28]\,
      I4 => p_0_in(28),
      I5 => p_0_in0_in(28),
      O => \dout0__2_carry__2_i_10_n_0\
    );
\dout0__2_carry__2_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_3_n_0\,
      I1 => \dout0__2_carry__2_i_17_n_0\,
      I2 => \my_value_reg_n_0_[28]\,
      I3 => \others_value_reg_n_0_[27]\,
      I4 => p_0_in(27),
      I5 => p_0_in0_in(27),
      O => \dout0__2_carry__2_i_11_n_0\
    );
\dout0__2_carry__2_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_4_n_0\,
      I1 => \dout0__2_carry__2_i_18_n_0\,
      I2 => \my_value_reg_n_0_[27]\,
      I3 => \others_value_reg_n_0_[26]\,
      I4 => p_0_in(26),
      I5 => p_0_in0_in(26),
      O => \dout0__2_carry__2_i_12_n_0\
    );
\dout0__2_carry__2_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_5_n_0\,
      I1 => \dout0__2_carry__2_i_19_n_0\,
      I2 => \my_value_reg_n_0_[26]\,
      I3 => \others_value_reg_n_0_[25]\,
      I4 => p_0_in(25),
      I5 => p_0_in0_in(25),
      O => \dout0__2_carry__2_i_13_n_0\
    );
\dout0__2_carry__2_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_6_n_0\,
      I1 => \dout0__2_carry__2_i_20_n_0\,
      I2 => \my_value_reg_n_0_[25]\,
      I3 => \others_value_reg_n_0_[24]\,
      I4 => p_0_in(24),
      I5 => p_0_in0_in(24),
      O => \dout0__2_carry__2_i_14_n_0\
    );
\dout0__2_carry__2_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_7_n_0\,
      I1 => \dout0__2_carry__2_i_21_n_0\,
      I2 => \my_value_reg_n_0_[24]\,
      I3 => \others_value_reg_n_0_[23]\,
      I4 => p_0_in(23),
      I5 => p_0_in0_in(23),
      O => \dout0__2_carry__2_i_15_n_0\
    );
\dout0__2_carry__2_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(29),
      I1 => \others_value_reg_n_0_[29]\,
      I2 => p_0_in(29),
      O => \dout0__2_carry__2_i_16_n_0\
    );
\dout0__2_carry__2_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(28),
      I1 => \others_value_reg_n_0_[28]\,
      I2 => p_0_in(28),
      O => \dout0__2_carry__2_i_17_n_0\
    );
\dout0__2_carry__2_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(27),
      I1 => \others_value_reg_n_0_[27]\,
      I2 => p_0_in(27),
      O => \dout0__2_carry__2_i_18_n_0\
    );
\dout0__2_carry__2_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(26),
      I1 => \others_value_reg_n_0_[26]\,
      I2 => p_0_in(26),
      O => \dout0__2_carry__2_i_19_n_0\
    );
\dout0__2_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[28]\,
      I1 => \dout0__2_carry__2_i_17_n_0\,
      I2 => p_0_in0_in(27),
      I3 => p_0_in(27),
      I4 => \others_value_reg_n_0_[27]\,
      O => \dout0__2_carry__2_i_2_n_0\
    );
\dout0__2_carry__2_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(25),
      I1 => \others_value_reg_n_0_[25]\,
      I2 => p_0_in(25),
      O => \dout0__2_carry__2_i_20_n_0\
    );
\dout0__2_carry__2_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(24),
      I1 => \others_value_reg_n_0_[24]\,
      I2 => p_0_in(24),
      O => \dout0__2_carry__2_i_21_n_0\
    );
\dout0__2_carry__2_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \others_value_reg_n_0_[29]\,
      I1 => p_0_in(29),
      I2 => p_0_in0_in(29),
      O => \dout0__2_carry__2_i_22_n_0\
    );
\dout0__2_carry__2_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => p_0_in(31),
      I1 => \others_value_reg_n_0_[31]\,
      I2 => p_0_in0_in(31),
      I3 => \my_value_reg_n_0_[31]\,
      O => \dout0__2_carry__2_i_23_n_0\
    );
\dout0__2_carry__2_i_24\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(30),
      I1 => \others_value_reg_n_0_[30]\,
      I2 => p_0_in(30),
      O => \dout0__2_carry__2_i_24_n_0\
    );
\dout0__2_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[27]\,
      I1 => \dout0__2_carry__2_i_18_n_0\,
      I2 => p_0_in0_in(26),
      I3 => p_0_in(26),
      I4 => \others_value_reg_n_0_[26]\,
      O => \dout0__2_carry__2_i_3_n_0\
    );
\dout0__2_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[26]\,
      I1 => \dout0__2_carry__2_i_19_n_0\,
      I2 => p_0_in0_in(25),
      I3 => p_0_in(25),
      I4 => \others_value_reg_n_0_[25]\,
      O => \dout0__2_carry__2_i_4_n_0\
    );
\dout0__2_carry__2_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[25]\,
      I1 => \dout0__2_carry__2_i_20_n_0\,
      I2 => p_0_in0_in(24),
      I3 => p_0_in(24),
      I4 => \others_value_reg_n_0_[24]\,
      O => \dout0__2_carry__2_i_5_n_0\
    );
\dout0__2_carry__2_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[24]\,
      I1 => \dout0__2_carry__2_i_21_n_0\,
      I2 => p_0_in0_in(23),
      I3 => p_0_in(23),
      I4 => \others_value_reg_n_0_[23]\,
      O => \dout0__2_carry__2_i_6_n_0\
    );
\dout0__2_carry__2_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[23]\,
      I1 => \dout0__2_carry__1_i_24_n_0\,
      I2 => p_0_in0_in(22),
      I3 => p_0_in(22),
      I4 => \others_value_reg_n_0_[22]\,
      O => \dout0__2_carry__2_i_7_n_0\
    );
\dout0__2_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E187871E871E1E78"
    )
        port map (
      I0 => \dout0__2_carry__2_i_22_n_0\,
      I1 => \my_value_reg_n_0_[30]\,
      I2 => \dout0__2_carry__2_i_23_n_0\,
      I3 => \others_value_reg_n_0_[30]\,
      I4 => p_0_in(30),
      I5 => p_0_in0_in(30),
      O => \dout0__2_carry__2_i_8_n_0\
    );
\dout0__2_carry__2_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry__2_i_1_n_0\,
      I1 => \dout0__2_carry__2_i_24_n_0\,
      I2 => \my_value_reg_n_0_[30]\,
      I3 => \others_value_reg_n_0_[29]\,
      I4 => p_0_in(29),
      I5 => p_0_in0_in(29),
      O => \dout0__2_carry__2_i_9_n_0\
    );
\dout0__2_carry_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[6]\,
      I1 => \dout0__2_carry_i_16_n_0\,
      I2 => p_0_in0_in(5),
      I3 => p_0_in(5),
      I4 => \others_value_reg_n_0_[5]\,
      O => \dout0__2_carry_i_1_n_0\
    );
\dout0__2_carry_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry_i_3_n_0\,
      I1 => \dout0__2_carry_i_17_n_0\,
      I2 => \my_value_reg_n_0_[5]\,
      I3 => \others_value_reg_n_0_[4]\,
      I4 => p_0_in(4),
      I5 => p_0_in0_in(4),
      O => \dout0__2_carry_i_10_n_0\
    );
\dout0__2_carry_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry_i_4_n_0\,
      I1 => \dout0__2_carry_i_18_n_0\,
      I2 => \my_value_reg_n_0_[4]\,
      I3 => \others_value_reg_n_0_[3]\,
      I4 => p_0_in(3),
      I5 => p_0_in0_in(3),
      O => \dout0__2_carry_i_11_n_0\
    );
\dout0__2_carry_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry_i_5_n_0\,
      I1 => \dout0__2_carry_i_19_n_0\,
      I2 => \my_value_reg_n_0_[3]\,
      I3 => \others_value_reg_n_0_[2]\,
      I4 => p_0_in(2),
      I5 => p_0_in0_in(2),
      O => \dout0__2_carry_i_12_n_0\
    );
\dout0__2_carry_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6999999699969666"
    )
        port map (
      I0 => \dout0__2_carry_i_20_n_0\,
      I1 => \my_value_reg_n_0_[2]\,
      I2 => p_0_in0_in(1),
      I3 => \others_value_reg_n_0_[1]\,
      I4 => p_0_in(1),
      I5 => \my_value_reg_n_0_[1]\,
      O => \dout0__2_carry_i_13_n_0\
    );
\dout0__2_carry_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"566A"
    )
        port map (
      I0 => \dout0__2_carry_i_7_n_0\,
      I1 => p_0_in0_in(0),
      I2 => p_0_in(0),
      I3 => \others_value_reg_n_0_[0]\,
      O => \dout0__2_carry_i_14_n_0\
    );
\dout0__2_carry_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => p_0_in(0),
      I1 => \others_value_reg_n_0_[0]\,
      I2 => p_0_in0_in(0),
      I3 => \my_value_reg_n_0_[0]\,
      O => \dout0__2_carry_i_15_n_0\
    );
\dout0__2_carry_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(6),
      I1 => \others_value_reg_n_0_[6]\,
      I2 => p_0_in(6),
      O => \dout0__2_carry_i_16_n_0\
    );
\dout0__2_carry_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(5),
      I1 => \others_value_reg_n_0_[5]\,
      I2 => p_0_in(5),
      O => \dout0__2_carry_i_17_n_0\
    );
\dout0__2_carry_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(4),
      I1 => \others_value_reg_n_0_[4]\,
      I2 => p_0_in(4),
      O => \dout0__2_carry_i_18_n_0\
    );
\dout0__2_carry_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(3),
      I1 => \others_value_reg_n_0_[3]\,
      I2 => p_0_in(3),
      O => \dout0__2_carry_i_19_n_0\
    );
\dout0__2_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[5]\,
      I1 => \dout0__2_carry_i_17_n_0\,
      I2 => p_0_in0_in(4),
      I3 => p_0_in(4),
      I4 => \others_value_reg_n_0_[4]\,
      O => \dout0__2_carry_i_2_n_0\
    );
\dout0__2_carry_i_20\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(2),
      I1 => \others_value_reg_n_0_[2]\,
      I2 => p_0_in(2),
      O => \dout0__2_carry_i_20_n_0\
    );
\dout0__2_carry_i_21\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => p_0_in0_in(7),
      I1 => \others_value_reg_n_0_[7]\,
      I2 => p_0_in(7),
      O => \dout0__2_carry_i_21_n_0\
    );
\dout0__2_carry_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[4]\,
      I1 => \dout0__2_carry_i_18_n_0\,
      I2 => p_0_in0_in(3),
      I3 => p_0_in(3),
      I4 => \others_value_reg_n_0_[3]\,
      O => \dout0__2_carry_i_3_n_0\
    );
\dout0__2_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[3]\,
      I1 => \dout0__2_carry_i_19_n_0\,
      I2 => p_0_in0_in(2),
      I3 => p_0_in(2),
      I4 => \others_value_reg_n_0_[2]\,
      O => \dout0__2_carry_i_4_n_0\
    );
\dout0__2_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \my_value_reg_n_0_[2]\,
      I1 => \dout0__2_carry_i_20_n_0\,
      I2 => p_0_in0_in(1),
      I3 => p_0_in(1),
      I4 => \others_value_reg_n_0_[1]\,
      O => \dout0__2_carry_i_5_n_0\
    );
\dout0__2_carry_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => p_0_in0_in(1),
      I1 => p_0_in(1),
      I2 => \others_value_reg_n_0_[1]\,
      I3 => \my_value_reg_n_0_[2]\,
      I4 => \dout0__2_carry_i_20_n_0\,
      O => \dout0__2_carry_i_6_n_0\
    );
\dout0__2_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => p_0_in(1),
      I1 => \others_value_reg_n_0_[1]\,
      I2 => p_0_in0_in(1),
      I3 => \my_value_reg_n_0_[1]\,
      O => \dout0__2_carry_i_7_n_0\
    );
\dout0__2_carry_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry_i_1_n_0\,
      I1 => \dout0__2_carry_i_21_n_0\,
      I2 => \my_value_reg_n_0_[7]\,
      I3 => \others_value_reg_n_0_[6]\,
      I4 => p_0_in(6),
      I5 => p_0_in0_in(6),
      O => \dout0__2_carry_i_8_n_0\
    );
\dout0__2_carry_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \dout0__2_carry_i_2_n_0\,
      I1 => \dout0__2_carry_i_16_n_0\,
      I2 => \my_value_reg_n_0_[6]\,
      I3 => \others_value_reg_n_0_[5]\,
      I4 => p_0_in(5),
      I5 => p_0_in0_in(5),
      O => \dout0__2_carry_i_9_n_0\
    );
\dout[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(0),
      O => \dout[0]_i_1_n_0\
    );
\dout[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(10),
      O => \dout[10]_i_1_n_0\
    );
\dout[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(11),
      O => \dout[11]_i_1_n_0\
    );
\dout[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(12),
      O => \dout[12]_i_1_n_0\
    );
\dout[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(13),
      O => \dout[13]_i_1_n_0\
    );
\dout[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(14),
      O => \dout[14]_i_1_n_0\
    );
\dout[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(15),
      O => \dout[15]_i_1_n_0\
    );
\dout[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(16),
      O => \dout[16]_i_1_n_0\
    );
\dout[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(17),
      O => \dout[17]_i_1_n_0\
    );
\dout[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(18),
      O => \dout[18]_i_1_n_0\
    );
\dout[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(19),
      O => \dout[19]_i_1_n_0\
    );
\dout[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(1),
      O => \dout[1]_i_1_n_0\
    );
\dout[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(20),
      O => \dout[20]_i_1_n_0\
    );
\dout[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(21),
      O => \dout[21]_i_1_n_0\
    );
\dout[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(22),
      O => \dout[22]_i_1_n_0\
    );
\dout[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(23),
      O => \dout[23]_i_1_n_0\
    );
\dout[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(24),
      O => \dout[24]_i_1_n_0\
    );
\dout[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(25),
      O => \dout[25]_i_1_n_0\
    );
\dout[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(26),
      O => \dout[26]_i_1_n_0\
    );
\dout[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(27),
      O => \dout[27]_i_1_n_0\
    );
\dout[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(28),
      O => \dout[28]_i_1_n_0\
    );
\dout[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(29),
      O => \dout[29]_i_1_n_0\
    );
\dout[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(2),
      O => \dout[2]_i_1_n_0\
    );
\dout[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(30),
      O => \dout[30]_i_1_n_0\
    );
\dout[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \FSM_onehot_state_reg_n_0_[6]\,
      O => \dout[31]_i_1_n_0\
    );
\dout[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(31),
      O => \dout[31]_i_2_n_0\
    );
\dout[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(3),
      O => \dout[3]_i_1_n_0\
    );
\dout[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(4),
      O => \dout[4]_i_1_n_0\
    );
\dout[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(5),
      O => \dout[5]_i_1_n_0\
    );
\dout[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(6),
      O => \dout[6]_i_1_n_0\
    );
\dout[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(7),
      O => \dout[7]_i_1_n_0\
    );
\dout[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(8),
      O => \dout[8]_i_1_n_0\
    );
\dout[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => in7(9),
      O => \dout[9]_i_1_n_0\
    );
\dout_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[0]_i_1_n_0\,
      Q => dout(0)
    );
\dout_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[10]_i_1_n_0\,
      Q => dout(10)
    );
\dout_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[11]_i_1_n_0\,
      Q => dout(11)
    );
\dout_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[12]_i_1_n_0\,
      Q => dout(12)
    );
\dout_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[13]_i_1_n_0\,
      Q => dout(13)
    );
\dout_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[14]_i_1_n_0\,
      Q => dout(14)
    );
\dout_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[15]_i_1_n_0\,
      Q => dout(15)
    );
\dout_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[16]_i_1_n_0\,
      Q => dout(16)
    );
\dout_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[17]_i_1_n_0\,
      Q => dout(17)
    );
\dout_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[18]_i_1_n_0\,
      Q => dout(18)
    );
\dout_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[19]_i_1_n_0\,
      Q => dout(19)
    );
\dout_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[1]_i_1_n_0\,
      Q => dout(1)
    );
\dout_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[20]_i_1_n_0\,
      Q => dout(20)
    );
\dout_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[21]_i_1_n_0\,
      Q => dout(21)
    );
\dout_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[22]_i_1_n_0\,
      Q => dout(22)
    );
\dout_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[23]_i_1_n_0\,
      Q => dout(23)
    );
\dout_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[24]_i_1_n_0\,
      Q => dout(24)
    );
\dout_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[25]_i_1_n_0\,
      Q => dout(25)
    );
\dout_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[26]_i_1_n_0\,
      Q => dout(26)
    );
\dout_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[27]_i_1_n_0\,
      Q => dout(27)
    );
\dout_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[28]_i_1_n_0\,
      Q => dout(28)
    );
\dout_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[29]_i_1_n_0\,
      Q => dout(29)
    );
\dout_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[2]_i_1_n_0\,
      Q => dout(2)
    );
\dout_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[30]_i_1_n_0\,
      Q => dout(30)
    );
\dout_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[31]_i_2_n_0\,
      Q => dout(31)
    );
\dout_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[3]_i_1_n_0\,
      Q => dout(3)
    );
\dout_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[4]_i_1_n_0\,
      Q => dout(4)
    );
\dout_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[5]_i_1_n_0\,
      Q => dout(5)
    );
\dout_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[6]_i_1_n_0\,
      Q => dout(6)
    );
\dout_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[7]_i_1_n_0\,
      Q => dout(7)
    );
\dout_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[8]_i_1_n_0\,
      Q => dout(8)
    );
\dout_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => SR(0),
      D => \dout[9]_i_1_n_0\,
      Q => dout(9)
    );
\fin[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_FIN_1,
      I2 => \^q\(0),
      I3 => fin(0),
      O => \fin[0]_i_1_n_0\
    );
\fin[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_FIN_2,
      I2 => \^q\(0),
      I3 => fin(1),
      O => \fin[1]_i_1_n_0\
    );
\fin[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_FIN_3,
      I2 => \^q\(0),
      I3 => fin(2),
      O => \fin[2]_i_1_n_0\
    );
\fin_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \fin[0]_i_1_n_0\,
      Q => fin(0)
    );
\fin_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \fin[1]_i_1_n_0\,
      Q => fin(1)
    );
\fin_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \fin[2]_i_1_n_0\,
      Q => fin(2)
    );
\my_value[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA80000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \my_value[0]_i_2_n_0\,
      I2 => \my_value[0]_i_3_n_0\,
      I3 => \my_value[0]_i_4_n_0\,
      I4 => \my_value_reg[31]_0\(0),
      O => \my_value[0]_i_1_n_0\
    );
\my_value[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBFFF"
    )
        port map (
      I0 => \my_value[0]_i_5_n_0\,
      I1 => \my_value_reg[31]_0\(30),
      I2 => \my_value_reg[31]_0\(29),
      I3 => \my_value_reg[31]_0\(31),
      I4 => \my_value[0]_i_6_n_0\,
      I5 => \my_value[0]_i_7_n_0\,
      O => \my_value[0]_i_2_n_0\
    );
\my_value[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(6),
      I1 => \my_value_reg[31]_0\(5),
      I2 => \my_value_reg[31]_0\(8),
      I3 => \my_value_reg[31]_0\(7),
      I4 => \my_value[0]_i_8_n_0\,
      O => \my_value[0]_i_3_n_0\
    );
\my_value[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(14),
      I1 => \my_value_reg[31]_0\(13),
      I2 => \my_value_reg[31]_0\(16),
      I3 => \my_value_reg[31]_0\(15),
      I4 => \my_value[0]_i_9_n_0\,
      O => \my_value[0]_i_4_n_0\
    );
\my_value[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(27),
      I1 => \my_value_reg[31]_0\(28),
      I2 => \my_value_reg[31]_0\(25),
      I3 => \my_value_reg[31]_0\(26),
      O => \my_value[0]_i_5_n_0\
    );
\my_value[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(19),
      I1 => \my_value_reg[31]_0\(20),
      I2 => \my_value_reg[31]_0\(17),
      I3 => \my_value_reg[31]_0\(18),
      O => \my_value[0]_i_6_n_0\
    );
\my_value[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(23),
      I1 => \my_value_reg[31]_0\(24),
      I2 => \my_value_reg[31]_0\(21),
      I3 => \my_value_reg[31]_0\(22),
      O => \my_value[0]_i_7_n_0\
    );
\my_value[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(3),
      I1 => \my_value_reg[31]_0\(4),
      I2 => \my_value_reg[31]_0\(1),
      I3 => \my_value_reg[31]_0\(2),
      O => \my_value[0]_i_8_n_0\
    );
\my_value[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \my_value_reg[31]_0\(11),
      I1 => \my_value_reg[31]_0\(12),
      I2 => \my_value_reg[31]_0\(9),
      I3 => \my_value_reg[31]_0\(10),
      O => \my_value[0]_i_9_n_0\
    );
\my_value[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(10),
      O => \my_value[10]_i_1_n_0\
    );
\my_value[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(11),
      O => \my_value[11]_i_1_n_0\
    );
\my_value[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(12),
      O => \my_value[12]_i_1_n_0\
    );
\my_value[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(13),
      O => \my_value[13]_i_1_n_0\
    );
\my_value[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(14),
      O => \my_value[14]_i_1_n_0\
    );
\my_value[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(15),
      O => \my_value[15]_i_1_n_0\
    );
\my_value[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(16),
      O => \my_value[16]_i_1_n_0\
    );
\my_value[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(17),
      O => \my_value[17]_i_1_n_0\
    );
\my_value[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(18),
      O => \my_value[18]_i_1_n_0\
    );
\my_value[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(19),
      O => \my_value[19]_i_1_n_0\
    );
\my_value[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(1),
      O => \my_value[1]_i_1_n_0\
    );
\my_value[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(20),
      O => \my_value[20]_i_1_n_0\
    );
\my_value[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(21),
      O => \my_value[21]_i_1_n_0\
    );
\my_value[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(22),
      O => \my_value[22]_i_1_n_0\
    );
\my_value[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(23),
      O => \my_value[23]_i_1_n_0\
    );
\my_value[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(24),
      O => \my_value[24]_i_1_n_0\
    );
\my_value[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(25),
      O => \my_value[25]_i_1_n_0\
    );
\my_value[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(26),
      O => \my_value[26]_i_1_n_0\
    );
\my_value[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(27),
      O => \my_value[27]_i_1_n_0\
    );
\my_value[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(28),
      O => \my_value[28]_i_1_n_0\
    );
\my_value[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(29),
      O => \my_value[29]_i_1_n_0\
    );
\my_value[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(2),
      O => \my_value[2]_i_1_n_0\
    );
\my_value[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(30),
      O => \my_value[30]_i_1_n_0\
    );
\my_value[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      O => \my_value[31]_i_1_n_0\
    );
\my_value[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(31),
      O => \my_value[31]_i_2_n_0\
    );
\my_value[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(3),
      O => \my_value[3]_i_1_n_0\
    );
\my_value[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(4),
      O => \my_value[4]_i_1_n_0\
    );
\my_value[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(5),
      O => \my_value[5]_i_1_n_0\
    );
\my_value[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(6),
      O => \my_value[6]_i_1_n_0\
    );
\my_value[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(7),
      O => \my_value[7]_i_1_n_0\
    );
\my_value[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(8),
      O => \my_value[8]_i_1_n_0\
    );
\my_value[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => \FSM_onehot_state[5]_i_2_n_0\,
      I2 => \FSM_onehot_state[5]_i_3_n_0\,
      I3 => \FSM_onehot_state[5]_i_4_n_0\,
      I4 => \FSM_onehot_state[5]_i_5_n_0\,
      I5 => \my_value_reg[31]_0\(9),
      O => \my_value[9]_i_1_n_0\
    );
\my_value_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[0]_i_1_n_0\,
      Q => \my_value_reg_n_0_[0]\
    );
\my_value_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[10]_i_1_n_0\,
      Q => \my_value_reg_n_0_[10]\
    );
\my_value_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[11]_i_1_n_0\,
      Q => \my_value_reg_n_0_[11]\
    );
\my_value_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[12]_i_1_n_0\,
      Q => \my_value_reg_n_0_[12]\
    );
\my_value_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[13]_i_1_n_0\,
      Q => \my_value_reg_n_0_[13]\
    );
\my_value_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[14]_i_1_n_0\,
      Q => \my_value_reg_n_0_[14]\
    );
\my_value_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[15]_i_1_n_0\,
      Q => \my_value_reg_n_0_[15]\
    );
\my_value_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[16]_i_1_n_0\,
      Q => \my_value_reg_n_0_[16]\
    );
\my_value_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[17]_i_1_n_0\,
      Q => \my_value_reg_n_0_[17]\
    );
\my_value_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[18]_i_1_n_0\,
      Q => \my_value_reg_n_0_[18]\
    );
\my_value_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[19]_i_1_n_0\,
      Q => \my_value_reg_n_0_[19]\
    );
\my_value_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[1]_i_1_n_0\,
      Q => \my_value_reg_n_0_[1]\
    );
\my_value_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[20]_i_1_n_0\,
      Q => \my_value_reg_n_0_[20]\
    );
\my_value_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[21]_i_1_n_0\,
      Q => \my_value_reg_n_0_[21]\
    );
\my_value_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[22]_i_1_n_0\,
      Q => \my_value_reg_n_0_[22]\
    );
\my_value_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[23]_i_1_n_0\,
      Q => \my_value_reg_n_0_[23]\
    );
\my_value_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[24]_i_1_n_0\,
      Q => \my_value_reg_n_0_[24]\
    );
\my_value_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[25]_i_1_n_0\,
      Q => \my_value_reg_n_0_[25]\
    );
\my_value_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[26]_i_1_n_0\,
      Q => \my_value_reg_n_0_[26]\
    );
\my_value_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[27]_i_1_n_0\,
      Q => \my_value_reg_n_0_[27]\
    );
\my_value_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[28]_i_1_n_0\,
      Q => \my_value_reg_n_0_[28]\
    );
\my_value_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[29]_i_1_n_0\,
      Q => \my_value_reg_n_0_[29]\
    );
\my_value_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[2]_i_1_n_0\,
      Q => \my_value_reg_n_0_[2]\
    );
\my_value_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[30]_i_1_n_0\,
      Q => \my_value_reg_n_0_[30]\
    );
\my_value_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[31]_i_2_n_0\,
      Q => \my_value_reg_n_0_[31]\
    );
\my_value_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[3]_i_1_n_0\,
      Q => \my_value_reg_n_0_[3]\
    );
\my_value_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[4]_i_1_n_0\,
      Q => \my_value_reg_n_0_[4]\
    );
\my_value_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[5]_i_1_n_0\,
      Q => \my_value_reg_n_0_[5]\
    );
\my_value_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[6]_i_1_n_0\,
      Q => \my_value_reg_n_0_[6]\
    );
\my_value_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[7]_i_1_n_0\,
      Q => \my_value_reg_n_0_[7]\
    );
\my_value_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[8]_i_1_n_0\,
      Q => \my_value_reg_n_0_[8]\
    );
\my_value_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => SR(0),
      D => \my_value[9]_i_1_n_0\,
      Q => \my_value_reg_n_0_[9]\
    );
\others_value[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(0),
      O => p_6_out0_in(0)
    );
\others_value[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(10),
      O => p_6_out0_in(10)
    );
\others_value[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(11),
      O => p_6_out0_in(11)
    );
\others_value[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(12),
      O => p_6_out0_in(12)
    );
\others_value[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(13),
      O => p_6_out0_in(13)
    );
\others_value[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(14),
      O => p_6_out0_in(14)
    );
\others_value[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(15),
      O => p_6_out0_in(15)
    );
\others_value[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(16),
      O => p_6_out0_in(16)
    );
\others_value[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(17),
      O => p_6_out0_in(17)
    );
\others_value[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(18),
      O => p_6_out0_in(18)
    );
\others_value[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(19),
      O => p_6_out0_in(19)
    );
\others_value[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(1),
      O => p_6_out0_in(1)
    );
\others_value[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(20),
      O => p_6_out0_in(20)
    );
\others_value[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(21),
      O => p_6_out0_in(21)
    );
\others_value[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(22),
      O => p_6_out0_in(22)
    );
\others_value[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(23),
      O => p_6_out0_in(23)
    );
\others_value[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(24),
      O => p_6_out0_in(24)
    );
\others_value[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(25),
      O => p_6_out0_in(25)
    );
\others_value[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(26),
      O => p_6_out0_in(26)
    );
\others_value[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(27),
      O => p_6_out0_in(27)
    );
\others_value[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(28),
      O => p_6_out0_in(28)
    );
\others_value[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(29),
      O => p_6_out0_in(29)
    );
\others_value[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(2),
      O => p_6_out0_in(2)
    );
\others_value[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(30),
      O => p_6_out0_in(30)
    );
\others_value[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEFE"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => \^q\(0),
      I2 => DST_FIN_1,
      I3 => \valid_reg_n_0_[0]\,
      O => p_6_out(31)
    );
\others_value[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(31),
      O => p_6_out0_in(31)
    );
\others_value[32]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(0),
      O => p_6_out0_in(32)
    );
\others_value[33]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(1),
      O => p_6_out0_in(33)
    );
\others_value[34]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(2),
      O => p_6_out0_in(34)
    );
\others_value[35]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(3),
      O => p_6_out0_in(35)
    );
\others_value[36]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(4),
      O => p_6_out0_in(36)
    );
\others_value[37]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(5),
      O => p_6_out0_in(37)
    );
\others_value[38]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(6),
      O => p_6_out0_in(38)
    );
\others_value[39]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(7),
      O => p_6_out0_in(39)
    );
\others_value[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(3),
      O => p_6_out0_in(3)
    );
\others_value[40]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(8),
      O => p_6_out0_in(40)
    );
\others_value[41]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(9),
      O => p_6_out0_in(41)
    );
\others_value[42]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(10),
      O => p_6_out0_in(42)
    );
\others_value[43]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(11),
      O => p_6_out0_in(43)
    );
\others_value[44]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(12),
      O => p_6_out0_in(44)
    );
\others_value[45]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(13),
      O => p_6_out0_in(45)
    );
\others_value[46]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(14),
      O => p_6_out0_in(46)
    );
\others_value[47]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(15),
      O => p_6_out0_in(47)
    );
\others_value[48]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(16),
      O => p_6_out0_in(48)
    );
\others_value[49]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(17),
      O => p_6_out0_in(49)
    );
\others_value[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(4),
      O => p_6_out0_in(4)
    );
\others_value[50]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(18),
      O => p_6_out0_in(50)
    );
\others_value[51]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(19),
      O => p_6_out0_in(51)
    );
\others_value[52]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(20),
      O => p_6_out0_in(52)
    );
\others_value[53]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(21),
      O => p_6_out0_in(53)
    );
\others_value[54]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(22),
      O => p_6_out0_in(54)
    );
\others_value[55]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(23),
      O => p_6_out0_in(55)
    );
\others_value[56]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(24),
      O => p_6_out0_in(56)
    );
\others_value[57]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(25),
      O => p_6_out0_in(57)
    );
\others_value[58]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(26),
      O => p_6_out0_in(58)
    );
\others_value[59]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(27),
      O => p_6_out0_in(59)
    );
\others_value[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(5),
      O => p_6_out0_in(5)
    );
\others_value[60]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(28),
      O => p_6_out0_in(60)
    );
\others_value[61]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(29),
      O => p_6_out0_in(61)
    );
\others_value[62]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(30),
      O => p_6_out0_in(62)
    );
\others_value[63]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEFE"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => \^q\(0),
      I2 => DST_FIN_2,
      I3 => \valid_reg_n_0_[1]\,
      O => p_6_out(63)
    );
\others_value[63]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => DST_DATA_2(31),
      O => p_6_out0_in(63)
    );
\others_value[64]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(0),
      O => p_6_out0_in(64)
    );
\others_value[65]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(1),
      O => p_6_out0_in(65)
    );
\others_value[66]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(2),
      O => p_6_out0_in(66)
    );
\others_value[67]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(3),
      O => p_6_out0_in(67)
    );
\others_value[68]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(4),
      O => p_6_out0_in(68)
    );
\others_value[69]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(5),
      O => p_6_out0_in(69)
    );
\others_value[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(6),
      O => p_6_out0_in(6)
    );
\others_value[70]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(6),
      O => p_6_out0_in(70)
    );
\others_value[71]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(7),
      O => p_6_out0_in(71)
    );
\others_value[72]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(8),
      O => p_6_out0_in(72)
    );
\others_value[73]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(9),
      O => p_6_out0_in(73)
    );
\others_value[74]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(10),
      O => p_6_out0_in(74)
    );
\others_value[75]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(11),
      O => p_6_out0_in(75)
    );
\others_value[76]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(12),
      O => p_6_out0_in(76)
    );
\others_value[77]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(13),
      O => p_6_out0_in(77)
    );
\others_value[78]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(14),
      O => p_6_out0_in(78)
    );
\others_value[79]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(15),
      O => p_6_out0_in(79)
    );
\others_value[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(7),
      O => p_6_out0_in(7)
    );
\others_value[80]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(16),
      O => p_6_out0_in(80)
    );
\others_value[81]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(17),
      O => p_6_out0_in(81)
    );
\others_value[82]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(18),
      O => p_6_out0_in(82)
    );
\others_value[83]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(19),
      O => p_6_out0_in(83)
    );
\others_value[84]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(20),
      O => p_6_out0_in(84)
    );
\others_value[85]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(21),
      O => p_6_out0_in(85)
    );
\others_value[86]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(22),
      O => p_6_out0_in(86)
    );
\others_value[87]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(23),
      O => p_6_out0_in(87)
    );
\others_value[88]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(24),
      O => p_6_out0_in(88)
    );
\others_value[89]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(25),
      O => p_6_out0_in(89)
    );
\others_value[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(8),
      O => p_6_out0_in(8)
    );
\others_value[90]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(26),
      O => p_6_out0_in(90)
    );
\others_value[91]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(27),
      O => p_6_out0_in(91)
    );
\others_value[92]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(28),
      O => p_6_out0_in(92)
    );
\others_value[93]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(29),
      O => p_6_out0_in(93)
    );
\others_value[94]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(30),
      O => p_6_out0_in(94)
    );
\others_value[95]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEFE"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => \^q\(0),
      I2 => DST_FIN_3,
      I3 => \valid_reg_n_0_[2]\,
      O => p_6_out(95)
    );
\others_value[95]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => DST_DATA_3(31),
      O => p_6_out0_in(95)
    );
\others_value[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => DST_DATA_1(9),
      O => p_6_out0_in(9)
    );
\others_value_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(0),
      Q => \others_value_reg_n_0_[0]\
    );
\others_value_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(10),
      Q => \others_value_reg_n_0_[10]\
    );
\others_value_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(11),
      Q => \others_value_reg_n_0_[11]\
    );
\others_value_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(12),
      Q => \others_value_reg_n_0_[12]\
    );
\others_value_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(13),
      Q => \others_value_reg_n_0_[13]\
    );
\others_value_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(14),
      Q => \others_value_reg_n_0_[14]\
    );
\others_value_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(15),
      Q => \others_value_reg_n_0_[15]\
    );
\others_value_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(16),
      Q => \others_value_reg_n_0_[16]\
    );
\others_value_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(17),
      Q => \others_value_reg_n_0_[17]\
    );
\others_value_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(18),
      Q => \others_value_reg_n_0_[18]\
    );
\others_value_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(19),
      Q => \others_value_reg_n_0_[19]\
    );
\others_value_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(1),
      Q => \others_value_reg_n_0_[1]\
    );
\others_value_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(20),
      Q => \others_value_reg_n_0_[20]\
    );
\others_value_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(21),
      Q => \others_value_reg_n_0_[21]\
    );
\others_value_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(22),
      Q => \others_value_reg_n_0_[22]\
    );
\others_value_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(23),
      Q => \others_value_reg_n_0_[23]\
    );
\others_value_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(24),
      Q => \others_value_reg_n_0_[24]\
    );
\others_value_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(25),
      Q => \others_value_reg_n_0_[25]\
    );
\others_value_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(26),
      Q => \others_value_reg_n_0_[26]\
    );
\others_value_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(27),
      Q => \others_value_reg_n_0_[27]\
    );
\others_value_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(28),
      Q => \others_value_reg_n_0_[28]\
    );
\others_value_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(29),
      Q => \others_value_reg_n_0_[29]\
    );
\others_value_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(2),
      Q => \others_value_reg_n_0_[2]\
    );
\others_value_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(30),
      Q => \others_value_reg_n_0_[30]\
    );
\others_value_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(31),
      Q => \others_value_reg_n_0_[31]\
    );
\others_value_reg[32]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(32),
      Q => p_0_in(0)
    );
\others_value_reg[33]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(33),
      Q => p_0_in(1)
    );
\others_value_reg[34]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(34),
      Q => p_0_in(2)
    );
\others_value_reg[35]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(35),
      Q => p_0_in(3)
    );
\others_value_reg[36]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(36),
      Q => p_0_in(4)
    );
\others_value_reg[37]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(37),
      Q => p_0_in(5)
    );
\others_value_reg[38]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(38),
      Q => p_0_in(6)
    );
\others_value_reg[39]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(39),
      Q => p_0_in(7)
    );
\others_value_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(3),
      Q => \others_value_reg_n_0_[3]\
    );
\others_value_reg[40]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(40),
      Q => p_0_in(8)
    );
\others_value_reg[41]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(41),
      Q => p_0_in(9)
    );
\others_value_reg[42]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(42),
      Q => p_0_in(10)
    );
\others_value_reg[43]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(43),
      Q => p_0_in(11)
    );
\others_value_reg[44]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(44),
      Q => p_0_in(12)
    );
\others_value_reg[45]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(45),
      Q => p_0_in(13)
    );
\others_value_reg[46]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(46),
      Q => p_0_in(14)
    );
\others_value_reg[47]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(47),
      Q => p_0_in(15)
    );
\others_value_reg[48]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(48),
      Q => p_0_in(16)
    );
\others_value_reg[49]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(49),
      Q => p_0_in(17)
    );
\others_value_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(4),
      Q => \others_value_reg_n_0_[4]\
    );
\others_value_reg[50]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(50),
      Q => p_0_in(18)
    );
\others_value_reg[51]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(51),
      Q => p_0_in(19)
    );
\others_value_reg[52]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(52),
      Q => p_0_in(20)
    );
\others_value_reg[53]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(53),
      Q => p_0_in(21)
    );
\others_value_reg[54]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(54),
      Q => p_0_in(22)
    );
\others_value_reg[55]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(55),
      Q => p_0_in(23)
    );
\others_value_reg[56]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(56),
      Q => p_0_in(24)
    );
\others_value_reg[57]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(57),
      Q => p_0_in(25)
    );
\others_value_reg[58]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(58),
      Q => p_0_in(26)
    );
\others_value_reg[59]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(59),
      Q => p_0_in(27)
    );
\others_value_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(5),
      Q => \others_value_reg_n_0_[5]\
    );
\others_value_reg[60]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(60),
      Q => p_0_in(28)
    );
\others_value_reg[61]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(61),
      Q => p_0_in(29)
    );
\others_value_reg[62]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(62),
      Q => p_0_in(30)
    );
\others_value_reg[63]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(63),
      CLR => SR(0),
      D => p_6_out0_in(63),
      Q => p_0_in(31)
    );
\others_value_reg[64]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(64),
      Q => p_0_in0_in(0)
    );
\others_value_reg[65]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(65),
      Q => p_0_in0_in(1)
    );
\others_value_reg[66]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(66),
      Q => p_0_in0_in(2)
    );
\others_value_reg[67]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(67),
      Q => p_0_in0_in(3)
    );
\others_value_reg[68]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(68),
      Q => p_0_in0_in(4)
    );
\others_value_reg[69]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(69),
      Q => p_0_in0_in(5)
    );
\others_value_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(6),
      Q => \others_value_reg_n_0_[6]\
    );
\others_value_reg[70]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(70),
      Q => p_0_in0_in(6)
    );
\others_value_reg[71]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(71),
      Q => p_0_in0_in(7)
    );
\others_value_reg[72]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(72),
      Q => p_0_in0_in(8)
    );
\others_value_reg[73]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(73),
      Q => p_0_in0_in(9)
    );
\others_value_reg[74]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(74),
      Q => p_0_in0_in(10)
    );
\others_value_reg[75]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(75),
      Q => p_0_in0_in(11)
    );
\others_value_reg[76]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(76),
      Q => p_0_in0_in(12)
    );
\others_value_reg[77]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(77),
      Q => p_0_in0_in(13)
    );
\others_value_reg[78]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(78),
      Q => p_0_in0_in(14)
    );
\others_value_reg[79]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(79),
      Q => p_0_in0_in(15)
    );
\others_value_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(7),
      Q => \others_value_reg_n_0_[7]\
    );
\others_value_reg[80]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(80),
      Q => p_0_in0_in(16)
    );
\others_value_reg[81]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(81),
      Q => p_0_in0_in(17)
    );
\others_value_reg[82]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(82),
      Q => p_0_in0_in(18)
    );
\others_value_reg[83]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(83),
      Q => p_0_in0_in(19)
    );
\others_value_reg[84]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(84),
      Q => p_0_in0_in(20)
    );
\others_value_reg[85]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(85),
      Q => p_0_in0_in(21)
    );
\others_value_reg[86]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(86),
      Q => p_0_in0_in(22)
    );
\others_value_reg[87]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(87),
      Q => p_0_in0_in(23)
    );
\others_value_reg[88]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(88),
      Q => p_0_in0_in(24)
    );
\others_value_reg[89]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(89),
      Q => p_0_in0_in(25)
    );
\others_value_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(8),
      Q => \others_value_reg_n_0_[8]\
    );
\others_value_reg[90]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(90),
      Q => p_0_in0_in(26)
    );
\others_value_reg[91]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(91),
      Q => p_0_in0_in(27)
    );
\others_value_reg[92]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(92),
      Q => p_0_in0_in(28)
    );
\others_value_reg[93]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(93),
      Q => p_0_in0_in(29)
    );
\others_value_reg[94]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(94),
      Q => p_0_in0_in(30)
    );
\others_value_reg[95]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(95),
      CLR => SR(0),
      D => p_6_out0_in(95),
      Q => p_0_in0_in(31)
    );
\others_value_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => p_6_out(31),
      CLR => SR(0),
      D => p_6_out0_in(9),
      Q => \others_value_reg_n_0_[9]\
    );
\valid[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => DST_VALID_1,
      I1 => \^q\(0),
      I2 => \valid_reg_n_0_[0]\,
      O => \valid[0]_i_1_n_0\
    );
\valid[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => DST_VALID_2,
      I1 => \^q\(0),
      I2 => \valid_reg_n_0_[1]\,
      O => \valid[1]_i_1_n_0\
    );
\valid[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => DST_VALID_3,
      I1 => \^q\(0),
      I2 => \valid_reg_n_0_[2]\,
      O => \valid[2]_i_1_n_0\
    );
\valid_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \valid[0]_i_1_n_0\,
      Q => \valid_reg_n_0_[0]\
    );
\valid_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \valid[1]_i_1_n_0\,
      Q => \valid_reg_n_0_[1]\
    );
\valid_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => SR(0),
      D => \valid[2]_i_1_n_0\,
      Q => \valid_reg_n_0_[2]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_myip_4core_1_0_myip_4core_v1_0_S00_AXI is
  port (
    DST_ACK_1 : out STD_LOGIC;
    DST_ACK_2 : out STD_LOGIC;
    DST_ACK_3 : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    SRC_VALID_1 : out STD_LOGIC;
    SRC_FIN_1 : out STD_LOGIC;
    done : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    aw_en_reg_0 : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    SRC_DATA_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_VALID_1 : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    DST_VALID_2 : in STD_LOGIC;
    DST_VALID_3 : in STD_LOGIC;
    SRC_VALID_1_reg : in STD_LOGIC;
    SRC_FIN_1_reg : in STD_LOGIC;
    done_reg : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    aw_en_reg_1 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    SRC_ACK_3 : in STD_LOGIC;
    SRC_ACK_2 : in STD_LOGIC;
    SRC_ACK_1 : in STD_LOGIC;
    DST_FIN_3 : in STD_LOGIC;
    DST_FIN_2 : in STD_LOGIC;
    DST_FIN_1 : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DST_DATA_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_DATA_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_DATA_3 : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end design_1_myip_4core_1_0_myip_4core_v1_0_S00_AXI;

architecture STRUCTURE of design_1_myip_4core_1_0_myip_4core_v1_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal \^aw_en_reg_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg0__0\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg5 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg5[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg6 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg6[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg7[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal \slv_reg_wren__0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \slv_reg0[31]_i_2\ : label is "soft_lutpair88";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  aw_en_reg_0 <= \^aw_en_reg_0\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_reg_1,
      Q => \^aw_en_reg_0\,
      S => SR(0)
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      R => SR(0)
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      R => SR(0)
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      R => SR(0)
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => SR(0)
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => SR(0)
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => SR(0)
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => SR(0)
    );
axi_awready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => \^s_axi_awready\,
      I2 => \^aw_en_reg_0\,
      I3 => s00_axi_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => SR(0)
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => s00_axi_bvalid,
      R => SR(0)
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(0),
      I1 => slv_reg6(0),
      I2 => sel0(1),
      I3 => slv_reg5(0),
      I4 => sel0(0),
      I5 => slv_reg4(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(10),
      I1 => slv_reg6(10),
      I2 => sel0(1),
      I3 => slv_reg5(10),
      I4 => sel0(0),
      I5 => slv_reg4(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(11),
      I1 => slv_reg6(11),
      I2 => sel0(1),
      I3 => slv_reg5(11),
      I4 => sel0(0),
      I5 => slv_reg4(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(12),
      I1 => slv_reg6(12),
      I2 => sel0(1),
      I3 => slv_reg5(12),
      I4 => sel0(0),
      I5 => slv_reg4(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(13),
      I1 => slv_reg6(13),
      I2 => sel0(1),
      I3 => slv_reg5(13),
      I4 => sel0(0),
      I5 => slv_reg4(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(14),
      I1 => slv_reg6(14),
      I2 => sel0(1),
      I3 => slv_reg5(14),
      I4 => sel0(0),
      I5 => slv_reg4(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(15),
      I1 => slv_reg6(15),
      I2 => sel0(1),
      I3 => slv_reg5(15),
      I4 => sel0(0),
      I5 => slv_reg4(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(16),
      I1 => slv_reg6(16),
      I2 => sel0(1),
      I3 => slv_reg5(16),
      I4 => sel0(0),
      I5 => slv_reg4(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(17),
      I1 => slv_reg6(17),
      I2 => sel0(1),
      I3 => slv_reg5(17),
      I4 => sel0(0),
      I5 => slv_reg4(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(18),
      I1 => slv_reg6(18),
      I2 => sel0(1),
      I3 => slv_reg5(18),
      I4 => sel0(0),
      I5 => slv_reg4(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(19),
      I1 => slv_reg6(19),
      I2 => sel0(1),
      I3 => slv_reg5(19),
      I4 => sel0(0),
      I5 => slv_reg4(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(1),
      I1 => slv_reg6(1),
      I2 => sel0(1),
      I3 => slv_reg5(1),
      I4 => sel0(0),
      I5 => slv_reg4(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(20),
      I1 => slv_reg6(20),
      I2 => sel0(1),
      I3 => slv_reg5(20),
      I4 => sel0(0),
      I5 => slv_reg4(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(21),
      I1 => slv_reg6(21),
      I2 => sel0(1),
      I3 => slv_reg5(21),
      I4 => sel0(0),
      I5 => slv_reg4(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(22),
      I1 => slv_reg6(22),
      I2 => sel0(1),
      I3 => slv_reg5(22),
      I4 => sel0(0),
      I5 => slv_reg4(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(23),
      I1 => slv_reg6(23),
      I2 => sel0(1),
      I3 => slv_reg5(23),
      I4 => sel0(0),
      I5 => slv_reg4(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(24),
      I1 => slv_reg6(24),
      I2 => sel0(1),
      I3 => slv_reg5(24),
      I4 => sel0(0),
      I5 => slv_reg4(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(25),
      I1 => slv_reg6(25),
      I2 => sel0(1),
      I3 => slv_reg5(25),
      I4 => sel0(0),
      I5 => slv_reg4(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(26),
      I1 => slv_reg6(26),
      I2 => sel0(1),
      I3 => slv_reg5(26),
      I4 => sel0(0),
      I5 => slv_reg4(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(27),
      I1 => slv_reg6(27),
      I2 => sel0(1),
      I3 => slv_reg5(27),
      I4 => sel0(0),
      I5 => slv_reg4(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(28),
      I1 => slv_reg6(28),
      I2 => sel0(1),
      I3 => slv_reg5(28),
      I4 => sel0(0),
      I5 => slv_reg4(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(29),
      I1 => slv_reg6(29),
      I2 => sel0(1),
      I3 => slv_reg5(29),
      I4 => sel0(0),
      I5 => slv_reg4(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(2),
      I1 => slv_reg6(2),
      I2 => sel0(1),
      I3 => slv_reg5(2),
      I4 => sel0(0),
      I5 => slv_reg4(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(30),
      I1 => slv_reg6(30),
      I2 => sel0(1),
      I3 => slv_reg5(30),
      I4 => sel0(0),
      I5 => slv_reg4(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(31),
      I1 => slv_reg6(31),
      I2 => sel0(1),
      I3 => slv_reg5(31),
      I4 => sel0(0),
      I5 => slv_reg4(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(3),
      I1 => slv_reg6(3),
      I2 => sel0(1),
      I3 => slv_reg5(3),
      I4 => sel0(0),
      I5 => slv_reg4(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(4),
      I1 => slv_reg6(4),
      I2 => sel0(1),
      I3 => slv_reg5(4),
      I4 => sel0(0),
      I5 => slv_reg4(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(5),
      I1 => slv_reg6(5),
      I2 => sel0(1),
      I3 => slv_reg5(5),
      I4 => sel0(0),
      I5 => slv_reg4(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(6),
      I1 => slv_reg6(6),
      I2 => sel0(1),
      I3 => slv_reg5(6),
      I4 => sel0(0),
      I5 => slv_reg4(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(7),
      I1 => slv_reg6(7),
      I2 => sel0(1),
      I3 => slv_reg5(7),
      I4 => sel0(0),
      I5 => slv_reg4(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(8),
      I1 => slv_reg6(8),
      I2 => sel0(1),
      I3 => slv_reg5(8),
      I4 => sel0(0),
      I5 => slv_reg4(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(9),
      I1 => slv_reg6(9),
      I2 => sel0(1),
      I3 => slv_reg5(9),
      I4 => sel0(0),
      I5 => slv_reg4(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => SR(0)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => SR(0)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => SR(0)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => SR(0)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => SR(0)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => SR(0)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => SR(0)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => SR(0)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => SR(0)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => SR(0)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => SR(0)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => SR(0)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => SR(0)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => SR(0)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => SR(0)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => SR(0)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => SR(0)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => SR(0)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => SR(0)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => SR(0)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => SR(0)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => SR(0)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => SR(0)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => SR(0)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => SR(0)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => SR(0)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => SR(0)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => SR(0)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => SR(0)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => SR(0)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => SR(0)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => SR(0)
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^s00_axi_rvalid\,
      R => SR(0)
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_wready\,
      I3 => \^aw_en_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => SR(0)
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => \^s_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__0\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg0(0),
      R => SR(0)
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => \slv_reg0__0\(10),
      R => SR(0)
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => \slv_reg0__0\(11),
      R => SR(0)
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => \slv_reg0__0\(12),
      R => SR(0)
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => \slv_reg0__0\(13),
      R => SR(0)
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => \slv_reg0__0\(14),
      R => SR(0)
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => \slv_reg0__0\(15),
      R => SR(0)
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => \slv_reg0__0\(16),
      R => SR(0)
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => \slv_reg0__0\(17),
      R => SR(0)
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => \slv_reg0__0\(18),
      R => SR(0)
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => \slv_reg0__0\(19),
      R => SR(0)
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => \slv_reg0__0\(1),
      R => SR(0)
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => \slv_reg0__0\(20),
      R => SR(0)
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => \slv_reg0__0\(21),
      R => SR(0)
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => \slv_reg0__0\(22),
      R => SR(0)
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => \slv_reg0__0\(23),
      R => SR(0)
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => \slv_reg0__0\(24),
      R => SR(0)
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => \slv_reg0__0\(25),
      R => SR(0)
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => \slv_reg0__0\(26),
      R => SR(0)
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => \slv_reg0__0\(27),
      R => SR(0)
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => \slv_reg0__0\(28),
      R => SR(0)
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => \slv_reg0__0\(29),
      R => SR(0)
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => \slv_reg0__0\(2),
      R => SR(0)
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => \slv_reg0__0\(30),
      R => SR(0)
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => \slv_reg0__0\(31),
      R => SR(0)
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => \slv_reg0__0\(3),
      R => SR(0)
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => \slv_reg0__0\(4),
      R => SR(0)
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => \slv_reg0__0\(5),
      R => SR(0)
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => \slv_reg0__0\(6),
      R => SR(0)
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => \slv_reg0__0\(7),
      R => SR(0)
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => \slv_reg0__0\(8),
      R => SR(0)
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => \slv_reg0__0\(9),
      R => SR(0)
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => SR(0)
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => SR(0)
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => SR(0)
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => SR(0)
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => SR(0)
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => SR(0)
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => SR(0)
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => SR(0)
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => SR(0)
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => SR(0)
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => SR(0)
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => SR(0)
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => SR(0)
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => SR(0)
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => SR(0)
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => SR(0)
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => SR(0)
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => SR(0)
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => SR(0)
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => SR(0)
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => SR(0)
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => SR(0)
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => SR(0)
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => SR(0)
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => SR(0)
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => SR(0)
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => SR(0)
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => SR(0)
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => SR(0)
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => SR(0)
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => SR(0)
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => SR(0)
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg4(0),
      R => SR(0)
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg4(10),
      R => SR(0)
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg4(11),
      R => SR(0)
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg4(12),
      R => SR(0)
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg4(13),
      R => SR(0)
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg4(14),
      R => SR(0)
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg4(15),
      R => SR(0)
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg4(16),
      R => SR(0)
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg4(17),
      R => SR(0)
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg4(18),
      R => SR(0)
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg4(19),
      R => SR(0)
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg4(1),
      R => SR(0)
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg4(20),
      R => SR(0)
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg4(21),
      R => SR(0)
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg4(22),
      R => SR(0)
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg4(23),
      R => SR(0)
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg4(24),
      R => SR(0)
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg4(25),
      R => SR(0)
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg4(26),
      R => SR(0)
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg4(27),
      R => SR(0)
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg4(28),
      R => SR(0)
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg4(29),
      R => SR(0)
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg4(2),
      R => SR(0)
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg4(30),
      R => SR(0)
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg4(31),
      R => SR(0)
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg4(3),
      R => SR(0)
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg4(4),
      R => SR(0)
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg4(5),
      R => SR(0)
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg4(6),
      R => SR(0)
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg4(7),
      R => SR(0)
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg4(8),
      R => SR(0)
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg4(9),
      R => SR(0)
    );
\slv_reg5[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      O => \slv_reg5[15]_i_1_n_0\
    );
\slv_reg5[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      O => \slv_reg5[23]_i_1_n_0\
    );
\slv_reg5[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      O => \slv_reg5[31]_i_1_n_0\
    );
\slv_reg5[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      O => \slv_reg5[7]_i_1_n_0\
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg5(0),
      R => SR(0)
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg5(10),
      R => SR(0)
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg5(11),
      R => SR(0)
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg5(12),
      R => SR(0)
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg5(13),
      R => SR(0)
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg5(14),
      R => SR(0)
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg5(15),
      R => SR(0)
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg5(16),
      R => SR(0)
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg5(17),
      R => SR(0)
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg5(18),
      R => SR(0)
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg5(19),
      R => SR(0)
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg5(1),
      R => SR(0)
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg5(20),
      R => SR(0)
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg5(21),
      R => SR(0)
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg5(22),
      R => SR(0)
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg5(23),
      R => SR(0)
    );
\slv_reg5_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg5(24),
      R => SR(0)
    );
\slv_reg5_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg5(25),
      R => SR(0)
    );
\slv_reg5_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg5(26),
      R => SR(0)
    );
\slv_reg5_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg5(27),
      R => SR(0)
    );
\slv_reg5_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg5(28),
      R => SR(0)
    );
\slv_reg5_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg5(29),
      R => SR(0)
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg5(2),
      R => SR(0)
    );
\slv_reg5_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg5(30),
      R => SR(0)
    );
\slv_reg5_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg5(31),
      R => SR(0)
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg5(3),
      R => SR(0)
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg5(4),
      R => SR(0)
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg5(5),
      R => SR(0)
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg5(6),
      R => SR(0)
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg5(7),
      R => SR(0)
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg5(8),
      R => SR(0)
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg5(9),
      R => SR(0)
    );
\slv_reg6[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(2),
      O => \slv_reg6[15]_i_1_n_0\
    );
\slv_reg6[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(2),
      O => \slv_reg6[23]_i_1_n_0\
    );
\slv_reg6[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(2),
      O => \slv_reg6[31]_i_1_n_0\
    );
\slv_reg6[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(0),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(2),
      O => \slv_reg6[7]_i_1_n_0\
    );
\slv_reg6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg6(0),
      R => SR(0)
    );
\slv_reg6_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg6(10),
      R => SR(0)
    );
\slv_reg6_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg6(11),
      R => SR(0)
    );
\slv_reg6_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg6(12),
      R => SR(0)
    );
\slv_reg6_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg6(13),
      R => SR(0)
    );
\slv_reg6_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg6(14),
      R => SR(0)
    );
\slv_reg6_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg6(15),
      R => SR(0)
    );
\slv_reg6_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg6(16),
      R => SR(0)
    );
\slv_reg6_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg6(17),
      R => SR(0)
    );
\slv_reg6_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg6(18),
      R => SR(0)
    );
\slv_reg6_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg6(19),
      R => SR(0)
    );
\slv_reg6_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg6(1),
      R => SR(0)
    );
\slv_reg6_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg6(20),
      R => SR(0)
    );
\slv_reg6_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg6(21),
      R => SR(0)
    );
\slv_reg6_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg6(22),
      R => SR(0)
    );
\slv_reg6_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg6(23),
      R => SR(0)
    );
\slv_reg6_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg6(24),
      R => SR(0)
    );
\slv_reg6_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg6(25),
      R => SR(0)
    );
\slv_reg6_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg6(26),
      R => SR(0)
    );
\slv_reg6_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg6(27),
      R => SR(0)
    );
\slv_reg6_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg6(28),
      R => SR(0)
    );
\slv_reg6_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg6(29),
      R => SR(0)
    );
\slv_reg6_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg6(2),
      R => SR(0)
    );
\slv_reg6_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg6(30),
      R => SR(0)
    );
\slv_reg6_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg6(31),
      R => SR(0)
    );
\slv_reg6_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg6(3),
      R => SR(0)
    );
\slv_reg6_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg6(4),
      R => SR(0)
    );
\slv_reg6_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg6(5),
      R => SR(0)
    );
\slv_reg6_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg6(6),
      R => SR(0)
    );
\slv_reg6_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg6(7),
      R => SR(0)
    );
\slv_reg6_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg6(8),
      R => SR(0)
    );
\slv_reg6_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg6(9),
      R => SR(0)
    );
\slv_reg7[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg7[15]_i_1_n_0\
    );
\slv_reg7[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg7[23]_i_1_n_0\
    );
\slv_reg7[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg7[31]_i_1_n_0\
    );
\slv_reg7[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg7[7]_i_1_n_0\
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg7(0),
      R => SR(0)
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg7(10),
      R => SR(0)
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg7(11),
      R => SR(0)
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg7(12),
      R => SR(0)
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg7(13),
      R => SR(0)
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg7(14),
      R => SR(0)
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg7(15),
      R => SR(0)
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg7(16),
      R => SR(0)
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg7(17),
      R => SR(0)
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg7(18),
      R => SR(0)
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg7(19),
      R => SR(0)
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg7(1),
      R => SR(0)
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg7(20),
      R => SR(0)
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg7(21),
      R => SR(0)
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg7(22),
      R => SR(0)
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg7(23),
      R => SR(0)
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg7(24),
      R => SR(0)
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg7(25),
      R => SR(0)
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg7(26),
      R => SR(0)
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg7(27),
      R => SR(0)
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg7(28),
      R => SR(0)
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg7(29),
      R => SR(0)
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg7(2),
      R => SR(0)
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg7(30),
      R => SR(0)
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg7(31),
      R => SR(0)
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg7(3),
      R => SR(0)
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg7(4),
      R => SR(0)
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg7(5),
      R => SR(0)
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg7(6),
      R => SR(0)
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg7(7),
      R => SR(0)
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg7(8),
      R => SR(0)
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg7(9),
      R => SR(0)
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_rvalid\,
      I2 => \^s_axi_arready\,
      O => \slv_reg_rden__0\
    );
u_sample: entity work.design_1_myip_4core_1_0_sample_for4
     port map (
      D(31 downto 0) => reg_data_out(31 downto 0),
      DST_ACK_1 => DST_ACK_1,
      DST_ACK_2 => DST_ACK_2,
      DST_ACK_3 => DST_ACK_3,
      DST_DATA_1(31 downto 0) => DST_DATA_1(31 downto 0),
      DST_DATA_2(31 downto 0) => DST_DATA_2(31 downto 0),
      DST_DATA_3(31 downto 0) => DST_DATA_3(31 downto 0),
      DST_FIN_1 => DST_FIN_1,
      DST_FIN_2 => DST_FIN_2,
      DST_FIN_3 => DST_FIN_3,
      DST_VALID_1 => DST_VALID_1,
      DST_VALID_2 => DST_VALID_2,
      DST_VALID_3 => DST_VALID_3,
      Q(4 downto 0) => Q(4 downto 0),
      SR(0) => SR(0),
      SRC_ACK_1 => SRC_ACK_1,
      SRC_ACK_2 => SRC_ACK_2,
      SRC_ACK_3 => SRC_ACK_3,
      SRC_DATA_1(31 downto 0) => SRC_DATA_1(31 downto 0),
      SRC_FIN_1 => SRC_FIN_1,
      SRC_FIN_1_reg_0 => SRC_FIN_1_reg,
      SRC_VALID_1 => SRC_VALID_1,
      SRC_VALID_1_reg_0 => SRC_VALID_1_reg,
      \axi_rdata_reg[0]\(2 downto 0) => sel0(2 downto 0),
      \axi_rdata_reg[0]_0\ => \axi_rdata[0]_i_3_n_0\,
      \axi_rdata_reg[10]\ => \axi_rdata[10]_i_3_n_0\,
      \axi_rdata_reg[11]\ => \axi_rdata[11]_i_3_n_0\,
      \axi_rdata_reg[12]\ => \axi_rdata[12]_i_3_n_0\,
      \axi_rdata_reg[13]\ => \axi_rdata[13]_i_3_n_0\,
      \axi_rdata_reg[14]\ => \axi_rdata[14]_i_3_n_0\,
      \axi_rdata_reg[15]\ => \axi_rdata[15]_i_3_n_0\,
      \axi_rdata_reg[16]\ => \axi_rdata[16]_i_3_n_0\,
      \axi_rdata_reg[17]\ => \axi_rdata[17]_i_3_n_0\,
      \axi_rdata_reg[18]\ => \axi_rdata[18]_i_3_n_0\,
      \axi_rdata_reg[19]\ => \axi_rdata[19]_i_3_n_0\,
      \axi_rdata_reg[1]\ => \axi_rdata[1]_i_3_n_0\,
      \axi_rdata_reg[20]\ => \axi_rdata[20]_i_3_n_0\,
      \axi_rdata_reg[21]\ => \axi_rdata[21]_i_3_n_0\,
      \axi_rdata_reg[22]\ => \axi_rdata[22]_i_3_n_0\,
      \axi_rdata_reg[23]\ => \axi_rdata[23]_i_3_n_0\,
      \axi_rdata_reg[24]\ => \axi_rdata[24]_i_3_n_0\,
      \axi_rdata_reg[25]\ => \axi_rdata[25]_i_3_n_0\,
      \axi_rdata_reg[26]\ => \axi_rdata[26]_i_3_n_0\,
      \axi_rdata_reg[27]\ => \axi_rdata[27]_i_3_n_0\,
      \axi_rdata_reg[28]\ => \axi_rdata[28]_i_3_n_0\,
      \axi_rdata_reg[29]\ => \axi_rdata[29]_i_3_n_0\,
      \axi_rdata_reg[2]\ => \axi_rdata[2]_i_3_n_0\,
      \axi_rdata_reg[30]\ => \axi_rdata[30]_i_3_n_0\,
      \axi_rdata_reg[31]\(31 downto 1) => \slv_reg0__0\(31 downto 1),
      \axi_rdata_reg[31]\(0) => slv_reg0(0),
      \axi_rdata_reg[31]_0\ => \axi_rdata[31]_i_3_n_0\,
      \axi_rdata_reg[3]\ => \axi_rdata[3]_i_3_n_0\,
      \axi_rdata_reg[4]\ => \axi_rdata[4]_i_3_n_0\,
      \axi_rdata_reg[5]\ => \axi_rdata[5]_i_3_n_0\,
      \axi_rdata_reg[6]\ => \axi_rdata[6]_i_3_n_0\,
      \axi_rdata_reg[7]\ => \axi_rdata[7]_i_3_n_0\,
      \axi_rdata_reg[8]\ => \axi_rdata[8]_i_3_n_0\,
      \axi_rdata_reg[9]\ => \axi_rdata[9]_i_3_n_0\,
      done => done,
      done_reg_0 => done_reg,
      \my_value_reg[31]_0\(31 downto 0) => slv_reg1(31 downto 0),
      s00_axi_aclk => s00_axi_aclk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_myip_4core_1_0_myip_4core_v1_0 is
  port (
    DST_ACK_1 : out STD_LOGIC;
    SRC_DATA_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_2 : out STD_LOGIC;
    DST_ACK_3 : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    SRC_VALID_1 : out STD_LOGIC;
    SRC_FIN_1 : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    DST_VALID_1 : in STD_LOGIC;
    SRC_ACK_3 : in STD_LOGIC;
    SRC_ACK_2 : in STD_LOGIC;
    SRC_ACK_1 : in STD_LOGIC;
    DST_VALID_3 : in STD_LOGIC;
    DST_FIN_3 : in STD_LOGIC;
    DST_VALID_2 : in STD_LOGIC;
    DST_FIN_2 : in STD_LOGIC;
    DST_FIN_1 : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    DST_DATA_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_DATA_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_DATA_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end design_1_myip_4core_1_0_myip_4core_v1_0;

architecture STRUCTURE of design_1_myip_4core_1_0_myip_4core_v1_0 is
  signal DST_ACK_1_i_1_n_0 : STD_LOGIC;
  signal \^src_fin_1\ : STD_LOGIC;
  signal SRC_FIN_1_i_1_n_0 : STD_LOGIC;
  signal \^src_valid_1\ : STD_LOGIC;
  signal SRC_VALID_1_i_1_n_0 : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal done : STD_LOGIC;
  signal done_i_1_n_0 : STD_LOGIC;
  signal myip_4core_v1_0_S00_AXI_inst_n_10 : STD_LOGIC;
  signal myip_4core_v1_0_S00_AXI_inst_n_12 : STD_LOGIC;
  signal myip_4core_v1_0_S00_AXI_inst_n_13 : STD_LOGIC;
  signal myip_4core_v1_0_S00_AXI_inst_n_14 : STD_LOGIC;
  signal myip_4core_v1_0_S00_AXI_inst_n_15 : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \u_sample/p_1_in\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of SRC_FIN_1_i_1 : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of done_i_1 : label is "soft_lutpair89";
begin
  SRC_FIN_1 <= \^src_fin_1\;
  SRC_VALID_1 <= \^src_valid_1\;
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
DST_ACK_1_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => DST_ACK_1_i_1_n_0
    );
SRC_FIN_1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DC"
    )
        port map (
      I0 => \u_sample/p_1_in\,
      I1 => myip_4core_v1_0_S00_AXI_inst_n_13,
      I2 => \^src_fin_1\,
      O => SRC_FIN_1_i_1_n_0
    );
SRC_VALID_1_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABAA"
    )
        port map (
      I0 => myip_4core_v1_0_S00_AXI_inst_n_15,
      I1 => \u_sample/p_1_in\,
      I2 => myip_4core_v1_0_S00_AXI_inst_n_14,
      I3 => \^src_valid_1\,
      O => SRC_VALID_1_i_1_n_0
    );
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC4CCC4CCC4CC"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => myip_4core_v1_0_S00_AXI_inst_n_10,
      I2 => \^s_axi_awready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_awready\,
      I3 => \^s_axi_wready\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
done_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DC"
    )
        port map (
      I0 => \u_sample/p_1_in\,
      I1 => myip_4core_v1_0_S00_AXI_inst_n_12,
      I2 => done,
      O => done_i_1_n_0
    );
myip_4core_v1_0_S00_AXI_inst: entity work.design_1_myip_4core_1_0_myip_4core_v1_0_S00_AXI
     port map (
      DST_ACK_1 => DST_ACK_1,
      DST_ACK_2 => DST_ACK_2,
      DST_ACK_3 => DST_ACK_3,
      DST_DATA_1(31 downto 0) => DST_DATA_1(31 downto 0),
      DST_DATA_2(31 downto 0) => DST_DATA_2(31 downto 0),
      DST_DATA_3(31 downto 0) => DST_DATA_3(31 downto 0),
      DST_FIN_1 => DST_FIN_1,
      DST_FIN_2 => DST_FIN_2,
      DST_FIN_3 => DST_FIN_3,
      DST_VALID_1 => DST_VALID_1,
      DST_VALID_2 => DST_VALID_2,
      DST_VALID_3 => DST_VALID_3,
      Q(4) => myip_4core_v1_0_S00_AXI_inst_n_12,
      Q(3) => myip_4core_v1_0_S00_AXI_inst_n_13,
      Q(2) => myip_4core_v1_0_S00_AXI_inst_n_14,
      Q(1) => myip_4core_v1_0_S00_AXI_inst_n_15,
      Q(0) => \u_sample/p_1_in\,
      SR(0) => DST_ACK_1_i_1_n_0,
      SRC_ACK_1 => SRC_ACK_1,
      SRC_ACK_2 => SRC_ACK_2,
      SRC_ACK_3 => SRC_ACK_3,
      SRC_DATA_1(31 downto 0) => SRC_DATA_1(31 downto 0),
      SRC_FIN_1 => \^src_fin_1\,
      SRC_FIN_1_reg => SRC_FIN_1_i_1_n_0,
      SRC_VALID_1 => \^src_valid_1\,
      SRC_VALID_1_reg => SRC_VALID_1_i_1_n_0,
      S_AXI_ARREADY => \^s_axi_arready\,
      S_AXI_AWREADY => \^s_axi_awready\,
      S_AXI_WREADY => \^s_axi_wready\,
      aw_en_reg_0 => myip_4core_v1_0_S00_AXI_inst_n_10,
      aw_en_reg_1 => aw_en_i_1_n_0,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      done => done,
      done_reg => done_i_1_n_0,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bvalid => \^s00_axi_bvalid\,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rvalid => \^s00_axi_rvalid\,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_myip_4core_1_0 is
  port (
    DST_VALID_1 : in STD_LOGIC;
    DST_DATA_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_1 : out STD_LOGIC;
    DST_FIN_1 : in STD_LOGIC;
    SRC_VALID_1 : out STD_LOGIC;
    SRC_DATA_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_ACK_1 : in STD_LOGIC;
    SRC_FIN_1 : out STD_LOGIC;
    DST_VALID_2 : in STD_LOGIC;
    DST_DATA_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_2 : out STD_LOGIC;
    DST_FIN_2 : in STD_LOGIC;
    SRC_VALID_2 : out STD_LOGIC;
    SRC_DATA_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_ACK_2 : in STD_LOGIC;
    SRC_FIN_2 : out STD_LOGIC;
    DST_VALID_3 : in STD_LOGIC;
    DST_DATA_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_3 : out STD_LOGIC;
    DST_FIN_3 : in STD_LOGIC;
    SRC_VALID_3 : out STD_LOGIC;
    SRC_DATA_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_ACK_3 : in STD_LOGIC;
    SRC_FIN_3 : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_myip_4core_1_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_myip_4core_1_0 : entity is "design_1_myip_4core_0_1,myip_4core_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_myip_4core_1_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_myip_4core_1_0 : entity is "myip_4core_v1_0,Vivado 2018.3";
end design_1_myip_4core_1_0;

architecture STRUCTURE of design_1_myip_4core_1_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^src_data_1\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^src_fin_1\ : STD_LOGIC;
  signal \^src_valid_1\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  SRC_DATA_1(31 downto 0) <= \^src_data_1\(31 downto 0);
  SRC_DATA_2(31 downto 0) <= \^src_data_1\(31 downto 0);
  SRC_DATA_3(31 downto 0) <= \^src_data_1\(31 downto 0);
  SRC_FIN_1 <= \^src_fin_1\;
  SRC_FIN_2 <= \^src_fin_1\;
  SRC_FIN_3 <= \^src_fin_1\;
  SRC_VALID_1 <= \^src_valid_1\;
  SRC_VALID_2 <= \^src_valid_1\;
  SRC_VALID_3 <= \^src_valid_1\;
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.design_1_myip_4core_1_0_myip_4core_v1_0
     port map (
      DST_ACK_1 => DST_ACK_1,
      DST_ACK_2 => DST_ACK_2,
      DST_ACK_3 => DST_ACK_3,
      DST_DATA_1(31 downto 0) => DST_DATA_1(31 downto 0),
      DST_DATA_2(31 downto 0) => DST_DATA_2(31 downto 0),
      DST_DATA_3(31 downto 0) => DST_DATA_3(31 downto 0),
      DST_FIN_1 => DST_FIN_1,
      DST_FIN_2 => DST_FIN_2,
      DST_FIN_3 => DST_FIN_3,
      DST_VALID_1 => DST_VALID_1,
      DST_VALID_2 => DST_VALID_2,
      DST_VALID_3 => DST_VALID_3,
      SRC_ACK_1 => SRC_ACK_1,
      SRC_ACK_2 => SRC_ACK_2,
      SRC_ACK_3 => SRC_ACK_3,
      SRC_DATA_1(31 downto 0) => \^src_data_1\(31 downto 0),
      SRC_FIN_1 => \^src_fin_1\,
      SRC_VALID_1 => \^src_valid_1\,
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
