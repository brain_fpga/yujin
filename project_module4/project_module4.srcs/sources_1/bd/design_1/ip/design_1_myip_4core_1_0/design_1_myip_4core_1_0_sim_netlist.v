// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Tue Jan 21 16:47:33 2020
// Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_myip_4core_1_0 -prefix
//               design_1_myip_4core_1_0_ design_1_myip_4core_0_1_sim_netlist.v
// Design      : design_1_myip_4core_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_myip_4core_0_1,myip_4core_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "myip_4core_v1_0,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module design_1_myip_4core_1_0
   (DST_VALID_1,
    DST_DATA_1,
    DST_ACK_1,
    DST_FIN_1,
    SRC_VALID_1,
    SRC_DATA_1,
    SRC_ACK_1,
    SRC_FIN_1,
    DST_VALID_2,
    DST_DATA_2,
    DST_ACK_2,
    DST_FIN_2,
    SRC_VALID_2,
    SRC_DATA_2,
    SRC_ACK_2,
    SRC_FIN_2,
    DST_VALID_3,
    DST_DATA_3,
    DST_ACK_3,
    DST_FIN_3,
    SRC_VALID_3,
    SRC_DATA_3,
    SRC_ACK_3,
    SRC_FIN_3,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input DST_VALID_1;
  input [31:0]DST_DATA_1;
  output DST_ACK_1;
  input DST_FIN_1;
  output SRC_VALID_1;
  output [31:0]SRC_DATA_1;
  input SRC_ACK_1;
  output SRC_FIN_1;
  input DST_VALID_2;
  input [31:0]DST_DATA_2;
  output DST_ACK_2;
  input DST_FIN_2;
  output SRC_VALID_2;
  output [31:0]SRC_DATA_2;
  input SRC_ACK_2;
  output SRC_FIN_2;
  input DST_VALID_3;
  input [31:0]DST_DATA_3;
  output DST_ACK_3;
  input DST_FIN_3;
  output SRC_VALID_3;
  output [31:0]SRC_DATA_3;
  input SRC_ACK_3;
  output SRC_FIN_3;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [4:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire DST_ACK_1;
  wire DST_ACK_2;
  wire DST_ACK_3;
  wire [31:0]DST_DATA_1;
  wire [31:0]DST_DATA_2;
  wire [31:0]DST_DATA_3;
  wire DST_FIN_1;
  wire DST_FIN_2;
  wire DST_FIN_3;
  wire DST_VALID_1;
  wire DST_VALID_2;
  wire DST_VALID_3;
  wire SRC_ACK_1;
  wire SRC_ACK_2;
  wire SRC_ACK_3;
  wire [31:0]SRC_DATA_1;
  wire SRC_FIN_1;
  wire SRC_VALID_1;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign SRC_DATA_2[31:0] = SRC_DATA_1;
  assign SRC_DATA_3[31:0] = SRC_DATA_1;
  assign SRC_FIN_2 = SRC_FIN_1;
  assign SRC_FIN_3 = SRC_FIN_1;
  assign SRC_VALID_2 = SRC_VALID_1;
  assign SRC_VALID_3 = SRC_VALID_1;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_myip_4core_1_0_myip_4core_v1_0 inst
       (.DST_ACK_1(DST_ACK_1),
        .DST_ACK_2(DST_ACK_2),
        .DST_ACK_3(DST_ACK_3),
        .DST_DATA_1(DST_DATA_1),
        .DST_DATA_2(DST_DATA_2),
        .DST_DATA_3(DST_DATA_3),
        .DST_FIN_1(DST_FIN_1),
        .DST_FIN_2(DST_FIN_2),
        .DST_FIN_3(DST_FIN_3),
        .DST_VALID_1(DST_VALID_1),
        .DST_VALID_2(DST_VALID_2),
        .DST_VALID_3(DST_VALID_3),
        .SRC_ACK_1(SRC_ACK_1),
        .SRC_ACK_2(SRC_ACK_2),
        .SRC_ACK_3(SRC_ACK_3),
        .SRC_DATA_1(SRC_DATA_1),
        .SRC_FIN_1(SRC_FIN_1),
        .SRC_VALID_1(SRC_VALID_1),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module design_1_myip_4core_1_0_myip_4core_v1_0
   (DST_ACK_1,
    SRC_DATA_1,
    DST_ACK_2,
    DST_ACK_3,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    SRC_VALID_1,
    SRC_FIN_1,
    s00_axi_bvalid,
    s00_axi_aclk,
    DST_VALID_1,
    SRC_ACK_3,
    SRC_ACK_2,
    SRC_ACK_1,
    DST_VALID_3,
    DST_FIN_3,
    DST_VALID_2,
    DST_FIN_2,
    DST_FIN_1,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    DST_DATA_1,
    DST_DATA_2,
    DST_DATA_3,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output DST_ACK_1;
  output [31:0]SRC_DATA_1;
  output DST_ACK_2;
  output DST_ACK_3;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output SRC_VALID_1;
  output SRC_FIN_1;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input DST_VALID_1;
  input SRC_ACK_3;
  input SRC_ACK_2;
  input SRC_ACK_1;
  input DST_VALID_3;
  input DST_FIN_3;
  input DST_VALID_2;
  input DST_FIN_2;
  input DST_FIN_1;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [31:0]DST_DATA_1;
  input [31:0]DST_DATA_2;
  input [31:0]DST_DATA_3;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire DST_ACK_1;
  wire DST_ACK_1_i_1_n_0;
  wire DST_ACK_2;
  wire DST_ACK_3;
  wire [31:0]DST_DATA_1;
  wire [31:0]DST_DATA_2;
  wire [31:0]DST_DATA_3;
  wire DST_FIN_1;
  wire DST_FIN_2;
  wire DST_FIN_3;
  wire DST_VALID_1;
  wire DST_VALID_2;
  wire DST_VALID_3;
  wire SRC_ACK_1;
  wire SRC_ACK_2;
  wire SRC_ACK_3;
  wire [31:0]SRC_DATA_1;
  wire SRC_FIN_1;
  wire SRC_FIN_1_i_1_n_0;
  wire SRC_VALID_1;
  wire SRC_VALID_1_i_1_n_0;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire done;
  wire done_i_1_n_0;
  wire myip_4core_v1_0_S00_AXI_inst_n_10;
  wire myip_4core_v1_0_S00_AXI_inst_n_12;
  wire myip_4core_v1_0_S00_AXI_inst_n_13;
  wire myip_4core_v1_0_S00_AXI_inst_n_14;
  wire myip_4core_v1_0_S00_AXI_inst_n_15;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire \u_sample/p_1_in ;

  LUT1 #(
    .INIT(2'h1)) 
    DST_ACK_1_i_1
       (.I0(s00_axi_aresetn),
        .O(DST_ACK_1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hDC)) 
    SRC_FIN_1_i_1
       (.I0(\u_sample/p_1_in ),
        .I1(myip_4core_v1_0_S00_AXI_inst_n_13),
        .I2(SRC_FIN_1),
        .O(SRC_FIN_1_i_1_n_0));
  LUT4 #(
    .INIT(16'hABAA)) 
    SRC_VALID_1_i_1
       (.I0(myip_4core_v1_0_S00_AXI_inst_n_15),
        .I1(\u_sample/p_1_in ),
        .I2(myip_4core_v1_0_S00_AXI_inst_n_14),
        .I3(SRC_VALID_1),
        .O(SRC_VALID_1_i_1_n_0));
  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(myip_4core_v1_0_S00_AXI_inst_n_10),
        .I2(S_AXI_AWREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WREADY),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hDC)) 
    done_i_1
       (.I0(\u_sample/p_1_in ),
        .I1(myip_4core_v1_0_S00_AXI_inst_n_12),
        .I2(done),
        .O(done_i_1_n_0));
  design_1_myip_4core_1_0_myip_4core_v1_0_S00_AXI myip_4core_v1_0_S00_AXI_inst
       (.DST_ACK_1(DST_ACK_1),
        .DST_ACK_2(DST_ACK_2),
        .DST_ACK_3(DST_ACK_3),
        .DST_DATA_1(DST_DATA_1),
        .DST_DATA_2(DST_DATA_2),
        .DST_DATA_3(DST_DATA_3),
        .DST_FIN_1(DST_FIN_1),
        .DST_FIN_2(DST_FIN_2),
        .DST_FIN_3(DST_FIN_3),
        .DST_VALID_1(DST_VALID_1),
        .DST_VALID_2(DST_VALID_2),
        .DST_VALID_3(DST_VALID_3),
        .Q({myip_4core_v1_0_S00_AXI_inst_n_12,myip_4core_v1_0_S00_AXI_inst_n_13,myip_4core_v1_0_S00_AXI_inst_n_14,myip_4core_v1_0_S00_AXI_inst_n_15,\u_sample/p_1_in }),
        .SR(DST_ACK_1_i_1_n_0),
        .SRC_ACK_1(SRC_ACK_1),
        .SRC_ACK_2(SRC_ACK_2),
        .SRC_ACK_3(SRC_ACK_3),
        .SRC_DATA_1(SRC_DATA_1),
        .SRC_FIN_1(SRC_FIN_1),
        .SRC_FIN_1_reg(SRC_FIN_1_i_1_n_0),
        .SRC_VALID_1(SRC_VALID_1),
        .SRC_VALID_1_reg(SRC_VALID_1_i_1_n_0),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .aw_en_reg_0(myip_4core_v1_0_S00_AXI_inst_n_10),
        .aw_en_reg_1(aw_en_i_1_n_0),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .done(done),
        .done_reg(done_i_1_n_0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module design_1_myip_4core_1_0_myip_4core_v1_0_S00_AXI
   (DST_ACK_1,
    DST_ACK_2,
    DST_ACK_3,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    SRC_VALID_1,
    SRC_FIN_1,
    done,
    s00_axi_bvalid,
    aw_en_reg_0,
    s00_axi_rvalid,
    Q,
    SRC_DATA_1,
    s00_axi_rdata,
    DST_VALID_1,
    s00_axi_aclk,
    SR,
    DST_VALID_2,
    DST_VALID_3,
    SRC_VALID_1_reg,
    SRC_FIN_1_reg,
    done_reg,
    axi_bvalid_reg_0,
    aw_en_reg_1,
    axi_rvalid_reg_0,
    SRC_ACK_3,
    SRC_ACK_2,
    SRC_ACK_1,
    DST_FIN_3,
    DST_FIN_2,
    DST_FIN_1,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    DST_DATA_1,
    DST_DATA_2,
    DST_DATA_3);
  output DST_ACK_1;
  output DST_ACK_2;
  output DST_ACK_3;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output SRC_VALID_1;
  output SRC_FIN_1;
  output done;
  output s00_axi_bvalid;
  output aw_en_reg_0;
  output s00_axi_rvalid;
  output [4:0]Q;
  output [31:0]SRC_DATA_1;
  output [31:0]s00_axi_rdata;
  input DST_VALID_1;
  input s00_axi_aclk;
  input [0:0]SR;
  input DST_VALID_2;
  input DST_VALID_3;
  input SRC_VALID_1_reg;
  input SRC_FIN_1_reg;
  input done_reg;
  input axi_bvalid_reg_0;
  input aw_en_reg_1;
  input axi_rvalid_reg_0;
  input SRC_ACK_3;
  input SRC_ACK_2;
  input SRC_ACK_1;
  input DST_FIN_3;
  input DST_FIN_2;
  input DST_FIN_1;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input [31:0]DST_DATA_1;
  input [31:0]DST_DATA_2;
  input [31:0]DST_DATA_3;

  wire DST_ACK_1;
  wire DST_ACK_2;
  wire DST_ACK_3;
  wire [31:0]DST_DATA_1;
  wire [31:0]DST_DATA_2;
  wire [31:0]DST_DATA_3;
  wire DST_FIN_1;
  wire DST_FIN_2;
  wire DST_FIN_3;
  wire DST_VALID_1;
  wire DST_VALID_2;
  wire DST_VALID_3;
  wire [4:0]Q;
  wire [0:0]SR;
  wire SRC_ACK_1;
  wire SRC_ACK_2;
  wire SRC_ACK_3;
  wire [31:0]SRC_DATA_1;
  wire SRC_FIN_1;
  wire SRC_FIN_1_reg;
  wire SRC_VALID_1;
  wire SRC_VALID_1_reg;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_reg_0;
  wire aw_en_reg_1;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire done;
  wire done_reg;
  wire [2:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire [0:0]slv_reg0;
  wire [31:1]slv_reg0__0;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [31:0]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [31:0]slv_reg6;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;

  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_reg_1),
        .Q(aw_en_reg_0),
        .S(SR));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .R(SR));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .R(SR));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .R(SR));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(SR));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(SR));
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_1
       (.I0(s00_axi_wvalid),
        .I1(S_AXI_AWREADY),
        .I2(aw_en_reg_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(SR));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(s00_axi_bvalid),
        .R(SR));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg7[0]),
        .I1(slv_reg6[0]),
        .I2(sel0[1]),
        .I3(slv_reg5[0]),
        .I4(sel0[0]),
        .I5(slv_reg4[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg7[10]),
        .I1(slv_reg6[10]),
        .I2(sel0[1]),
        .I3(slv_reg5[10]),
        .I4(sel0[0]),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg7[11]),
        .I1(slv_reg6[11]),
        .I2(sel0[1]),
        .I3(slv_reg5[11]),
        .I4(sel0[0]),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg7[12]),
        .I1(slv_reg6[12]),
        .I2(sel0[1]),
        .I3(slv_reg5[12]),
        .I4(sel0[0]),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg7[13]),
        .I1(slv_reg6[13]),
        .I2(sel0[1]),
        .I3(slv_reg5[13]),
        .I4(sel0[0]),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg7[14]),
        .I1(slv_reg6[14]),
        .I2(sel0[1]),
        .I3(slv_reg5[14]),
        .I4(sel0[0]),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg7[15]),
        .I1(slv_reg6[15]),
        .I2(sel0[1]),
        .I3(slv_reg5[15]),
        .I4(sel0[0]),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(slv_reg7[16]),
        .I1(slv_reg6[16]),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(sel0[0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(slv_reg7[17]),
        .I1(slv_reg6[17]),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(sel0[0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_3 
       (.I0(slv_reg7[18]),
        .I1(slv_reg6[18]),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(sel0[0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg7[19]),
        .I1(slv_reg6[19]),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(sel0[0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(slv_reg7[1]),
        .I1(slv_reg6[1]),
        .I2(sel0[1]),
        .I3(slv_reg5[1]),
        .I4(sel0[0]),
        .I5(slv_reg4[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_3 
       (.I0(slv_reg7[20]),
        .I1(slv_reg6[20]),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(sel0[0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(slv_reg7[21]),
        .I1(slv_reg6[21]),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(sel0[0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_3 
       (.I0(slv_reg7[22]),
        .I1(slv_reg6[22]),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(sel0[0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_3 
       (.I0(slv_reg7[23]),
        .I1(slv_reg6[23]),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(sel0[0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(slv_reg7[24]),
        .I1(slv_reg6[24]),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(sel0[0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(slv_reg7[25]),
        .I1(slv_reg6[25]),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(sel0[0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_3 
       (.I0(slv_reg7[26]),
        .I1(slv_reg6[26]),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(sel0[0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg7[27]),
        .I1(slv_reg6[27]),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(sel0[0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(slv_reg7[28]),
        .I1(slv_reg6[28]),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(sel0[0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_3 
       (.I0(slv_reg7[29]),
        .I1(slv_reg6[29]),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(sel0[0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg7[2]),
        .I1(slv_reg6[2]),
        .I2(sel0[1]),
        .I3(slv_reg5[2]),
        .I4(sel0[0]),
        .I5(slv_reg4[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(slv_reg7[30]),
        .I1(slv_reg6[30]),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(sel0[0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(slv_reg7[31]),
        .I1(slv_reg6[31]),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(sel0[0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg7[3]),
        .I1(slv_reg6[3]),
        .I2(sel0[1]),
        .I3(slv_reg5[3]),
        .I4(sel0[0]),
        .I5(slv_reg4[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg7[4]),
        .I1(slv_reg6[4]),
        .I2(sel0[1]),
        .I3(slv_reg5[4]),
        .I4(sel0[0]),
        .I5(slv_reg4[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg7[5]),
        .I1(slv_reg6[5]),
        .I2(sel0[1]),
        .I3(slv_reg5[5]),
        .I4(sel0[0]),
        .I5(slv_reg4[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg7[6]),
        .I1(slv_reg6[6]),
        .I2(sel0[1]),
        .I3(slv_reg5[6]),
        .I4(sel0[0]),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg7[7]),
        .I1(slv_reg6[7]),
        .I2(sel0[1]),
        .I3(slv_reg5[7]),
        .I4(sel0[0]),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg7[8]),
        .I1(slv_reg6[8]),
        .I2(sel0[1]),
        .I3(slv_reg5[8]),
        .I4(sel0[0]),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg7[9]),
        .I1(slv_reg6[9]),
        .I2(sel0[1]),
        .I3(slv_reg5[9]),
        .I4(sel0[0]),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(SR));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(SR));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(SR));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(SR));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(SR));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(SR));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(SR));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(SR));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(SR));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(SR));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(SR));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(SR));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(SR));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(SR));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(SR));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(SR));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(SR));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(SR));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(SR));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(SR));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(SR));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(SR));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(SR));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(SR));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(SR));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(SR));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(SR));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(SR));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(SR));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(SR));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(SR));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(SR));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(s00_axi_rvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .I3(aw_en_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(SR));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(S_AXI_WREADY),
        .I1(S_AXI_AWREADY),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0),
        .R(SR));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0__0[10]),
        .R(SR));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0__0[11]),
        .R(SR));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0__0[12]),
        .R(SR));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0__0[13]),
        .R(SR));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0__0[14]),
        .R(SR));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0__0[15]),
        .R(SR));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0__0[16]),
        .R(SR));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0__0[17]),
        .R(SR));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0__0[18]),
        .R(SR));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0__0[19]),
        .R(SR));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0__0[1]),
        .R(SR));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0__0[20]),
        .R(SR));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0__0[21]),
        .R(SR));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0__0[22]),
        .R(SR));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0__0[23]),
        .R(SR));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0__0[24]),
        .R(SR));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0__0[25]),
        .R(SR));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0__0[26]),
        .R(SR));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0__0[27]),
        .R(SR));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0__0[28]),
        .R(SR));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0__0[29]),
        .R(SR));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0__0[2]),
        .R(SR));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0__0[30]),
        .R(SR));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0__0[31]),
        .R(SR));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0__0[3]),
        .R(SR));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0__0[4]),
        .R(SR));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0__0[5]),
        .R(SR));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0__0[6]),
        .R(SR));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0__0[7]),
        .R(SR));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0__0[8]),
        .R(SR));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0__0[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(SR));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(SR));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(SR));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(SR));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(SR));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(SR));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(SR));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(SR));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(SR));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(SR));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(SR));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(SR));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(SR));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(SR));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(SR));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(SR));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(SR));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(SR));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(SR));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(SR));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(SR));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(SR));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(SR));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(SR));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(SR));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(SR));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(SR));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(SR));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(SR));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(SR));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(SR));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(SR));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(SR));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(SR));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(SR));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(SR));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(SR));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(SR));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(SR));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(SR));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(SR));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(SR));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(SR));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(SR));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(SR));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(SR));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(SR));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(SR));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(SR));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(SR));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(SR));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(SR));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(SR));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(SR));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(SR));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(SR));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(SR));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(SR));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(SR));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(SR));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(SR));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(SR));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg5[0]),
        .R(SR));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(SR));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(SR));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(SR));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(SR));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(SR));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(SR));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(SR));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(SR));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(SR));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(SR));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg5[1]),
        .R(SR));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(SR));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(SR));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(SR));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(SR));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(SR));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(SR));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(SR));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(SR));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(SR));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(SR));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg5[2]),
        .R(SR));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(SR));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(SR));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg5[3]),
        .R(SR));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(SR));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(SR));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(SR));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(SR));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(SR));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg6[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[2]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg6[0]),
        .R(SR));
  FDRE \slv_reg6_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg6[10]),
        .R(SR));
  FDRE \slv_reg6_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg6[11]),
        .R(SR));
  FDRE \slv_reg6_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg6[12]),
        .R(SR));
  FDRE \slv_reg6_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg6[13]),
        .R(SR));
  FDRE \slv_reg6_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg6[14]),
        .R(SR));
  FDRE \slv_reg6_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg6[15]),
        .R(SR));
  FDRE \slv_reg6_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg6[16]),
        .R(SR));
  FDRE \slv_reg6_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg6[17]),
        .R(SR));
  FDRE \slv_reg6_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg6[18]),
        .R(SR));
  FDRE \slv_reg6_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg6[19]),
        .R(SR));
  FDRE \slv_reg6_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg6[1]),
        .R(SR));
  FDRE \slv_reg6_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg6[20]),
        .R(SR));
  FDRE \slv_reg6_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg6[21]),
        .R(SR));
  FDRE \slv_reg6_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg6[22]),
        .R(SR));
  FDRE \slv_reg6_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg6[23]),
        .R(SR));
  FDRE \slv_reg6_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg6[24]),
        .R(SR));
  FDRE \slv_reg6_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg6[25]),
        .R(SR));
  FDRE \slv_reg6_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg6[26]),
        .R(SR));
  FDRE \slv_reg6_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg6[27]),
        .R(SR));
  FDRE \slv_reg6_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg6[28]),
        .R(SR));
  FDRE \slv_reg6_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg6[29]),
        .R(SR));
  FDRE \slv_reg6_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg6[2]),
        .R(SR));
  FDRE \slv_reg6_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg6[30]),
        .R(SR));
  FDRE \slv_reg6_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg6[31]),
        .R(SR));
  FDRE \slv_reg6_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg6[3]),
        .R(SR));
  FDRE \slv_reg6_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg6[4]),
        .R(SR));
  FDRE \slv_reg6_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg6[5]),
        .R(SR));
  FDRE \slv_reg6_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg6[6]),
        .R(SR));
  FDRE \slv_reg6_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg6[7]),
        .R(SR));
  FDRE \slv_reg6_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg6[8]),
        .R(SR));
  FDRE \slv_reg6_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg6[9]),
        .R(SR));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(SR));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg7[10]),
        .R(SR));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg7[11]),
        .R(SR));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg7[12]),
        .R(SR));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg7[13]),
        .R(SR));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg7[14]),
        .R(SR));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg7[15]),
        .R(SR));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg7[16]),
        .R(SR));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(SR));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(SR));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(SR));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg7[1]),
        .R(SR));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(SR));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(SR));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(SR));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(SR));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(SR));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(SR));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(SR));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(SR));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(SR));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(SR));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg7[2]),
        .R(SR));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(SR));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(SR));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg7[3]),
        .R(SR));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg7[4]),
        .R(SR));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg7[5]),
        .R(SR));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg7[6]),
        .R(SR));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg7[7]),
        .R(SR));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg7[8]),
        .R(SR));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg7[9]),
        .R(SR));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(S_AXI_ARREADY),
        .O(slv_reg_rden__0));
  design_1_myip_4core_1_0_sample_for4 u_sample
       (.D(reg_data_out),
        .DST_ACK_1(DST_ACK_1),
        .DST_ACK_2(DST_ACK_2),
        .DST_ACK_3(DST_ACK_3),
        .DST_DATA_1(DST_DATA_1),
        .DST_DATA_2(DST_DATA_2),
        .DST_DATA_3(DST_DATA_3),
        .DST_FIN_1(DST_FIN_1),
        .DST_FIN_2(DST_FIN_2),
        .DST_FIN_3(DST_FIN_3),
        .DST_VALID_1(DST_VALID_1),
        .DST_VALID_2(DST_VALID_2),
        .DST_VALID_3(DST_VALID_3),
        .Q(Q),
        .SR(SR),
        .SRC_ACK_1(SRC_ACK_1),
        .SRC_ACK_2(SRC_ACK_2),
        .SRC_ACK_3(SRC_ACK_3),
        .SRC_DATA_1(SRC_DATA_1),
        .SRC_FIN_1(SRC_FIN_1),
        .SRC_FIN_1_reg_0(SRC_FIN_1_reg),
        .SRC_VALID_1(SRC_VALID_1),
        .SRC_VALID_1_reg_0(SRC_VALID_1_reg),
        .\axi_rdata_reg[0] (sel0),
        .\axi_rdata_reg[0]_0 (\axi_rdata[0]_i_3_n_0 ),
        .\axi_rdata_reg[10] (\axi_rdata[10]_i_3_n_0 ),
        .\axi_rdata_reg[11] (\axi_rdata[11]_i_3_n_0 ),
        .\axi_rdata_reg[12] (\axi_rdata[12]_i_3_n_0 ),
        .\axi_rdata_reg[13] (\axi_rdata[13]_i_3_n_0 ),
        .\axi_rdata_reg[14] (\axi_rdata[14]_i_3_n_0 ),
        .\axi_rdata_reg[15] (\axi_rdata[15]_i_3_n_0 ),
        .\axi_rdata_reg[16] (\axi_rdata[16]_i_3_n_0 ),
        .\axi_rdata_reg[17] (\axi_rdata[17]_i_3_n_0 ),
        .\axi_rdata_reg[18] (\axi_rdata[18]_i_3_n_0 ),
        .\axi_rdata_reg[19] (\axi_rdata[19]_i_3_n_0 ),
        .\axi_rdata_reg[1] (\axi_rdata[1]_i_3_n_0 ),
        .\axi_rdata_reg[20] (\axi_rdata[20]_i_3_n_0 ),
        .\axi_rdata_reg[21] (\axi_rdata[21]_i_3_n_0 ),
        .\axi_rdata_reg[22] (\axi_rdata[22]_i_3_n_0 ),
        .\axi_rdata_reg[23] (\axi_rdata[23]_i_3_n_0 ),
        .\axi_rdata_reg[24] (\axi_rdata[24]_i_3_n_0 ),
        .\axi_rdata_reg[25] (\axi_rdata[25]_i_3_n_0 ),
        .\axi_rdata_reg[26] (\axi_rdata[26]_i_3_n_0 ),
        .\axi_rdata_reg[27] (\axi_rdata[27]_i_3_n_0 ),
        .\axi_rdata_reg[28] (\axi_rdata[28]_i_3_n_0 ),
        .\axi_rdata_reg[29] (\axi_rdata[29]_i_3_n_0 ),
        .\axi_rdata_reg[2] (\axi_rdata[2]_i_3_n_0 ),
        .\axi_rdata_reg[30] (\axi_rdata[30]_i_3_n_0 ),
        .\axi_rdata_reg[31] ({slv_reg0__0,slv_reg0}),
        .\axi_rdata_reg[31]_0 (\axi_rdata[31]_i_3_n_0 ),
        .\axi_rdata_reg[3] (\axi_rdata[3]_i_3_n_0 ),
        .\axi_rdata_reg[4] (\axi_rdata[4]_i_3_n_0 ),
        .\axi_rdata_reg[5] (\axi_rdata[5]_i_3_n_0 ),
        .\axi_rdata_reg[6] (\axi_rdata[6]_i_3_n_0 ),
        .\axi_rdata_reg[7] (\axi_rdata[7]_i_3_n_0 ),
        .\axi_rdata_reg[8] (\axi_rdata[8]_i_3_n_0 ),
        .\axi_rdata_reg[9] (\axi_rdata[9]_i_3_n_0 ),
        .done(done),
        .done_reg_0(done_reg),
        .\my_value_reg[31]_0 (slv_reg1),
        .s00_axi_aclk(s00_axi_aclk));
endmodule

module design_1_myip_4core_1_0_sample_for4
   (DST_ACK_1,
    DST_ACK_2,
    DST_ACK_3,
    SRC_VALID_1,
    SRC_FIN_1,
    done,
    Q,
    D,
    SRC_DATA_1,
    DST_VALID_1,
    s00_axi_aclk,
    SR,
    DST_VALID_2,
    DST_VALID_3,
    SRC_VALID_1_reg_0,
    SRC_FIN_1_reg_0,
    done_reg_0,
    \my_value_reg[31]_0 ,
    \axi_rdata_reg[31] ,
    DST_FIN_1,
    DST_FIN_2,
    DST_FIN_3,
    DST_DATA_1,
    DST_DATA_2,
    DST_DATA_3,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    \axi_rdata_reg[1] ,
    \axi_rdata_reg[2] ,
    \axi_rdata_reg[3] ,
    \axi_rdata_reg[4] ,
    \axi_rdata_reg[5] ,
    \axi_rdata_reg[6] ,
    \axi_rdata_reg[7] ,
    \axi_rdata_reg[8] ,
    \axi_rdata_reg[9] ,
    \axi_rdata_reg[10] ,
    \axi_rdata_reg[11] ,
    \axi_rdata_reg[12] ,
    \axi_rdata_reg[13] ,
    \axi_rdata_reg[14] ,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[16] ,
    \axi_rdata_reg[17] ,
    \axi_rdata_reg[18] ,
    \axi_rdata_reg[19] ,
    \axi_rdata_reg[20] ,
    \axi_rdata_reg[21] ,
    \axi_rdata_reg[22] ,
    \axi_rdata_reg[23] ,
    \axi_rdata_reg[24] ,
    \axi_rdata_reg[25] ,
    \axi_rdata_reg[26] ,
    \axi_rdata_reg[27] ,
    \axi_rdata_reg[28] ,
    \axi_rdata_reg[29] ,
    \axi_rdata_reg[30] ,
    \axi_rdata_reg[31]_0 ,
    SRC_ACK_3,
    SRC_ACK_2,
    SRC_ACK_1);
  output DST_ACK_1;
  output DST_ACK_2;
  output DST_ACK_3;
  output SRC_VALID_1;
  output SRC_FIN_1;
  output done;
  output [4:0]Q;
  output [31:0]D;
  output [31:0]SRC_DATA_1;
  input DST_VALID_1;
  input s00_axi_aclk;
  input [0:0]SR;
  input DST_VALID_2;
  input DST_VALID_3;
  input SRC_VALID_1_reg_0;
  input SRC_FIN_1_reg_0;
  input done_reg_0;
  input [31:0]\my_value_reg[31]_0 ;
  input [31:0]\axi_rdata_reg[31] ;
  input DST_FIN_1;
  input DST_FIN_2;
  input DST_FIN_3;
  input [31:0]DST_DATA_1;
  input [31:0]DST_DATA_2;
  input [31:0]DST_DATA_3;
  input [2:0]\axi_rdata_reg[0] ;
  input \axi_rdata_reg[0]_0 ;
  input \axi_rdata_reg[1] ;
  input \axi_rdata_reg[2] ;
  input \axi_rdata_reg[3] ;
  input \axi_rdata_reg[4] ;
  input \axi_rdata_reg[5] ;
  input \axi_rdata_reg[6] ;
  input \axi_rdata_reg[7] ;
  input \axi_rdata_reg[8] ;
  input \axi_rdata_reg[9] ;
  input \axi_rdata_reg[10] ;
  input \axi_rdata_reg[11] ;
  input \axi_rdata_reg[12] ;
  input \axi_rdata_reg[13] ;
  input \axi_rdata_reg[14] ;
  input \axi_rdata_reg[15] ;
  input \axi_rdata_reg[16] ;
  input \axi_rdata_reg[17] ;
  input \axi_rdata_reg[18] ;
  input \axi_rdata_reg[19] ;
  input \axi_rdata_reg[20] ;
  input \axi_rdata_reg[21] ;
  input \axi_rdata_reg[22] ;
  input \axi_rdata_reg[23] ;
  input \axi_rdata_reg[24] ;
  input \axi_rdata_reg[25] ;
  input \axi_rdata_reg[26] ;
  input \axi_rdata_reg[27] ;
  input \axi_rdata_reg[28] ;
  input \axi_rdata_reg[29] ;
  input \axi_rdata_reg[30] ;
  input \axi_rdata_reg[31]_0 ;
  input SRC_ACK_3;
  input SRC_ACK_2;
  input SRC_ACK_1;

  wire [31:0]D;
  wire DST_ACK_1;
  wire DST_ACK_2;
  wire DST_ACK_3;
  wire [31:0]DST_DATA_1;
  wire [31:0]DST_DATA_2;
  wire [31:0]DST_DATA_3;
  wire DST_FIN_1;
  wire DST_FIN_2;
  wire DST_FIN_3;
  wire DST_VALID_1;
  wire DST_VALID_2;
  wire DST_VALID_3;
  wire \FSM_onehot_state[3]_i_1_n_0 ;
  wire \FSM_onehot_state[5]_i_1_n_0 ;
  wire \FSM_onehot_state[5]_i_2_n_0 ;
  wire \FSM_onehot_state[5]_i_3_n_0 ;
  wire \FSM_onehot_state[5]_i_4_n_0 ;
  wire \FSM_onehot_state[5]_i_5_n_0 ;
  wire \FSM_onehot_state[5]_i_6_n_0 ;
  wire \FSM_onehot_state[5]_i_7_n_0 ;
  wire \FSM_onehot_state[5]_i_8_n_0 ;
  wire \FSM_onehot_state[5]_i_9_n_0 ;
  wire \FSM_onehot_state[7]_i_1_n_0 ;
  wire \FSM_onehot_state[7]_i_2_n_0 ;
  wire \FSM_onehot_state[7]_i_3_n_0 ;
  wire \FSM_onehot_state_reg_n_0_[1] ;
  wire \FSM_onehot_state_reg_n_0_[2] ;
  wire \FSM_onehot_state_reg_n_0_[6] ;
  wire [4:0]Q;
  wire [0:0]SR;
  wire SRC_ACK_1;
  wire SRC_ACK_2;
  wire SRC_ACK_3;
  wire [31:0]SRC_DATA_1;
  wire \SRC_DATA_1[0]_i_1_n_0 ;
  wire \SRC_DATA_1[10]_i_1_n_0 ;
  wire \SRC_DATA_1[11]_i_1_n_0 ;
  wire \SRC_DATA_1[12]_i_1_n_0 ;
  wire \SRC_DATA_1[13]_i_1_n_0 ;
  wire \SRC_DATA_1[14]_i_1_n_0 ;
  wire \SRC_DATA_1[15]_i_1_n_0 ;
  wire \SRC_DATA_1[16]_i_1_n_0 ;
  wire \SRC_DATA_1[17]_i_1_n_0 ;
  wire \SRC_DATA_1[18]_i_1_n_0 ;
  wire \SRC_DATA_1[19]_i_1_n_0 ;
  wire \SRC_DATA_1[1]_i_1_n_0 ;
  wire \SRC_DATA_1[20]_i_1_n_0 ;
  wire \SRC_DATA_1[21]_i_1_n_0 ;
  wire \SRC_DATA_1[22]_i_1_n_0 ;
  wire \SRC_DATA_1[23]_i_1_n_0 ;
  wire \SRC_DATA_1[24]_i_1_n_0 ;
  wire \SRC_DATA_1[25]_i_1_n_0 ;
  wire \SRC_DATA_1[26]_i_1_n_0 ;
  wire \SRC_DATA_1[27]_i_1_n_0 ;
  wire \SRC_DATA_1[28]_i_1_n_0 ;
  wire \SRC_DATA_1[29]_i_1_n_0 ;
  wire \SRC_DATA_1[2]_i_1_n_0 ;
  wire \SRC_DATA_1[30]_i_1_n_0 ;
  wire \SRC_DATA_1[31]_i_1_n_0 ;
  wire \SRC_DATA_1[31]_i_2_n_0 ;
  wire \SRC_DATA_1[3]_i_1_n_0 ;
  wire \SRC_DATA_1[4]_i_1_n_0 ;
  wire \SRC_DATA_1[5]_i_1_n_0 ;
  wire \SRC_DATA_1[6]_i_1_n_0 ;
  wire \SRC_DATA_1[7]_i_1_n_0 ;
  wire \SRC_DATA_1[8]_i_1_n_0 ;
  wire \SRC_DATA_1[9]_i_1_n_0 ;
  wire SRC_FIN_1;
  wire SRC_FIN_1_reg_0;
  wire SRC_VALID_1;
  wire SRC_VALID_1_reg_0;
  wire \ack[0]_i_1_n_0 ;
  wire \ack[1]_i_1_n_0 ;
  wire \ack[2]_i_1_n_0 ;
  wire \ack_reg_n_0_[0] ;
  wire \ack_reg_n_0_[1] ;
  wire \ack_reg_n_0_[2] ;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire [2:0]\axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[10] ;
  wire \axi_rdata_reg[11] ;
  wire \axi_rdata_reg[12] ;
  wire \axi_rdata_reg[13] ;
  wire \axi_rdata_reg[14] ;
  wire \axi_rdata_reg[15] ;
  wire \axi_rdata_reg[16] ;
  wire \axi_rdata_reg[17] ;
  wire \axi_rdata_reg[18] ;
  wire \axi_rdata_reg[19] ;
  wire \axi_rdata_reg[1] ;
  wire \axi_rdata_reg[20] ;
  wire \axi_rdata_reg[21] ;
  wire \axi_rdata_reg[22] ;
  wire \axi_rdata_reg[23] ;
  wire \axi_rdata_reg[24] ;
  wire \axi_rdata_reg[25] ;
  wire \axi_rdata_reg[26] ;
  wire \axi_rdata_reg[27] ;
  wire \axi_rdata_reg[28] ;
  wire \axi_rdata_reg[29] ;
  wire \axi_rdata_reg[2] ;
  wire \axi_rdata_reg[30] ;
  wire [31:0]\axi_rdata_reg[31] ;
  wire \axi_rdata_reg[31]_0 ;
  wire \axi_rdata_reg[3] ;
  wire \axi_rdata_reg[4] ;
  wire \axi_rdata_reg[5] ;
  wire \axi_rdata_reg[6] ;
  wire \axi_rdata_reg[7] ;
  wire \axi_rdata_reg[8] ;
  wire \axi_rdata_reg[9] ;
  wire done;
  wire done_reg_0;
  wire [31:0]dout;
  wire dout0__2_carry__0_i_10_n_0;
  wire dout0__2_carry__0_i_11_n_0;
  wire dout0__2_carry__0_i_12_n_0;
  wire dout0__2_carry__0_i_13_n_0;
  wire dout0__2_carry__0_i_14_n_0;
  wire dout0__2_carry__0_i_15_n_0;
  wire dout0__2_carry__0_i_16_n_0;
  wire dout0__2_carry__0_i_17_n_0;
  wire dout0__2_carry__0_i_18_n_0;
  wire dout0__2_carry__0_i_19_n_0;
  wire dout0__2_carry__0_i_1_n_0;
  wire dout0__2_carry__0_i_20_n_0;
  wire dout0__2_carry__0_i_21_n_0;
  wire dout0__2_carry__0_i_22_n_0;
  wire dout0__2_carry__0_i_23_n_0;
  wire dout0__2_carry__0_i_24_n_0;
  wire dout0__2_carry__0_i_2_n_0;
  wire dout0__2_carry__0_i_3_n_0;
  wire dout0__2_carry__0_i_4_n_0;
  wire dout0__2_carry__0_i_5_n_0;
  wire dout0__2_carry__0_i_6_n_0;
  wire dout0__2_carry__0_i_7_n_0;
  wire dout0__2_carry__0_i_8_n_0;
  wire dout0__2_carry__0_i_9_n_0;
  wire dout0__2_carry__0_n_0;
  wire dout0__2_carry__0_n_1;
  wire dout0__2_carry__0_n_2;
  wire dout0__2_carry__0_n_3;
  wire dout0__2_carry__0_n_4;
  wire dout0__2_carry__0_n_5;
  wire dout0__2_carry__0_n_6;
  wire dout0__2_carry__0_n_7;
  wire dout0__2_carry__1_i_10_n_0;
  wire dout0__2_carry__1_i_11_n_0;
  wire dout0__2_carry__1_i_12_n_0;
  wire dout0__2_carry__1_i_13_n_0;
  wire dout0__2_carry__1_i_14_n_0;
  wire dout0__2_carry__1_i_15_n_0;
  wire dout0__2_carry__1_i_16_n_0;
  wire dout0__2_carry__1_i_17_n_0;
  wire dout0__2_carry__1_i_18_n_0;
  wire dout0__2_carry__1_i_19_n_0;
  wire dout0__2_carry__1_i_1_n_0;
  wire dout0__2_carry__1_i_20_n_0;
  wire dout0__2_carry__1_i_21_n_0;
  wire dout0__2_carry__1_i_22_n_0;
  wire dout0__2_carry__1_i_23_n_0;
  wire dout0__2_carry__1_i_24_n_0;
  wire dout0__2_carry__1_i_2_n_0;
  wire dout0__2_carry__1_i_3_n_0;
  wire dout0__2_carry__1_i_4_n_0;
  wire dout0__2_carry__1_i_5_n_0;
  wire dout0__2_carry__1_i_6_n_0;
  wire dout0__2_carry__1_i_7_n_0;
  wire dout0__2_carry__1_i_8_n_0;
  wire dout0__2_carry__1_i_9_n_0;
  wire dout0__2_carry__1_n_0;
  wire dout0__2_carry__1_n_1;
  wire dout0__2_carry__1_n_2;
  wire dout0__2_carry__1_n_3;
  wire dout0__2_carry__1_n_4;
  wire dout0__2_carry__1_n_5;
  wire dout0__2_carry__1_n_6;
  wire dout0__2_carry__1_n_7;
  wire dout0__2_carry__2_i_10_n_0;
  wire dout0__2_carry__2_i_11_n_0;
  wire dout0__2_carry__2_i_12_n_0;
  wire dout0__2_carry__2_i_13_n_0;
  wire dout0__2_carry__2_i_14_n_0;
  wire dout0__2_carry__2_i_15_n_0;
  wire dout0__2_carry__2_i_16_n_0;
  wire dout0__2_carry__2_i_17_n_0;
  wire dout0__2_carry__2_i_18_n_0;
  wire dout0__2_carry__2_i_19_n_0;
  wire dout0__2_carry__2_i_1_n_0;
  wire dout0__2_carry__2_i_20_n_0;
  wire dout0__2_carry__2_i_21_n_0;
  wire dout0__2_carry__2_i_22_n_0;
  wire dout0__2_carry__2_i_23_n_0;
  wire dout0__2_carry__2_i_24_n_0;
  wire dout0__2_carry__2_i_2_n_0;
  wire dout0__2_carry__2_i_3_n_0;
  wire dout0__2_carry__2_i_4_n_0;
  wire dout0__2_carry__2_i_5_n_0;
  wire dout0__2_carry__2_i_6_n_0;
  wire dout0__2_carry__2_i_7_n_0;
  wire dout0__2_carry__2_i_8_n_0;
  wire dout0__2_carry__2_i_9_n_0;
  wire dout0__2_carry__2_n_1;
  wire dout0__2_carry__2_n_2;
  wire dout0__2_carry__2_n_3;
  wire dout0__2_carry__2_n_4;
  wire dout0__2_carry__2_n_5;
  wire dout0__2_carry__2_n_6;
  wire dout0__2_carry__2_n_7;
  wire dout0__2_carry_i_10_n_0;
  wire dout0__2_carry_i_11_n_0;
  wire dout0__2_carry_i_12_n_0;
  wire dout0__2_carry_i_13_n_0;
  wire dout0__2_carry_i_14_n_0;
  wire dout0__2_carry_i_15_n_0;
  wire dout0__2_carry_i_16_n_0;
  wire dout0__2_carry_i_17_n_0;
  wire dout0__2_carry_i_18_n_0;
  wire dout0__2_carry_i_19_n_0;
  wire dout0__2_carry_i_1_n_0;
  wire dout0__2_carry_i_20_n_0;
  wire dout0__2_carry_i_21_n_0;
  wire dout0__2_carry_i_2_n_0;
  wire dout0__2_carry_i_3_n_0;
  wire dout0__2_carry_i_4_n_0;
  wire dout0__2_carry_i_5_n_0;
  wire dout0__2_carry_i_6_n_0;
  wire dout0__2_carry_i_7_n_0;
  wire dout0__2_carry_i_8_n_0;
  wire dout0__2_carry_i_9_n_0;
  wire dout0__2_carry_n_0;
  wire dout0__2_carry_n_1;
  wire dout0__2_carry_n_2;
  wire dout0__2_carry_n_3;
  wire dout0__2_carry_n_4;
  wire dout0__2_carry_n_5;
  wire dout0__2_carry_n_6;
  wire dout0__2_carry_n_7;
  wire \dout[0]_i_1_n_0 ;
  wire \dout[10]_i_1_n_0 ;
  wire \dout[11]_i_1_n_0 ;
  wire \dout[12]_i_1_n_0 ;
  wire \dout[13]_i_1_n_0 ;
  wire \dout[14]_i_1_n_0 ;
  wire \dout[15]_i_1_n_0 ;
  wire \dout[16]_i_1_n_0 ;
  wire \dout[17]_i_1_n_0 ;
  wire \dout[18]_i_1_n_0 ;
  wire \dout[19]_i_1_n_0 ;
  wire \dout[1]_i_1_n_0 ;
  wire \dout[20]_i_1_n_0 ;
  wire \dout[21]_i_1_n_0 ;
  wire \dout[22]_i_1_n_0 ;
  wire \dout[23]_i_1_n_0 ;
  wire \dout[24]_i_1_n_0 ;
  wire \dout[25]_i_1_n_0 ;
  wire \dout[26]_i_1_n_0 ;
  wire \dout[27]_i_1_n_0 ;
  wire \dout[28]_i_1_n_0 ;
  wire \dout[29]_i_1_n_0 ;
  wire \dout[2]_i_1_n_0 ;
  wire \dout[30]_i_1_n_0 ;
  wire \dout[31]_i_1_n_0 ;
  wire \dout[31]_i_2_n_0 ;
  wire \dout[3]_i_1_n_0 ;
  wire \dout[4]_i_1_n_0 ;
  wire \dout[5]_i_1_n_0 ;
  wire \dout[6]_i_1_n_0 ;
  wire \dout[7]_i_1_n_0 ;
  wire \dout[8]_i_1_n_0 ;
  wire \dout[9]_i_1_n_0 ;
  wire [2:0]fin;
  wire \fin[0]_i_1_n_0 ;
  wire \fin[1]_i_1_n_0 ;
  wire \fin[2]_i_1_n_0 ;
  wire [31:0]in7;
  wire \my_value[0]_i_1_n_0 ;
  wire \my_value[0]_i_2_n_0 ;
  wire \my_value[0]_i_3_n_0 ;
  wire \my_value[0]_i_4_n_0 ;
  wire \my_value[0]_i_5_n_0 ;
  wire \my_value[0]_i_6_n_0 ;
  wire \my_value[0]_i_7_n_0 ;
  wire \my_value[0]_i_8_n_0 ;
  wire \my_value[0]_i_9_n_0 ;
  wire \my_value[10]_i_1_n_0 ;
  wire \my_value[11]_i_1_n_0 ;
  wire \my_value[12]_i_1_n_0 ;
  wire \my_value[13]_i_1_n_0 ;
  wire \my_value[14]_i_1_n_0 ;
  wire \my_value[15]_i_1_n_0 ;
  wire \my_value[16]_i_1_n_0 ;
  wire \my_value[17]_i_1_n_0 ;
  wire \my_value[18]_i_1_n_0 ;
  wire \my_value[19]_i_1_n_0 ;
  wire \my_value[1]_i_1_n_0 ;
  wire \my_value[20]_i_1_n_0 ;
  wire \my_value[21]_i_1_n_0 ;
  wire \my_value[22]_i_1_n_0 ;
  wire \my_value[23]_i_1_n_0 ;
  wire \my_value[24]_i_1_n_0 ;
  wire \my_value[25]_i_1_n_0 ;
  wire \my_value[26]_i_1_n_0 ;
  wire \my_value[27]_i_1_n_0 ;
  wire \my_value[28]_i_1_n_0 ;
  wire \my_value[29]_i_1_n_0 ;
  wire \my_value[2]_i_1_n_0 ;
  wire \my_value[30]_i_1_n_0 ;
  wire \my_value[31]_i_1_n_0 ;
  wire \my_value[31]_i_2_n_0 ;
  wire \my_value[3]_i_1_n_0 ;
  wire \my_value[4]_i_1_n_0 ;
  wire \my_value[5]_i_1_n_0 ;
  wire \my_value[6]_i_1_n_0 ;
  wire \my_value[7]_i_1_n_0 ;
  wire \my_value[8]_i_1_n_0 ;
  wire \my_value[9]_i_1_n_0 ;
  wire [31:0]\my_value_reg[31]_0 ;
  wire \my_value_reg_n_0_[0] ;
  wire \my_value_reg_n_0_[10] ;
  wire \my_value_reg_n_0_[11] ;
  wire \my_value_reg_n_0_[12] ;
  wire \my_value_reg_n_0_[13] ;
  wire \my_value_reg_n_0_[14] ;
  wire \my_value_reg_n_0_[15] ;
  wire \my_value_reg_n_0_[16] ;
  wire \my_value_reg_n_0_[17] ;
  wire \my_value_reg_n_0_[18] ;
  wire \my_value_reg_n_0_[19] ;
  wire \my_value_reg_n_0_[1] ;
  wire \my_value_reg_n_0_[20] ;
  wire \my_value_reg_n_0_[21] ;
  wire \my_value_reg_n_0_[22] ;
  wire \my_value_reg_n_0_[23] ;
  wire \my_value_reg_n_0_[24] ;
  wire \my_value_reg_n_0_[25] ;
  wire \my_value_reg_n_0_[26] ;
  wire \my_value_reg_n_0_[27] ;
  wire \my_value_reg_n_0_[28] ;
  wire \my_value_reg_n_0_[29] ;
  wire \my_value_reg_n_0_[2] ;
  wire \my_value_reg_n_0_[30] ;
  wire \my_value_reg_n_0_[31] ;
  wire \my_value_reg_n_0_[3] ;
  wire \my_value_reg_n_0_[4] ;
  wire \my_value_reg_n_0_[5] ;
  wire \my_value_reg_n_0_[6] ;
  wire \my_value_reg_n_0_[7] ;
  wire \my_value_reg_n_0_[8] ;
  wire \my_value_reg_n_0_[9] ;
  wire \others_value_reg_n_0_[0] ;
  wire \others_value_reg_n_0_[10] ;
  wire \others_value_reg_n_0_[11] ;
  wire \others_value_reg_n_0_[12] ;
  wire \others_value_reg_n_0_[13] ;
  wire \others_value_reg_n_0_[14] ;
  wire \others_value_reg_n_0_[15] ;
  wire \others_value_reg_n_0_[16] ;
  wire \others_value_reg_n_0_[17] ;
  wire \others_value_reg_n_0_[18] ;
  wire \others_value_reg_n_0_[19] ;
  wire \others_value_reg_n_0_[1] ;
  wire \others_value_reg_n_0_[20] ;
  wire \others_value_reg_n_0_[21] ;
  wire \others_value_reg_n_0_[22] ;
  wire \others_value_reg_n_0_[23] ;
  wire \others_value_reg_n_0_[24] ;
  wire \others_value_reg_n_0_[25] ;
  wire \others_value_reg_n_0_[26] ;
  wire \others_value_reg_n_0_[27] ;
  wire \others_value_reg_n_0_[28] ;
  wire \others_value_reg_n_0_[29] ;
  wire \others_value_reg_n_0_[2] ;
  wire \others_value_reg_n_0_[30] ;
  wire \others_value_reg_n_0_[31] ;
  wire \others_value_reg_n_0_[3] ;
  wire \others_value_reg_n_0_[4] ;
  wire \others_value_reg_n_0_[5] ;
  wire \others_value_reg_n_0_[6] ;
  wire \others_value_reg_n_0_[7] ;
  wire \others_value_reg_n_0_[8] ;
  wire \others_value_reg_n_0_[9] ;
  wire [31:0]p_0_in;
  wire [31:0]p_0_in0_in;
  wire [95:31]p_6_out;
  wire [95:0]p_6_out0_in;
  wire s00_axi_aclk;
  wire \valid[0]_i_1_n_0 ;
  wire \valid[1]_i_1_n_0 ;
  wire \valid[2]_i_1_n_0 ;
  wire \valid_reg_n_0_[0] ;
  wire \valid_reg_n_0_[1] ;
  wire \valid_reg_n_0_[2] ;
  wire [7:7]NLW_dout0__2_carry__2_CO_UNCONNECTED;

  FDCE DST_ACK_1_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(DST_VALID_1),
        .Q(DST_ACK_1));
  FDCE DST_ACK_2_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(DST_VALID_2),
        .Q(DST_ACK_2));
  FDCE DST_ACK_3_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(DST_VALID_3),
        .Q(DST_ACK_3));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    \FSM_onehot_state[3]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_5_n_0 ),
        .I2(\FSM_onehot_state[5]_i_4_n_0 ),
        .I3(\FSM_onehot_state[5]_i_3_n_0 ),
        .I4(\FSM_onehot_state[5]_i_2_n_0 ),
        .O(\FSM_onehot_state[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAABAAAAAAAA)) 
    \FSM_onehot_state[5]_i_1 
       (.I0(Q[2]),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\FSM_onehot_state_reg_n_0_[2] ),
        .O(\FSM_onehot_state[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \FSM_onehot_state[5]_i_2 
       (.I0(\my_value_reg[31]_0 [21]),
        .I1(\my_value_reg[31]_0 [20]),
        .I2(\my_value_reg[31]_0 [23]),
        .I3(\my_value_reg[31]_0 [22]),
        .I4(\FSM_onehot_state[5]_i_6_n_0 ),
        .O(\FSM_onehot_state[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \FSM_onehot_state[5]_i_3 
       (.I0(\my_value_reg[31]_0 [29]),
        .I1(\my_value_reg[31]_0 [28]),
        .I2(\my_value_reg[31]_0 [30]),
        .I3(\my_value_reg[31]_0 [31]),
        .I4(\FSM_onehot_state[5]_i_7_n_0 ),
        .O(\FSM_onehot_state[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \FSM_onehot_state[5]_i_4 
       (.I0(\my_value_reg[31]_0 [5]),
        .I1(\my_value_reg[31]_0 [4]),
        .I2(\my_value_reg[31]_0 [7]),
        .I3(\my_value_reg[31]_0 [6]),
        .I4(\FSM_onehot_state[5]_i_8_n_0 ),
        .O(\FSM_onehot_state[5]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \FSM_onehot_state[5]_i_5 
       (.I0(\my_value_reg[31]_0 [13]),
        .I1(\my_value_reg[31]_0 [12]),
        .I2(\my_value_reg[31]_0 [15]),
        .I3(\my_value_reg[31]_0 [14]),
        .I4(\FSM_onehot_state[5]_i_9_n_0 ),
        .O(\FSM_onehot_state[5]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \FSM_onehot_state[5]_i_6 
       (.I0(\my_value_reg[31]_0 [18]),
        .I1(\my_value_reg[31]_0 [19]),
        .I2(\my_value_reg[31]_0 [16]),
        .I3(\my_value_reg[31]_0 [17]),
        .O(\FSM_onehot_state[5]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \FSM_onehot_state[5]_i_7 
       (.I0(\my_value_reg[31]_0 [26]),
        .I1(\my_value_reg[31]_0 [27]),
        .I2(\my_value_reg[31]_0 [24]),
        .I3(\my_value_reg[31]_0 [25]),
        .O(\FSM_onehot_state[5]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \FSM_onehot_state[5]_i_8 
       (.I0(\my_value_reg[31]_0 [2]),
        .I1(\my_value_reg[31]_0 [3]),
        .I2(\my_value_reg[31]_0 [0]),
        .I3(\my_value_reg[31]_0 [1]),
        .O(\FSM_onehot_state[5]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \FSM_onehot_state[5]_i_9 
       (.I0(\my_value_reg[31]_0 [10]),
        .I1(\my_value_reg[31]_0 [11]),
        .I2(\my_value_reg[31]_0 [8]),
        .I3(\my_value_reg[31]_0 [9]),
        .O(\FSM_onehot_state[5]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state[7]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\FSM_onehot_state_reg_n_0_[2] ),
        .I3(\FSM_onehot_state[7]_i_2_n_0 ),
        .I4(\FSM_onehot_state[7]_i_3_n_0 ),
        .O(\FSM_onehot_state[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF800080008000)) 
    \FSM_onehot_state[7]_i_2 
       (.I0(\ack_reg_n_0_[0] ),
        .I1(\ack_reg_n_0_[2] ),
        .I2(\ack_reg_n_0_[1] ),
        .I3(Q[2]),
        .I4(\axi_rdata_reg[31] [0]),
        .I5(\FSM_onehot_state_reg_n_0_[1] ),
        .O(\FSM_onehot_state[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEAAAAAAA)) 
    \FSM_onehot_state[7]_i_3 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(fin[0]),
        .I2(fin[2]),
        .I3(fin[1]),
        .I4(Q[3]),
        .O(\FSM_onehot_state[7]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .D(1'b0),
        .PRE(SR),
        .Q(Q[0]));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(Q[0]),
        .Q(\FSM_onehot_state_reg_n_0_[1] ));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_onehot_state_reg_n_0_[1] ),
        .Q(\FSM_onehot_state_reg_n_0_[2] ));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_onehot_state[3]_i_1_n_0 ),
        .Q(Q[1]));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(Q[1]),
        .Q(Q[2]));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_onehot_state[5]_i_1_n_0 ),
        .Q(Q[3]));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(Q[3]),
        .Q(\FSM_onehot_state_reg_n_0_[6] ));
  (* FSM_ENCODED_STATES = "STATE_IDLE:00000001,STATE_WAIT:00000010,STATE_STORE:00000100,STATE_ACK_WAIT:00010000,STATE_CAL:01000000,STATE_DONE:10000000,STATE_SEND:00001000,STATE_FIN:00100000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\FSM_onehot_state[7]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_onehot_state_reg_n_0_[6] ),
        .Q(Q[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[0]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[0] ),
        .O(\SRC_DATA_1[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[10]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[10] ),
        .O(\SRC_DATA_1[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[11]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[11] ),
        .O(\SRC_DATA_1[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[12]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[12] ),
        .O(\SRC_DATA_1[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[13]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[13] ),
        .O(\SRC_DATA_1[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[14]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[14] ),
        .O(\SRC_DATA_1[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[15]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[15] ),
        .O(\SRC_DATA_1[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[16]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[16] ),
        .O(\SRC_DATA_1[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[17]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[17] ),
        .O(\SRC_DATA_1[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[18]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[18] ),
        .O(\SRC_DATA_1[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[19]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[19] ),
        .O(\SRC_DATA_1[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[1]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[1] ),
        .O(\SRC_DATA_1[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[20]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[20] ),
        .O(\SRC_DATA_1[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[21]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[21] ),
        .O(\SRC_DATA_1[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[22]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[22] ),
        .O(\SRC_DATA_1[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[23]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[23] ),
        .O(\SRC_DATA_1[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[24]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[24] ),
        .O(\SRC_DATA_1[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[25]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[25] ),
        .O(\SRC_DATA_1[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[26]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[26] ),
        .O(\SRC_DATA_1[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[27]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[27] ),
        .O(\SRC_DATA_1[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[28]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[28] ),
        .O(\SRC_DATA_1[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[29]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[29] ),
        .O(\SRC_DATA_1[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[2]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[2] ),
        .O(\SRC_DATA_1[2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[30]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[30] ),
        .O(\SRC_DATA_1[30]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \SRC_DATA_1[31]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\SRC_DATA_1[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[31]_i_2 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[31] ),
        .O(\SRC_DATA_1[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[3]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[3] ),
        .O(\SRC_DATA_1[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[4]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[4] ),
        .O(\SRC_DATA_1[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[5]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[5] ),
        .O(\SRC_DATA_1[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[6]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[6] ),
        .O(\SRC_DATA_1[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[7]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[7] ),
        .O(\SRC_DATA_1[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[8]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[8] ),
        .O(\SRC_DATA_1[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA_1[9]_i_1 
       (.I0(Q[1]),
        .I1(\my_value_reg_n_0_[9] ),
        .O(\SRC_DATA_1[9]_i_1_n_0 ));
  FDCE \SRC_DATA_1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[0]_i_1_n_0 ),
        .Q(SRC_DATA_1[0]));
  FDCE \SRC_DATA_1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[10]_i_1_n_0 ),
        .Q(SRC_DATA_1[10]));
  FDCE \SRC_DATA_1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[11]_i_1_n_0 ),
        .Q(SRC_DATA_1[11]));
  FDCE \SRC_DATA_1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[12]_i_1_n_0 ),
        .Q(SRC_DATA_1[12]));
  FDCE \SRC_DATA_1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[13]_i_1_n_0 ),
        .Q(SRC_DATA_1[13]));
  FDCE \SRC_DATA_1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[14]_i_1_n_0 ),
        .Q(SRC_DATA_1[14]));
  FDCE \SRC_DATA_1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[15]_i_1_n_0 ),
        .Q(SRC_DATA_1[15]));
  FDCE \SRC_DATA_1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[16]_i_1_n_0 ),
        .Q(SRC_DATA_1[16]));
  FDCE \SRC_DATA_1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[17]_i_1_n_0 ),
        .Q(SRC_DATA_1[17]));
  FDCE \SRC_DATA_1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[18]_i_1_n_0 ),
        .Q(SRC_DATA_1[18]));
  FDCE \SRC_DATA_1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[19]_i_1_n_0 ),
        .Q(SRC_DATA_1[19]));
  FDCE \SRC_DATA_1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[1]_i_1_n_0 ),
        .Q(SRC_DATA_1[1]));
  FDCE \SRC_DATA_1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[20]_i_1_n_0 ),
        .Q(SRC_DATA_1[20]));
  FDCE \SRC_DATA_1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[21]_i_1_n_0 ),
        .Q(SRC_DATA_1[21]));
  FDCE \SRC_DATA_1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[22]_i_1_n_0 ),
        .Q(SRC_DATA_1[22]));
  FDCE \SRC_DATA_1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[23]_i_1_n_0 ),
        .Q(SRC_DATA_1[23]));
  FDCE \SRC_DATA_1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[24]_i_1_n_0 ),
        .Q(SRC_DATA_1[24]));
  FDCE \SRC_DATA_1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[25]_i_1_n_0 ),
        .Q(SRC_DATA_1[25]));
  FDCE \SRC_DATA_1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[26]_i_1_n_0 ),
        .Q(SRC_DATA_1[26]));
  FDCE \SRC_DATA_1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[27]_i_1_n_0 ),
        .Q(SRC_DATA_1[27]));
  FDCE \SRC_DATA_1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[28]_i_1_n_0 ),
        .Q(SRC_DATA_1[28]));
  FDCE \SRC_DATA_1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[29]_i_1_n_0 ),
        .Q(SRC_DATA_1[29]));
  FDCE \SRC_DATA_1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[2]_i_1_n_0 ),
        .Q(SRC_DATA_1[2]));
  FDCE \SRC_DATA_1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[30]_i_1_n_0 ),
        .Q(SRC_DATA_1[30]));
  FDCE \SRC_DATA_1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[31]_i_2_n_0 ),
        .Q(SRC_DATA_1[31]));
  FDCE \SRC_DATA_1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[3]_i_1_n_0 ),
        .Q(SRC_DATA_1[3]));
  FDCE \SRC_DATA_1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[4]_i_1_n_0 ),
        .Q(SRC_DATA_1[4]));
  FDCE \SRC_DATA_1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[5]_i_1_n_0 ),
        .Q(SRC_DATA_1[5]));
  FDCE \SRC_DATA_1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[6]_i_1_n_0 ),
        .Q(SRC_DATA_1[6]));
  FDCE \SRC_DATA_1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[7]_i_1_n_0 ),
        .Q(SRC_DATA_1[7]));
  FDCE \SRC_DATA_1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[8]_i_1_n_0 ),
        .Q(SRC_DATA_1[8]));
  FDCE \SRC_DATA_1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA_1[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\SRC_DATA_1[9]_i_1_n_0 ),
        .Q(SRC_DATA_1[9]));
  FDCE SRC_FIN_1_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(SRC_FIN_1_reg_0),
        .Q(SRC_FIN_1));
  FDCE SRC_VALID_1_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(SRC_VALID_1_reg_0),
        .Q(SRC_VALID_1));
  LUT4 #(
    .INIT(16'hD5C0)) 
    \ack[0]_i_1 
       (.I0(Q[0]),
        .I1(SRC_ACK_1),
        .I2(Q[2]),
        .I3(\ack_reg_n_0_[0] ),
        .O(\ack[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hD5C0)) 
    \ack[1]_i_1 
       (.I0(Q[0]),
        .I1(SRC_ACK_2),
        .I2(Q[2]),
        .I3(\ack_reg_n_0_[1] ),
        .O(\ack[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hD5C0)) 
    \ack[2]_i_1 
       (.I0(Q[0]),
        .I1(SRC_ACK_3),
        .I2(Q[2]),
        .I3(\ack_reg_n_0_[2] ),
        .O(\ack[2]_i_1_n_0 ));
  FDCE \ack_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\ack[0]_i_1_n_0 ),
        .Q(\ack_reg_n_0_[0] ));
  FDCE \ack_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\ack[1]_i_1_n_0 ),
        .Q(\ack_reg_n_0_[1] ));
  FDCE \ack_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\ack[2]_i_1_n_0 ),
        .Q(\ack_reg_n_0_[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(done),
        .I1(dout[0]),
        .I2(\axi_rdata_reg[0] [1]),
        .I3(\my_value_reg[31]_0 [0]),
        .I4(\axi_rdata_reg[0] [0]),
        .I5(\axi_rdata_reg[31] [0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_2 
       (.I0(dout[10]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [10]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_2 
       (.I0(dout[11]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [11]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_2 
       (.I0(dout[12]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [12]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_2 
       (.I0(dout[13]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [13]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_2 
       (.I0(dout[14]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [14]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_2 
       (.I0(dout[15]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [15]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_2 
       (.I0(dout[16]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [16]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_2 
       (.I0(dout[17]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [17]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_2 
       (.I0(dout[18]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [18]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_2 
       (.I0(dout[19]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [19]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_2 
       (.I0(dout[1]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [1]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_2 
       (.I0(dout[20]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [20]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_2 
       (.I0(dout[21]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [21]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_2 
       (.I0(dout[22]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [22]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_2 
       (.I0(dout[23]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [23]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_2 
       (.I0(dout[24]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [24]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_2 
       (.I0(dout[25]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [25]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_2 
       (.I0(dout[26]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [26]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_2 
       (.I0(dout[27]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [27]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_2 
       (.I0(dout[28]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [28]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_2 
       (.I0(dout[29]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [29]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_2 
       (.I0(dout[2]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [2]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_2 
       (.I0(dout[30]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [30]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_2 
       (.I0(dout[31]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [31]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [31]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_2 
       (.I0(dout[3]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [3]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_2 
       (.I0(dout[4]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [4]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_2 
       (.I0(dout[5]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [5]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_2 
       (.I0(dout[6]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [6]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_2 
       (.I0(dout[7]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [7]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_2 
       (.I0(dout[8]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [8]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_2 
       (.I0(dout[9]),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\my_value_reg[31]_0 [9]),
        .I3(\axi_rdata_reg[0] [0]),
        .I4(\axi_rdata_reg[31] [9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  MUXF7 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata_reg[0]_0 ),
        .O(D[0]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(\axi_rdata_reg[10] ),
        .O(D[10]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(\axi_rdata_reg[11] ),
        .O(D[11]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(\axi_rdata_reg[12] ),
        .O(D[12]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(\axi_rdata_reg[13] ),
        .O(D[13]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(\axi_rdata_reg[14] ),
        .O(D[14]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata_reg[15] ),
        .O(D[15]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(\axi_rdata_reg[16] ),
        .O(D[16]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(\axi_rdata_reg[17] ),
        .O(D[17]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(\axi_rdata_reg[18] ),
        .O(D[18]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(\axi_rdata_reg[19] ),
        .O(D[19]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata_reg[1] ),
        .O(D[1]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(\axi_rdata_reg[20] ),
        .O(D[20]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(\axi_rdata_reg[21] ),
        .O(D[21]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(\axi_rdata_reg[22] ),
        .O(D[22]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(\axi_rdata_reg[23] ),
        .O(D[23]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(\axi_rdata_reg[24] ),
        .O(D[24]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(\axi_rdata_reg[25] ),
        .O(D[25]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(\axi_rdata_reg[26] ),
        .O(D[26]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(\axi_rdata_reg[27] ),
        .O(D[27]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(\axi_rdata_reg[28] ),
        .O(D[28]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(\axi_rdata_reg[29] ),
        .O(D[29]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata_reg[2] ),
        .O(D[2]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(\axi_rdata_reg[30] ),
        .O(D[30]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[31]_i_1 
       (.I0(\axi_rdata[31]_i_2_n_0 ),
        .I1(\axi_rdata_reg[31]_0 ),
        .O(D[31]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata_reg[3] ),
        .O(D[3]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata_reg[4] ),
        .O(D[4]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\axi_rdata_reg[5] ),
        .O(D[5]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\axi_rdata_reg[6] ),
        .O(D[6]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(\axi_rdata_reg[7] ),
        .O(D[7]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(\axi_rdata_reg[8] ),
        .O(D[8]),
        .S(\axi_rdata_reg[0] [2]));
  MUXF7 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(\axi_rdata_reg[9] ),
        .O(D[9]),
        .S(\axi_rdata_reg[0] [2]));
  FDCE done_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(done_reg_0),
        .Q(done));
  CARRY8 dout0__2_carry
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({dout0__2_carry_n_0,dout0__2_carry_n_1,dout0__2_carry_n_2,dout0__2_carry_n_3,dout0__2_carry_n_4,dout0__2_carry_n_5,dout0__2_carry_n_6,dout0__2_carry_n_7}),
        .DI({dout0__2_carry_i_1_n_0,dout0__2_carry_i_2_n_0,dout0__2_carry_i_3_n_0,dout0__2_carry_i_4_n_0,dout0__2_carry_i_5_n_0,dout0__2_carry_i_6_n_0,dout0__2_carry_i_7_n_0,\my_value_reg_n_0_[0] }),
        .O(in7[7:0]),
        .S({dout0__2_carry_i_8_n_0,dout0__2_carry_i_9_n_0,dout0__2_carry_i_10_n_0,dout0__2_carry_i_11_n_0,dout0__2_carry_i_12_n_0,dout0__2_carry_i_13_n_0,dout0__2_carry_i_14_n_0,dout0__2_carry_i_15_n_0}));
  CARRY8 dout0__2_carry__0
       (.CI(dout0__2_carry_n_0),
        .CI_TOP(1'b0),
        .CO({dout0__2_carry__0_n_0,dout0__2_carry__0_n_1,dout0__2_carry__0_n_2,dout0__2_carry__0_n_3,dout0__2_carry__0_n_4,dout0__2_carry__0_n_5,dout0__2_carry__0_n_6,dout0__2_carry__0_n_7}),
        .DI({dout0__2_carry__0_i_1_n_0,dout0__2_carry__0_i_2_n_0,dout0__2_carry__0_i_3_n_0,dout0__2_carry__0_i_4_n_0,dout0__2_carry__0_i_5_n_0,dout0__2_carry__0_i_6_n_0,dout0__2_carry__0_i_7_n_0,dout0__2_carry__0_i_8_n_0}),
        .O(in7[15:8]),
        .S({dout0__2_carry__0_i_9_n_0,dout0__2_carry__0_i_10_n_0,dout0__2_carry__0_i_11_n_0,dout0__2_carry__0_i_12_n_0,dout0__2_carry__0_i_13_n_0,dout0__2_carry__0_i_14_n_0,dout0__2_carry__0_i_15_n_0,dout0__2_carry__0_i_16_n_0}));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_1
       (.I0(\my_value_reg_n_0_[14] ),
        .I1(dout0__2_carry__0_i_17_n_0),
        .I2(p_0_in0_in[13]),
        .I3(p_0_in[13]),
        .I4(\others_value_reg_n_0_[13] ),
        .O(dout0__2_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_10
       (.I0(dout0__2_carry__0_i_2_n_0),
        .I1(dout0__2_carry__0_i_17_n_0),
        .I2(\my_value_reg_n_0_[14] ),
        .I3(\others_value_reg_n_0_[13] ),
        .I4(p_0_in[13]),
        .I5(p_0_in0_in[13]),
        .O(dout0__2_carry__0_i_10_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_11
       (.I0(dout0__2_carry__0_i_3_n_0),
        .I1(dout0__2_carry__0_i_18_n_0),
        .I2(\my_value_reg_n_0_[13] ),
        .I3(\others_value_reg_n_0_[12] ),
        .I4(p_0_in[12]),
        .I5(p_0_in0_in[12]),
        .O(dout0__2_carry__0_i_11_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_12
       (.I0(dout0__2_carry__0_i_4_n_0),
        .I1(dout0__2_carry__0_i_19_n_0),
        .I2(\my_value_reg_n_0_[12] ),
        .I3(\others_value_reg_n_0_[11] ),
        .I4(p_0_in[11]),
        .I5(p_0_in0_in[11]),
        .O(dout0__2_carry__0_i_12_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_13
       (.I0(dout0__2_carry__0_i_5_n_0),
        .I1(dout0__2_carry__0_i_20_n_0),
        .I2(\my_value_reg_n_0_[11] ),
        .I3(\others_value_reg_n_0_[10] ),
        .I4(p_0_in[10]),
        .I5(p_0_in0_in[10]),
        .O(dout0__2_carry__0_i_13_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_14
       (.I0(dout0__2_carry__0_i_6_n_0),
        .I1(dout0__2_carry__0_i_21_n_0),
        .I2(\my_value_reg_n_0_[10] ),
        .I3(\others_value_reg_n_0_[9] ),
        .I4(p_0_in[9]),
        .I5(p_0_in0_in[9]),
        .O(dout0__2_carry__0_i_14_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_15
       (.I0(dout0__2_carry__0_i_7_n_0),
        .I1(dout0__2_carry__0_i_22_n_0),
        .I2(\my_value_reg_n_0_[9] ),
        .I3(\others_value_reg_n_0_[8] ),
        .I4(p_0_in[8]),
        .I5(p_0_in0_in[8]),
        .O(dout0__2_carry__0_i_15_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_16
       (.I0(dout0__2_carry__0_i_8_n_0),
        .I1(dout0__2_carry__0_i_23_n_0),
        .I2(\my_value_reg_n_0_[8] ),
        .I3(\others_value_reg_n_0_[7] ),
        .I4(p_0_in[7]),
        .I5(p_0_in0_in[7]),
        .O(dout0__2_carry__0_i_16_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_17
       (.I0(p_0_in0_in[14]),
        .I1(\others_value_reg_n_0_[14] ),
        .I2(p_0_in[14]),
        .O(dout0__2_carry__0_i_17_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_18
       (.I0(p_0_in0_in[13]),
        .I1(\others_value_reg_n_0_[13] ),
        .I2(p_0_in[13]),
        .O(dout0__2_carry__0_i_18_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_19
       (.I0(p_0_in0_in[12]),
        .I1(\others_value_reg_n_0_[12] ),
        .I2(p_0_in[12]),
        .O(dout0__2_carry__0_i_19_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_2
       (.I0(\my_value_reg_n_0_[13] ),
        .I1(dout0__2_carry__0_i_18_n_0),
        .I2(p_0_in0_in[12]),
        .I3(p_0_in[12]),
        .I4(\others_value_reg_n_0_[12] ),
        .O(dout0__2_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_20
       (.I0(p_0_in0_in[11]),
        .I1(\others_value_reg_n_0_[11] ),
        .I2(p_0_in[11]),
        .O(dout0__2_carry__0_i_20_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_21
       (.I0(p_0_in0_in[10]),
        .I1(\others_value_reg_n_0_[10] ),
        .I2(p_0_in[10]),
        .O(dout0__2_carry__0_i_21_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_22
       (.I0(p_0_in0_in[9]),
        .I1(\others_value_reg_n_0_[9] ),
        .I2(p_0_in[9]),
        .O(dout0__2_carry__0_i_22_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_23
       (.I0(p_0_in0_in[8]),
        .I1(\others_value_reg_n_0_[8] ),
        .I2(p_0_in[8]),
        .O(dout0__2_carry__0_i_23_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__0_i_24
       (.I0(p_0_in0_in[15]),
        .I1(\others_value_reg_n_0_[15] ),
        .I2(p_0_in[15]),
        .O(dout0__2_carry__0_i_24_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_3
       (.I0(\my_value_reg_n_0_[12] ),
        .I1(dout0__2_carry__0_i_19_n_0),
        .I2(p_0_in0_in[11]),
        .I3(p_0_in[11]),
        .I4(\others_value_reg_n_0_[11] ),
        .O(dout0__2_carry__0_i_3_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_4
       (.I0(\my_value_reg_n_0_[11] ),
        .I1(dout0__2_carry__0_i_20_n_0),
        .I2(p_0_in0_in[10]),
        .I3(p_0_in[10]),
        .I4(\others_value_reg_n_0_[10] ),
        .O(dout0__2_carry__0_i_4_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_5
       (.I0(\my_value_reg_n_0_[10] ),
        .I1(dout0__2_carry__0_i_21_n_0),
        .I2(p_0_in0_in[9]),
        .I3(p_0_in[9]),
        .I4(\others_value_reg_n_0_[9] ),
        .O(dout0__2_carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_6
       (.I0(\my_value_reg_n_0_[9] ),
        .I1(dout0__2_carry__0_i_22_n_0),
        .I2(p_0_in0_in[8]),
        .I3(p_0_in[8]),
        .I4(\others_value_reg_n_0_[8] ),
        .O(dout0__2_carry__0_i_6_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_7
       (.I0(\my_value_reg_n_0_[8] ),
        .I1(dout0__2_carry__0_i_23_n_0),
        .I2(p_0_in0_in[7]),
        .I3(p_0_in[7]),
        .I4(\others_value_reg_n_0_[7] ),
        .O(dout0__2_carry__0_i_7_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__0_i_8
       (.I0(\my_value_reg_n_0_[7] ),
        .I1(dout0__2_carry_i_21_n_0),
        .I2(p_0_in0_in[6]),
        .I3(p_0_in[6]),
        .I4(\others_value_reg_n_0_[6] ),
        .O(dout0__2_carry__0_i_8_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__0_i_9
       (.I0(dout0__2_carry__0_i_1_n_0),
        .I1(dout0__2_carry__0_i_24_n_0),
        .I2(\my_value_reg_n_0_[15] ),
        .I3(\others_value_reg_n_0_[14] ),
        .I4(p_0_in[14]),
        .I5(p_0_in0_in[14]),
        .O(dout0__2_carry__0_i_9_n_0));
  CARRY8 dout0__2_carry__1
       (.CI(dout0__2_carry__0_n_0),
        .CI_TOP(1'b0),
        .CO({dout0__2_carry__1_n_0,dout0__2_carry__1_n_1,dout0__2_carry__1_n_2,dout0__2_carry__1_n_3,dout0__2_carry__1_n_4,dout0__2_carry__1_n_5,dout0__2_carry__1_n_6,dout0__2_carry__1_n_7}),
        .DI({dout0__2_carry__1_i_1_n_0,dout0__2_carry__1_i_2_n_0,dout0__2_carry__1_i_3_n_0,dout0__2_carry__1_i_4_n_0,dout0__2_carry__1_i_5_n_0,dout0__2_carry__1_i_6_n_0,dout0__2_carry__1_i_7_n_0,dout0__2_carry__1_i_8_n_0}),
        .O(in7[23:16]),
        .S({dout0__2_carry__1_i_9_n_0,dout0__2_carry__1_i_10_n_0,dout0__2_carry__1_i_11_n_0,dout0__2_carry__1_i_12_n_0,dout0__2_carry__1_i_13_n_0,dout0__2_carry__1_i_14_n_0,dout0__2_carry__1_i_15_n_0,dout0__2_carry__1_i_16_n_0}));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_1
       (.I0(\my_value_reg_n_0_[22] ),
        .I1(dout0__2_carry__1_i_17_n_0),
        .I2(p_0_in0_in[21]),
        .I3(p_0_in[21]),
        .I4(\others_value_reg_n_0_[21] ),
        .O(dout0__2_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_10
       (.I0(dout0__2_carry__1_i_2_n_0),
        .I1(dout0__2_carry__1_i_17_n_0),
        .I2(\my_value_reg_n_0_[22] ),
        .I3(\others_value_reg_n_0_[21] ),
        .I4(p_0_in[21]),
        .I5(p_0_in0_in[21]),
        .O(dout0__2_carry__1_i_10_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_11
       (.I0(dout0__2_carry__1_i_3_n_0),
        .I1(dout0__2_carry__1_i_18_n_0),
        .I2(\my_value_reg_n_0_[21] ),
        .I3(\others_value_reg_n_0_[20] ),
        .I4(p_0_in[20]),
        .I5(p_0_in0_in[20]),
        .O(dout0__2_carry__1_i_11_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_12
       (.I0(dout0__2_carry__1_i_4_n_0),
        .I1(dout0__2_carry__1_i_19_n_0),
        .I2(\my_value_reg_n_0_[20] ),
        .I3(\others_value_reg_n_0_[19] ),
        .I4(p_0_in[19]),
        .I5(p_0_in0_in[19]),
        .O(dout0__2_carry__1_i_12_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_13
       (.I0(dout0__2_carry__1_i_5_n_0),
        .I1(dout0__2_carry__1_i_20_n_0),
        .I2(\my_value_reg_n_0_[19] ),
        .I3(\others_value_reg_n_0_[18] ),
        .I4(p_0_in[18]),
        .I5(p_0_in0_in[18]),
        .O(dout0__2_carry__1_i_13_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_14
       (.I0(dout0__2_carry__1_i_6_n_0),
        .I1(dout0__2_carry__1_i_21_n_0),
        .I2(\my_value_reg_n_0_[18] ),
        .I3(\others_value_reg_n_0_[17] ),
        .I4(p_0_in[17]),
        .I5(p_0_in0_in[17]),
        .O(dout0__2_carry__1_i_14_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_15
       (.I0(dout0__2_carry__1_i_7_n_0),
        .I1(dout0__2_carry__1_i_22_n_0),
        .I2(\my_value_reg_n_0_[17] ),
        .I3(\others_value_reg_n_0_[16] ),
        .I4(p_0_in[16]),
        .I5(p_0_in0_in[16]),
        .O(dout0__2_carry__1_i_15_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_16
       (.I0(dout0__2_carry__1_i_8_n_0),
        .I1(dout0__2_carry__1_i_23_n_0),
        .I2(\my_value_reg_n_0_[16] ),
        .I3(\others_value_reg_n_0_[15] ),
        .I4(p_0_in[15]),
        .I5(p_0_in0_in[15]),
        .O(dout0__2_carry__1_i_16_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_17
       (.I0(p_0_in0_in[22]),
        .I1(\others_value_reg_n_0_[22] ),
        .I2(p_0_in[22]),
        .O(dout0__2_carry__1_i_17_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_18
       (.I0(p_0_in0_in[21]),
        .I1(\others_value_reg_n_0_[21] ),
        .I2(p_0_in[21]),
        .O(dout0__2_carry__1_i_18_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_19
       (.I0(p_0_in0_in[20]),
        .I1(\others_value_reg_n_0_[20] ),
        .I2(p_0_in[20]),
        .O(dout0__2_carry__1_i_19_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_2
       (.I0(\my_value_reg_n_0_[21] ),
        .I1(dout0__2_carry__1_i_18_n_0),
        .I2(p_0_in0_in[20]),
        .I3(p_0_in[20]),
        .I4(\others_value_reg_n_0_[20] ),
        .O(dout0__2_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_20
       (.I0(p_0_in0_in[19]),
        .I1(\others_value_reg_n_0_[19] ),
        .I2(p_0_in[19]),
        .O(dout0__2_carry__1_i_20_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_21
       (.I0(p_0_in0_in[18]),
        .I1(\others_value_reg_n_0_[18] ),
        .I2(p_0_in[18]),
        .O(dout0__2_carry__1_i_21_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_22
       (.I0(p_0_in0_in[17]),
        .I1(\others_value_reg_n_0_[17] ),
        .I2(p_0_in[17]),
        .O(dout0__2_carry__1_i_22_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_23
       (.I0(p_0_in0_in[16]),
        .I1(\others_value_reg_n_0_[16] ),
        .I2(p_0_in[16]),
        .O(dout0__2_carry__1_i_23_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__1_i_24
       (.I0(p_0_in0_in[23]),
        .I1(\others_value_reg_n_0_[23] ),
        .I2(p_0_in[23]),
        .O(dout0__2_carry__1_i_24_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_3
       (.I0(\my_value_reg_n_0_[20] ),
        .I1(dout0__2_carry__1_i_19_n_0),
        .I2(p_0_in0_in[19]),
        .I3(p_0_in[19]),
        .I4(\others_value_reg_n_0_[19] ),
        .O(dout0__2_carry__1_i_3_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_4
       (.I0(\my_value_reg_n_0_[19] ),
        .I1(dout0__2_carry__1_i_20_n_0),
        .I2(p_0_in0_in[18]),
        .I3(p_0_in[18]),
        .I4(\others_value_reg_n_0_[18] ),
        .O(dout0__2_carry__1_i_4_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_5
       (.I0(\my_value_reg_n_0_[18] ),
        .I1(dout0__2_carry__1_i_21_n_0),
        .I2(p_0_in0_in[17]),
        .I3(p_0_in[17]),
        .I4(\others_value_reg_n_0_[17] ),
        .O(dout0__2_carry__1_i_5_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_6
       (.I0(\my_value_reg_n_0_[17] ),
        .I1(dout0__2_carry__1_i_22_n_0),
        .I2(p_0_in0_in[16]),
        .I3(p_0_in[16]),
        .I4(\others_value_reg_n_0_[16] ),
        .O(dout0__2_carry__1_i_6_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_7
       (.I0(\my_value_reg_n_0_[16] ),
        .I1(dout0__2_carry__1_i_23_n_0),
        .I2(p_0_in0_in[15]),
        .I3(p_0_in[15]),
        .I4(\others_value_reg_n_0_[15] ),
        .O(dout0__2_carry__1_i_7_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__1_i_8
       (.I0(\my_value_reg_n_0_[15] ),
        .I1(dout0__2_carry__0_i_24_n_0),
        .I2(p_0_in0_in[14]),
        .I3(p_0_in[14]),
        .I4(\others_value_reg_n_0_[14] ),
        .O(dout0__2_carry__1_i_8_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__1_i_9
       (.I0(dout0__2_carry__1_i_1_n_0),
        .I1(dout0__2_carry__1_i_24_n_0),
        .I2(\my_value_reg_n_0_[23] ),
        .I3(\others_value_reg_n_0_[22] ),
        .I4(p_0_in[22]),
        .I5(p_0_in0_in[22]),
        .O(dout0__2_carry__1_i_9_n_0));
  CARRY8 dout0__2_carry__2
       (.CI(dout0__2_carry__1_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_dout0__2_carry__2_CO_UNCONNECTED[7],dout0__2_carry__2_n_1,dout0__2_carry__2_n_2,dout0__2_carry__2_n_3,dout0__2_carry__2_n_4,dout0__2_carry__2_n_5,dout0__2_carry__2_n_6,dout0__2_carry__2_n_7}),
        .DI({1'b0,dout0__2_carry__2_i_1_n_0,dout0__2_carry__2_i_2_n_0,dout0__2_carry__2_i_3_n_0,dout0__2_carry__2_i_4_n_0,dout0__2_carry__2_i_5_n_0,dout0__2_carry__2_i_6_n_0,dout0__2_carry__2_i_7_n_0}),
        .O(in7[31:24]),
        .S({dout0__2_carry__2_i_8_n_0,dout0__2_carry__2_i_9_n_0,dout0__2_carry__2_i_10_n_0,dout0__2_carry__2_i_11_n_0,dout0__2_carry__2_i_12_n_0,dout0__2_carry__2_i_13_n_0,dout0__2_carry__2_i_14_n_0,dout0__2_carry__2_i_15_n_0}));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_1
       (.I0(\my_value_reg_n_0_[29] ),
        .I1(dout0__2_carry__2_i_16_n_0),
        .I2(p_0_in0_in[28]),
        .I3(p_0_in[28]),
        .I4(\others_value_reg_n_0_[28] ),
        .O(dout0__2_carry__2_i_1_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_10
       (.I0(dout0__2_carry__2_i_2_n_0),
        .I1(dout0__2_carry__2_i_16_n_0),
        .I2(\my_value_reg_n_0_[29] ),
        .I3(\others_value_reg_n_0_[28] ),
        .I4(p_0_in[28]),
        .I5(p_0_in0_in[28]),
        .O(dout0__2_carry__2_i_10_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_11
       (.I0(dout0__2_carry__2_i_3_n_0),
        .I1(dout0__2_carry__2_i_17_n_0),
        .I2(\my_value_reg_n_0_[28] ),
        .I3(\others_value_reg_n_0_[27] ),
        .I4(p_0_in[27]),
        .I5(p_0_in0_in[27]),
        .O(dout0__2_carry__2_i_11_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_12
       (.I0(dout0__2_carry__2_i_4_n_0),
        .I1(dout0__2_carry__2_i_18_n_0),
        .I2(\my_value_reg_n_0_[27] ),
        .I3(\others_value_reg_n_0_[26] ),
        .I4(p_0_in[26]),
        .I5(p_0_in0_in[26]),
        .O(dout0__2_carry__2_i_12_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_13
       (.I0(dout0__2_carry__2_i_5_n_0),
        .I1(dout0__2_carry__2_i_19_n_0),
        .I2(\my_value_reg_n_0_[26] ),
        .I3(\others_value_reg_n_0_[25] ),
        .I4(p_0_in[25]),
        .I5(p_0_in0_in[25]),
        .O(dout0__2_carry__2_i_13_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_14
       (.I0(dout0__2_carry__2_i_6_n_0),
        .I1(dout0__2_carry__2_i_20_n_0),
        .I2(\my_value_reg_n_0_[25] ),
        .I3(\others_value_reg_n_0_[24] ),
        .I4(p_0_in[24]),
        .I5(p_0_in0_in[24]),
        .O(dout0__2_carry__2_i_14_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_15
       (.I0(dout0__2_carry__2_i_7_n_0),
        .I1(dout0__2_carry__2_i_21_n_0),
        .I2(\my_value_reg_n_0_[24] ),
        .I3(\others_value_reg_n_0_[23] ),
        .I4(p_0_in[23]),
        .I5(p_0_in0_in[23]),
        .O(dout0__2_carry__2_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_16
       (.I0(p_0_in0_in[29]),
        .I1(\others_value_reg_n_0_[29] ),
        .I2(p_0_in[29]),
        .O(dout0__2_carry__2_i_16_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_17
       (.I0(p_0_in0_in[28]),
        .I1(\others_value_reg_n_0_[28] ),
        .I2(p_0_in[28]),
        .O(dout0__2_carry__2_i_17_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_18
       (.I0(p_0_in0_in[27]),
        .I1(\others_value_reg_n_0_[27] ),
        .I2(p_0_in[27]),
        .O(dout0__2_carry__2_i_18_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_19
       (.I0(p_0_in0_in[26]),
        .I1(\others_value_reg_n_0_[26] ),
        .I2(p_0_in[26]),
        .O(dout0__2_carry__2_i_19_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_2
       (.I0(\my_value_reg_n_0_[28] ),
        .I1(dout0__2_carry__2_i_17_n_0),
        .I2(p_0_in0_in[27]),
        .I3(p_0_in[27]),
        .I4(\others_value_reg_n_0_[27] ),
        .O(dout0__2_carry__2_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_20
       (.I0(p_0_in0_in[25]),
        .I1(\others_value_reg_n_0_[25] ),
        .I2(p_0_in[25]),
        .O(dout0__2_carry__2_i_20_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_21
       (.I0(p_0_in0_in[24]),
        .I1(\others_value_reg_n_0_[24] ),
        .I2(p_0_in[24]),
        .O(dout0__2_carry__2_i_21_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    dout0__2_carry__2_i_22
       (.I0(\others_value_reg_n_0_[29] ),
        .I1(p_0_in[29]),
        .I2(p_0_in0_in[29]),
        .O(dout0__2_carry__2_i_22_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    dout0__2_carry__2_i_23
       (.I0(p_0_in[31]),
        .I1(\others_value_reg_n_0_[31] ),
        .I2(p_0_in0_in[31]),
        .I3(\my_value_reg_n_0_[31] ),
        .O(dout0__2_carry__2_i_23_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry__2_i_24
       (.I0(p_0_in0_in[30]),
        .I1(\others_value_reg_n_0_[30] ),
        .I2(p_0_in[30]),
        .O(dout0__2_carry__2_i_24_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_3
       (.I0(\my_value_reg_n_0_[27] ),
        .I1(dout0__2_carry__2_i_18_n_0),
        .I2(p_0_in0_in[26]),
        .I3(p_0_in[26]),
        .I4(\others_value_reg_n_0_[26] ),
        .O(dout0__2_carry__2_i_3_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_4
       (.I0(\my_value_reg_n_0_[26] ),
        .I1(dout0__2_carry__2_i_19_n_0),
        .I2(p_0_in0_in[25]),
        .I3(p_0_in[25]),
        .I4(\others_value_reg_n_0_[25] ),
        .O(dout0__2_carry__2_i_4_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_5
       (.I0(\my_value_reg_n_0_[25] ),
        .I1(dout0__2_carry__2_i_20_n_0),
        .I2(p_0_in0_in[24]),
        .I3(p_0_in[24]),
        .I4(\others_value_reg_n_0_[24] ),
        .O(dout0__2_carry__2_i_5_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_6
       (.I0(\my_value_reg_n_0_[24] ),
        .I1(dout0__2_carry__2_i_21_n_0),
        .I2(p_0_in0_in[23]),
        .I3(p_0_in[23]),
        .I4(\others_value_reg_n_0_[23] ),
        .O(dout0__2_carry__2_i_6_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry__2_i_7
       (.I0(\my_value_reg_n_0_[23] ),
        .I1(dout0__2_carry__1_i_24_n_0),
        .I2(p_0_in0_in[22]),
        .I3(p_0_in[22]),
        .I4(\others_value_reg_n_0_[22] ),
        .O(dout0__2_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'hE187871E871E1E78)) 
    dout0__2_carry__2_i_8
       (.I0(dout0__2_carry__2_i_22_n_0),
        .I1(\my_value_reg_n_0_[30] ),
        .I2(dout0__2_carry__2_i_23_n_0),
        .I3(\others_value_reg_n_0_[30] ),
        .I4(p_0_in[30]),
        .I5(p_0_in0_in[30]),
        .O(dout0__2_carry__2_i_8_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry__2_i_9
       (.I0(dout0__2_carry__2_i_1_n_0),
        .I1(dout0__2_carry__2_i_24_n_0),
        .I2(\my_value_reg_n_0_[30] ),
        .I3(\others_value_reg_n_0_[29] ),
        .I4(p_0_in[29]),
        .I5(p_0_in0_in[29]),
        .O(dout0__2_carry__2_i_9_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry_i_1
       (.I0(\my_value_reg_n_0_[6] ),
        .I1(dout0__2_carry_i_16_n_0),
        .I2(p_0_in0_in[5]),
        .I3(p_0_in[5]),
        .I4(\others_value_reg_n_0_[5] ),
        .O(dout0__2_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry_i_10
       (.I0(dout0__2_carry_i_3_n_0),
        .I1(dout0__2_carry_i_17_n_0),
        .I2(\my_value_reg_n_0_[5] ),
        .I3(\others_value_reg_n_0_[4] ),
        .I4(p_0_in[4]),
        .I5(p_0_in0_in[4]),
        .O(dout0__2_carry_i_10_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry_i_11
       (.I0(dout0__2_carry_i_4_n_0),
        .I1(dout0__2_carry_i_18_n_0),
        .I2(\my_value_reg_n_0_[4] ),
        .I3(\others_value_reg_n_0_[3] ),
        .I4(p_0_in[3]),
        .I5(p_0_in0_in[3]),
        .O(dout0__2_carry_i_11_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry_i_12
       (.I0(dout0__2_carry_i_5_n_0),
        .I1(dout0__2_carry_i_19_n_0),
        .I2(\my_value_reg_n_0_[3] ),
        .I3(\others_value_reg_n_0_[2] ),
        .I4(p_0_in[2]),
        .I5(p_0_in0_in[2]),
        .O(dout0__2_carry_i_12_n_0));
  LUT6 #(
    .INIT(64'h6999999699969666)) 
    dout0__2_carry_i_13
       (.I0(dout0__2_carry_i_20_n_0),
        .I1(\my_value_reg_n_0_[2] ),
        .I2(p_0_in0_in[1]),
        .I3(\others_value_reg_n_0_[1] ),
        .I4(p_0_in[1]),
        .I5(\my_value_reg_n_0_[1] ),
        .O(dout0__2_carry_i_13_n_0));
  LUT4 #(
    .INIT(16'h566A)) 
    dout0__2_carry_i_14
       (.I0(dout0__2_carry_i_7_n_0),
        .I1(p_0_in0_in[0]),
        .I2(p_0_in[0]),
        .I3(\others_value_reg_n_0_[0] ),
        .O(dout0__2_carry_i_14_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    dout0__2_carry_i_15
       (.I0(p_0_in[0]),
        .I1(\others_value_reg_n_0_[0] ),
        .I2(p_0_in0_in[0]),
        .I3(\my_value_reg_n_0_[0] ),
        .O(dout0__2_carry_i_15_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry_i_16
       (.I0(p_0_in0_in[6]),
        .I1(\others_value_reg_n_0_[6] ),
        .I2(p_0_in[6]),
        .O(dout0__2_carry_i_16_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry_i_17
       (.I0(p_0_in0_in[5]),
        .I1(\others_value_reg_n_0_[5] ),
        .I2(p_0_in[5]),
        .O(dout0__2_carry_i_17_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry_i_18
       (.I0(p_0_in0_in[4]),
        .I1(\others_value_reg_n_0_[4] ),
        .I2(p_0_in[4]),
        .O(dout0__2_carry_i_18_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry_i_19
       (.I0(p_0_in0_in[3]),
        .I1(\others_value_reg_n_0_[3] ),
        .I2(p_0_in[3]),
        .O(dout0__2_carry_i_19_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry_i_2
       (.I0(\my_value_reg_n_0_[5] ),
        .I1(dout0__2_carry_i_17_n_0),
        .I2(p_0_in0_in[4]),
        .I3(p_0_in[4]),
        .I4(\others_value_reg_n_0_[4] ),
        .O(dout0__2_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry_i_20
       (.I0(p_0_in0_in[2]),
        .I1(\others_value_reg_n_0_[2] ),
        .I2(p_0_in[2]),
        .O(dout0__2_carry_i_20_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    dout0__2_carry_i_21
       (.I0(p_0_in0_in[7]),
        .I1(\others_value_reg_n_0_[7] ),
        .I2(p_0_in[7]),
        .O(dout0__2_carry_i_21_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry_i_3
       (.I0(\my_value_reg_n_0_[4] ),
        .I1(dout0__2_carry_i_18_n_0),
        .I2(p_0_in0_in[3]),
        .I3(p_0_in[3]),
        .I4(\others_value_reg_n_0_[3] ),
        .O(dout0__2_carry_i_3_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry_i_4
       (.I0(\my_value_reg_n_0_[3] ),
        .I1(dout0__2_carry_i_19_n_0),
        .I2(p_0_in0_in[2]),
        .I3(p_0_in[2]),
        .I4(\others_value_reg_n_0_[2] ),
        .O(dout0__2_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    dout0__2_carry_i_5
       (.I0(\my_value_reg_n_0_[2] ),
        .I1(dout0__2_carry_i_20_n_0),
        .I2(p_0_in0_in[1]),
        .I3(p_0_in[1]),
        .I4(\others_value_reg_n_0_[1] ),
        .O(dout0__2_carry_i_5_n_0));
  LUT5 #(
    .INIT(32'hE81717E8)) 
    dout0__2_carry_i_6
       (.I0(p_0_in0_in[1]),
        .I1(p_0_in[1]),
        .I2(\others_value_reg_n_0_[1] ),
        .I3(\my_value_reg_n_0_[2] ),
        .I4(dout0__2_carry_i_20_n_0),
        .O(dout0__2_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    dout0__2_carry_i_7
       (.I0(p_0_in[1]),
        .I1(\others_value_reg_n_0_[1] ),
        .I2(p_0_in0_in[1]),
        .I3(\my_value_reg_n_0_[1] ),
        .O(dout0__2_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry_i_8
       (.I0(dout0__2_carry_i_1_n_0),
        .I1(dout0__2_carry_i_21_n_0),
        .I2(\my_value_reg_n_0_[7] ),
        .I3(\others_value_reg_n_0_[6] ),
        .I4(p_0_in[6]),
        .I5(p_0_in0_in[6]),
        .O(dout0__2_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    dout0__2_carry_i_9
       (.I0(dout0__2_carry_i_2_n_0),
        .I1(dout0__2_carry_i_16_n_0),
        .I2(\my_value_reg_n_0_[6] ),
        .I3(\others_value_reg_n_0_[5] ),
        .I4(p_0_in[5]),
        .I5(p_0_in0_in[5]),
        .O(dout0__2_carry_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[0]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[0]),
        .O(\dout[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[10]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[10]),
        .O(\dout[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[11]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[11]),
        .O(\dout[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[12]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[12]),
        .O(\dout[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[13]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[13]),
        .O(\dout[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[14]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[14]),
        .O(\dout[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[15]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[15]),
        .O(\dout[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[16]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[16]),
        .O(\dout[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[17]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[17]),
        .O(\dout[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[18]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[18]),
        .O(\dout[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[19]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[19]),
        .O(\dout[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[1]),
        .O(\dout[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[20]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[20]),
        .O(\dout[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[21]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[21]),
        .O(\dout[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[22]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[22]),
        .O(\dout[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[23]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[23]),
        .O(\dout[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[24]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[24]),
        .O(\dout[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[25]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[25]),
        .O(\dout[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[26]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[26]),
        .O(\dout[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[27]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[27]),
        .O(\dout[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[28]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[28]),
        .O(\dout[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[29]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[29]),
        .O(\dout[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[2]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[2]),
        .O(\dout[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[30]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[30]),
        .O(\dout[30]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \dout[31]_i_1 
       (.I0(Q[0]),
        .I1(\FSM_onehot_state_reg_n_0_[6] ),
        .O(\dout[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[31]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[31]),
        .O(\dout[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[3]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[3]),
        .O(\dout[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[4]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[4]),
        .O(\dout[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[5]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[5]),
        .O(\dout[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[6]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[6]),
        .O(\dout[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[7]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[7]),
        .O(\dout[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[8]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[8]),
        .O(\dout[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[9]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(in7[9]),
        .O(\dout[9]_i_1_n_0 ));
  FDCE \dout_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[0]_i_1_n_0 ),
        .Q(dout[0]));
  FDCE \dout_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[10]_i_1_n_0 ),
        .Q(dout[10]));
  FDCE \dout_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[11]_i_1_n_0 ),
        .Q(dout[11]));
  FDCE \dout_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[12]_i_1_n_0 ),
        .Q(dout[12]));
  FDCE \dout_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[13]_i_1_n_0 ),
        .Q(dout[13]));
  FDCE \dout_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[14]_i_1_n_0 ),
        .Q(dout[14]));
  FDCE \dout_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[15]_i_1_n_0 ),
        .Q(dout[15]));
  FDCE \dout_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[16]_i_1_n_0 ),
        .Q(dout[16]));
  FDCE \dout_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[17]_i_1_n_0 ),
        .Q(dout[17]));
  FDCE \dout_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[18]_i_1_n_0 ),
        .Q(dout[18]));
  FDCE \dout_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[19]_i_1_n_0 ),
        .Q(dout[19]));
  FDCE \dout_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[1]_i_1_n_0 ),
        .Q(dout[1]));
  FDCE \dout_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[20]_i_1_n_0 ),
        .Q(dout[20]));
  FDCE \dout_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[21]_i_1_n_0 ),
        .Q(dout[21]));
  FDCE \dout_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[22]_i_1_n_0 ),
        .Q(dout[22]));
  FDCE \dout_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[23]_i_1_n_0 ),
        .Q(dout[23]));
  FDCE \dout_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[24]_i_1_n_0 ),
        .Q(dout[24]));
  FDCE \dout_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[25]_i_1_n_0 ),
        .Q(dout[25]));
  FDCE \dout_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[26]_i_1_n_0 ),
        .Q(dout[26]));
  FDCE \dout_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[27]_i_1_n_0 ),
        .Q(dout[27]));
  FDCE \dout_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[28]_i_1_n_0 ),
        .Q(dout[28]));
  FDCE \dout_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[29]_i_1_n_0 ),
        .Q(dout[29]));
  FDCE \dout_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[2]_i_1_n_0 ),
        .Q(dout[2]));
  FDCE \dout_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[30]_i_1_n_0 ),
        .Q(dout[30]));
  FDCE \dout_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[31]_i_2_n_0 ),
        .Q(dout[31]));
  FDCE \dout_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[3]_i_1_n_0 ),
        .Q(dout[3]));
  FDCE \dout_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[4]_i_1_n_0 ),
        .Q(dout[4]));
  FDCE \dout_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[5]_i_1_n_0 ),
        .Q(dout[5]));
  FDCE \dout_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[6]_i_1_n_0 ),
        .Q(dout[6]));
  FDCE \dout_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[7]_i_1_n_0 ),
        .Q(dout[7]));
  FDCE \dout_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[8]_i_1_n_0 ),
        .Q(dout[8]));
  FDCE \dout_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\dout[9]_i_1_n_0 ),
        .Q(dout[9]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \fin[0]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_FIN_1),
        .I2(Q[0]),
        .I3(fin[0]),
        .O(\fin[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \fin[1]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_FIN_2),
        .I2(Q[0]),
        .I3(fin[1]),
        .O(\fin[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \fin[2]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_FIN_3),
        .I2(Q[0]),
        .I3(fin[2]),
        .O(\fin[2]_i_1_n_0 ));
  FDCE \fin_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\fin[0]_i_1_n_0 ),
        .Q(fin[0]));
  FDCE \fin_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\fin[1]_i_1_n_0 ),
        .Q(fin[1]));
  FDCE \fin_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\fin[2]_i_1_n_0 ),
        .Q(fin[2]));
  LUT5 #(
    .INIT(32'hAAA80000)) 
    \my_value[0]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\my_value[0]_i_2_n_0 ),
        .I2(\my_value[0]_i_3_n_0 ),
        .I3(\my_value[0]_i_4_n_0 ),
        .I4(\my_value_reg[31]_0 [0]),
        .O(\my_value[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFBFFF)) 
    \my_value[0]_i_2 
       (.I0(\my_value[0]_i_5_n_0 ),
        .I1(\my_value_reg[31]_0 [30]),
        .I2(\my_value_reg[31]_0 [29]),
        .I3(\my_value_reg[31]_0 [31]),
        .I4(\my_value[0]_i_6_n_0 ),
        .I5(\my_value[0]_i_7_n_0 ),
        .O(\my_value[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \my_value[0]_i_3 
       (.I0(\my_value_reg[31]_0 [6]),
        .I1(\my_value_reg[31]_0 [5]),
        .I2(\my_value_reg[31]_0 [8]),
        .I3(\my_value_reg[31]_0 [7]),
        .I4(\my_value[0]_i_8_n_0 ),
        .O(\my_value[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \my_value[0]_i_4 
       (.I0(\my_value_reg[31]_0 [14]),
        .I1(\my_value_reg[31]_0 [13]),
        .I2(\my_value_reg[31]_0 [16]),
        .I3(\my_value_reg[31]_0 [15]),
        .I4(\my_value[0]_i_9_n_0 ),
        .O(\my_value[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \my_value[0]_i_5 
       (.I0(\my_value_reg[31]_0 [27]),
        .I1(\my_value_reg[31]_0 [28]),
        .I2(\my_value_reg[31]_0 [25]),
        .I3(\my_value_reg[31]_0 [26]),
        .O(\my_value[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \my_value[0]_i_6 
       (.I0(\my_value_reg[31]_0 [19]),
        .I1(\my_value_reg[31]_0 [20]),
        .I2(\my_value_reg[31]_0 [17]),
        .I3(\my_value_reg[31]_0 [18]),
        .O(\my_value[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \my_value[0]_i_7 
       (.I0(\my_value_reg[31]_0 [23]),
        .I1(\my_value_reg[31]_0 [24]),
        .I2(\my_value_reg[31]_0 [21]),
        .I3(\my_value_reg[31]_0 [22]),
        .O(\my_value[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \my_value[0]_i_8 
       (.I0(\my_value_reg[31]_0 [3]),
        .I1(\my_value_reg[31]_0 [4]),
        .I2(\my_value_reg[31]_0 [1]),
        .I3(\my_value_reg[31]_0 [2]),
        .O(\my_value[0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \my_value[0]_i_9 
       (.I0(\my_value_reg[31]_0 [11]),
        .I1(\my_value_reg[31]_0 [12]),
        .I2(\my_value_reg[31]_0 [9]),
        .I3(\my_value_reg[31]_0 [10]),
        .O(\my_value[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[10]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [10]),
        .O(\my_value[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[11]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [11]),
        .O(\my_value[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[12]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [12]),
        .O(\my_value[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[13]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [13]),
        .O(\my_value[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[14]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [14]),
        .O(\my_value[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[15]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [15]),
        .O(\my_value[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[16]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [16]),
        .O(\my_value[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[17]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [17]),
        .O(\my_value[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[18]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [18]),
        .O(\my_value[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[19]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [19]),
        .O(\my_value[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [1]),
        .O(\my_value[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[20]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [20]),
        .O(\my_value[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[21]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [21]),
        .O(\my_value[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[22]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [22]),
        .O(\my_value[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[23]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [23]),
        .O(\my_value[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[24]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [24]),
        .O(\my_value[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[25]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [25]),
        .O(\my_value[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[26]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [26]),
        .O(\my_value[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[27]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [27]),
        .O(\my_value[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[28]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [28]),
        .O(\my_value[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[29]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [29]),
        .O(\my_value[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[2]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [2]),
        .O(\my_value[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[30]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [30]),
        .O(\my_value[30]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \my_value[31]_i_1 
       (.I0(Q[0]),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .O(\my_value[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[31]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [31]),
        .O(\my_value[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[3]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [3]),
        .O(\my_value[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[4]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [4]),
        .O(\my_value[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[5]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [5]),
        .O(\my_value[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[6]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [6]),
        .O(\my_value[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[7]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [7]),
        .O(\my_value[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[8]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [8]),
        .O(\my_value[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
    \my_value[9]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(\FSM_onehot_state[5]_i_2_n_0 ),
        .I2(\FSM_onehot_state[5]_i_3_n_0 ),
        .I3(\FSM_onehot_state[5]_i_4_n_0 ),
        .I4(\FSM_onehot_state[5]_i_5_n_0 ),
        .I5(\my_value_reg[31]_0 [9]),
        .O(\my_value[9]_i_1_n_0 ));
  FDCE \my_value_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[0]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[0] ));
  FDCE \my_value_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[10]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[10] ));
  FDCE \my_value_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[11]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[11] ));
  FDCE \my_value_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[12]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[12] ));
  FDCE \my_value_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[13]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[13] ));
  FDCE \my_value_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[14]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[14] ));
  FDCE \my_value_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[15]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[15] ));
  FDCE \my_value_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[16]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[16] ));
  FDCE \my_value_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[17]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[17] ));
  FDCE \my_value_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[18]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[18] ));
  FDCE \my_value_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[19]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[19] ));
  FDCE \my_value_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[1]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[1] ));
  FDCE \my_value_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[20]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[20] ));
  FDCE \my_value_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[21]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[21] ));
  FDCE \my_value_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[22]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[22] ));
  FDCE \my_value_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[23]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[23] ));
  FDCE \my_value_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[24]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[24] ));
  FDCE \my_value_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[25]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[25] ));
  FDCE \my_value_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[26]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[26] ));
  FDCE \my_value_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[27]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[27] ));
  FDCE \my_value_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[28]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[28] ));
  FDCE \my_value_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[29]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[29] ));
  FDCE \my_value_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[2]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[2] ));
  FDCE \my_value_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[30]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[30] ));
  FDCE \my_value_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[31]_i_2_n_0 ),
        .Q(\my_value_reg_n_0_[31] ));
  FDCE \my_value_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[3]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[3] ));
  FDCE \my_value_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[4]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[4] ));
  FDCE \my_value_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[5]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[5] ));
  FDCE \my_value_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[6]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[6] ));
  FDCE \my_value_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[7]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[7] ));
  FDCE \my_value_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[8]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[8] ));
  FDCE \my_value_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(\my_value[9]_i_1_n_0 ),
        .Q(\my_value_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[0]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[0]),
        .O(p_6_out0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[10]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[10]),
        .O(p_6_out0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[11]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[11]),
        .O(p_6_out0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[12]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[12]),
        .O(p_6_out0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[13]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[13]),
        .O(p_6_out0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[14]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[14]),
        .O(p_6_out0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[15]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[15]),
        .O(p_6_out0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[16]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[16]),
        .O(p_6_out0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[17]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[17]),
        .O(p_6_out0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[18]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[18]),
        .O(p_6_out0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[19]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[19]),
        .O(p_6_out0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[1]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[1]),
        .O(p_6_out0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[20]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[20]),
        .O(p_6_out0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[21]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[21]),
        .O(p_6_out0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[22]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[22]),
        .O(p_6_out0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[23]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[23]),
        .O(p_6_out0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[24]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[24]),
        .O(p_6_out0_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[25]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[25]),
        .O(p_6_out0_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[26]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[26]),
        .O(p_6_out0_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[27]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[27]),
        .O(p_6_out0_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[28]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[28]),
        .O(p_6_out0_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[29]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[29]),
        .O(p_6_out0_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[2]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[2]),
        .O(p_6_out0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[30]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[30]),
        .O(p_6_out0_in[30]));
  LUT4 #(
    .INIT(16'hEEFE)) 
    \others_value[31]_i_1 
       (.I0(DST_VALID_1),
        .I1(Q[0]),
        .I2(DST_FIN_1),
        .I3(\valid_reg_n_0_[0] ),
        .O(p_6_out[31]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[31]_i_2 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[31]),
        .O(p_6_out0_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[32]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[0]),
        .O(p_6_out0_in[32]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[33]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[1]),
        .O(p_6_out0_in[33]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[34]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[2]),
        .O(p_6_out0_in[34]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[35]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[3]),
        .O(p_6_out0_in[35]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[36]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[4]),
        .O(p_6_out0_in[36]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[37]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[5]),
        .O(p_6_out0_in[37]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[38]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[6]),
        .O(p_6_out0_in[38]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[39]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[7]),
        .O(p_6_out0_in[39]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[3]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[3]),
        .O(p_6_out0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[40]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[8]),
        .O(p_6_out0_in[40]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[41]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[9]),
        .O(p_6_out0_in[41]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[42]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[10]),
        .O(p_6_out0_in[42]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[43]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[11]),
        .O(p_6_out0_in[43]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[44]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[12]),
        .O(p_6_out0_in[44]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[45]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[13]),
        .O(p_6_out0_in[45]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[46]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[14]),
        .O(p_6_out0_in[46]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[47]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[15]),
        .O(p_6_out0_in[47]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[48]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[16]),
        .O(p_6_out0_in[48]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[49]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[17]),
        .O(p_6_out0_in[49]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[4]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[4]),
        .O(p_6_out0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[50]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[18]),
        .O(p_6_out0_in[50]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[51]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[19]),
        .O(p_6_out0_in[51]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[52]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[20]),
        .O(p_6_out0_in[52]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[53]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[21]),
        .O(p_6_out0_in[53]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[54]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[22]),
        .O(p_6_out0_in[54]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[55]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[23]),
        .O(p_6_out0_in[55]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[56]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[24]),
        .O(p_6_out0_in[56]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[57]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[25]),
        .O(p_6_out0_in[57]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[58]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[26]),
        .O(p_6_out0_in[58]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[59]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[27]),
        .O(p_6_out0_in[59]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[5]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[5]),
        .O(p_6_out0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[60]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[28]),
        .O(p_6_out0_in[60]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[61]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[29]),
        .O(p_6_out0_in[61]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[62]_i_1 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[30]),
        .O(p_6_out0_in[62]));
  LUT4 #(
    .INIT(16'hEEFE)) 
    \others_value[63]_i_1 
       (.I0(DST_VALID_2),
        .I1(Q[0]),
        .I2(DST_FIN_2),
        .I3(\valid_reg_n_0_[1] ),
        .O(p_6_out[63]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[63]_i_2 
       (.I0(DST_VALID_2),
        .I1(DST_DATA_2[31]),
        .O(p_6_out0_in[63]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[64]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[0]),
        .O(p_6_out0_in[64]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[65]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[1]),
        .O(p_6_out0_in[65]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[66]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[2]),
        .O(p_6_out0_in[66]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[67]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[3]),
        .O(p_6_out0_in[67]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[68]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[4]),
        .O(p_6_out0_in[68]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[69]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[5]),
        .O(p_6_out0_in[69]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[6]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[6]),
        .O(p_6_out0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[70]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[6]),
        .O(p_6_out0_in[70]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[71]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[7]),
        .O(p_6_out0_in[71]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[72]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[8]),
        .O(p_6_out0_in[72]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[73]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[9]),
        .O(p_6_out0_in[73]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[74]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[10]),
        .O(p_6_out0_in[74]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[75]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[11]),
        .O(p_6_out0_in[75]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[76]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[12]),
        .O(p_6_out0_in[76]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[77]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[13]),
        .O(p_6_out0_in[77]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[78]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[14]),
        .O(p_6_out0_in[78]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[79]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[15]),
        .O(p_6_out0_in[79]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[7]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[7]),
        .O(p_6_out0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[80]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[16]),
        .O(p_6_out0_in[80]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[81]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[17]),
        .O(p_6_out0_in[81]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[82]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[18]),
        .O(p_6_out0_in[82]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[83]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[19]),
        .O(p_6_out0_in[83]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[84]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[20]),
        .O(p_6_out0_in[84]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[85]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[21]),
        .O(p_6_out0_in[85]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[86]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[22]),
        .O(p_6_out0_in[86]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[87]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[23]),
        .O(p_6_out0_in[87]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[88]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[24]),
        .O(p_6_out0_in[88]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[89]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[25]),
        .O(p_6_out0_in[89]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[8]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[8]),
        .O(p_6_out0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[90]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[26]),
        .O(p_6_out0_in[90]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[91]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[27]),
        .O(p_6_out0_in[91]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[92]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[28]),
        .O(p_6_out0_in[92]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[93]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[29]),
        .O(p_6_out0_in[93]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[94]_i_1 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[30]),
        .O(p_6_out0_in[94]));
  LUT4 #(
    .INIT(16'hEEFE)) 
    \others_value[95]_i_1 
       (.I0(DST_VALID_3),
        .I1(Q[0]),
        .I2(DST_FIN_3),
        .I3(\valid_reg_n_0_[2] ),
        .O(p_6_out[95]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[95]_i_2 
       (.I0(DST_VALID_3),
        .I1(DST_DATA_3[31]),
        .O(p_6_out0_in[95]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[9]_i_1 
       (.I0(DST_VALID_1),
        .I1(DST_DATA_1[9]),
        .O(p_6_out0_in[9]));
  FDCE \others_value_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[0]),
        .Q(\others_value_reg_n_0_[0] ));
  FDCE \others_value_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[10]),
        .Q(\others_value_reg_n_0_[10] ));
  FDCE \others_value_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[11]),
        .Q(\others_value_reg_n_0_[11] ));
  FDCE \others_value_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[12]),
        .Q(\others_value_reg_n_0_[12] ));
  FDCE \others_value_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[13]),
        .Q(\others_value_reg_n_0_[13] ));
  FDCE \others_value_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[14]),
        .Q(\others_value_reg_n_0_[14] ));
  FDCE \others_value_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[15]),
        .Q(\others_value_reg_n_0_[15] ));
  FDCE \others_value_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[16]),
        .Q(\others_value_reg_n_0_[16] ));
  FDCE \others_value_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[17]),
        .Q(\others_value_reg_n_0_[17] ));
  FDCE \others_value_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[18]),
        .Q(\others_value_reg_n_0_[18] ));
  FDCE \others_value_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[19]),
        .Q(\others_value_reg_n_0_[19] ));
  FDCE \others_value_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[1]),
        .Q(\others_value_reg_n_0_[1] ));
  FDCE \others_value_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[20]),
        .Q(\others_value_reg_n_0_[20] ));
  FDCE \others_value_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[21]),
        .Q(\others_value_reg_n_0_[21] ));
  FDCE \others_value_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[22]),
        .Q(\others_value_reg_n_0_[22] ));
  FDCE \others_value_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[23]),
        .Q(\others_value_reg_n_0_[23] ));
  FDCE \others_value_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[24]),
        .Q(\others_value_reg_n_0_[24] ));
  FDCE \others_value_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[25]),
        .Q(\others_value_reg_n_0_[25] ));
  FDCE \others_value_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[26]),
        .Q(\others_value_reg_n_0_[26] ));
  FDCE \others_value_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[27]),
        .Q(\others_value_reg_n_0_[27] ));
  FDCE \others_value_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[28]),
        .Q(\others_value_reg_n_0_[28] ));
  FDCE \others_value_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[29]),
        .Q(\others_value_reg_n_0_[29] ));
  FDCE \others_value_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[2]),
        .Q(\others_value_reg_n_0_[2] ));
  FDCE \others_value_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[30]),
        .Q(\others_value_reg_n_0_[30] ));
  FDCE \others_value_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[31]),
        .Q(\others_value_reg_n_0_[31] ));
  FDCE \others_value_reg[32] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[32]),
        .Q(p_0_in[0]));
  FDCE \others_value_reg[33] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[33]),
        .Q(p_0_in[1]));
  FDCE \others_value_reg[34] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[34]),
        .Q(p_0_in[2]));
  FDCE \others_value_reg[35] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[35]),
        .Q(p_0_in[3]));
  FDCE \others_value_reg[36] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[36]),
        .Q(p_0_in[4]));
  FDCE \others_value_reg[37] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[37]),
        .Q(p_0_in[5]));
  FDCE \others_value_reg[38] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[38]),
        .Q(p_0_in[6]));
  FDCE \others_value_reg[39] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[39]),
        .Q(p_0_in[7]));
  FDCE \others_value_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[3]),
        .Q(\others_value_reg_n_0_[3] ));
  FDCE \others_value_reg[40] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[40]),
        .Q(p_0_in[8]));
  FDCE \others_value_reg[41] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[41]),
        .Q(p_0_in[9]));
  FDCE \others_value_reg[42] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[42]),
        .Q(p_0_in[10]));
  FDCE \others_value_reg[43] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[43]),
        .Q(p_0_in[11]));
  FDCE \others_value_reg[44] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[44]),
        .Q(p_0_in[12]));
  FDCE \others_value_reg[45] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[45]),
        .Q(p_0_in[13]));
  FDCE \others_value_reg[46] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[46]),
        .Q(p_0_in[14]));
  FDCE \others_value_reg[47] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[47]),
        .Q(p_0_in[15]));
  FDCE \others_value_reg[48] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[48]),
        .Q(p_0_in[16]));
  FDCE \others_value_reg[49] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[49]),
        .Q(p_0_in[17]));
  FDCE \others_value_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[4]),
        .Q(\others_value_reg_n_0_[4] ));
  FDCE \others_value_reg[50] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[50]),
        .Q(p_0_in[18]));
  FDCE \others_value_reg[51] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[51]),
        .Q(p_0_in[19]));
  FDCE \others_value_reg[52] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[52]),
        .Q(p_0_in[20]));
  FDCE \others_value_reg[53] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[53]),
        .Q(p_0_in[21]));
  FDCE \others_value_reg[54] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[54]),
        .Q(p_0_in[22]));
  FDCE \others_value_reg[55] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[55]),
        .Q(p_0_in[23]));
  FDCE \others_value_reg[56] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[56]),
        .Q(p_0_in[24]));
  FDCE \others_value_reg[57] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[57]),
        .Q(p_0_in[25]));
  FDCE \others_value_reg[58] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[58]),
        .Q(p_0_in[26]));
  FDCE \others_value_reg[59] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[59]),
        .Q(p_0_in[27]));
  FDCE \others_value_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[5]),
        .Q(\others_value_reg_n_0_[5] ));
  FDCE \others_value_reg[60] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[60]),
        .Q(p_0_in[28]));
  FDCE \others_value_reg[61] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[61]),
        .Q(p_0_in[29]));
  FDCE \others_value_reg[62] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[62]),
        .Q(p_0_in[30]));
  FDCE \others_value_reg[63] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[63]),
        .CLR(SR),
        .D(p_6_out0_in[63]),
        .Q(p_0_in[31]));
  FDCE \others_value_reg[64] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[64]),
        .Q(p_0_in0_in[0]));
  FDCE \others_value_reg[65] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[65]),
        .Q(p_0_in0_in[1]));
  FDCE \others_value_reg[66] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[66]),
        .Q(p_0_in0_in[2]));
  FDCE \others_value_reg[67] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[67]),
        .Q(p_0_in0_in[3]));
  FDCE \others_value_reg[68] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[68]),
        .Q(p_0_in0_in[4]));
  FDCE \others_value_reg[69] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[69]),
        .Q(p_0_in0_in[5]));
  FDCE \others_value_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[6]),
        .Q(\others_value_reg_n_0_[6] ));
  FDCE \others_value_reg[70] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[70]),
        .Q(p_0_in0_in[6]));
  FDCE \others_value_reg[71] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[71]),
        .Q(p_0_in0_in[7]));
  FDCE \others_value_reg[72] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[72]),
        .Q(p_0_in0_in[8]));
  FDCE \others_value_reg[73] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[73]),
        .Q(p_0_in0_in[9]));
  FDCE \others_value_reg[74] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[74]),
        .Q(p_0_in0_in[10]));
  FDCE \others_value_reg[75] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[75]),
        .Q(p_0_in0_in[11]));
  FDCE \others_value_reg[76] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[76]),
        .Q(p_0_in0_in[12]));
  FDCE \others_value_reg[77] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[77]),
        .Q(p_0_in0_in[13]));
  FDCE \others_value_reg[78] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[78]),
        .Q(p_0_in0_in[14]));
  FDCE \others_value_reg[79] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[79]),
        .Q(p_0_in0_in[15]));
  FDCE \others_value_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[7]),
        .Q(\others_value_reg_n_0_[7] ));
  FDCE \others_value_reg[80] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[80]),
        .Q(p_0_in0_in[16]));
  FDCE \others_value_reg[81] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[81]),
        .Q(p_0_in0_in[17]));
  FDCE \others_value_reg[82] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[82]),
        .Q(p_0_in0_in[18]));
  FDCE \others_value_reg[83] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[83]),
        .Q(p_0_in0_in[19]));
  FDCE \others_value_reg[84] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[84]),
        .Q(p_0_in0_in[20]));
  FDCE \others_value_reg[85] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[85]),
        .Q(p_0_in0_in[21]));
  FDCE \others_value_reg[86] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[86]),
        .Q(p_0_in0_in[22]));
  FDCE \others_value_reg[87] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[87]),
        .Q(p_0_in0_in[23]));
  FDCE \others_value_reg[88] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[88]),
        .Q(p_0_in0_in[24]));
  FDCE \others_value_reg[89] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[89]),
        .Q(p_0_in0_in[25]));
  FDCE \others_value_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[8]),
        .Q(\others_value_reg_n_0_[8] ));
  FDCE \others_value_reg[90] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[90]),
        .Q(p_0_in0_in[26]));
  FDCE \others_value_reg[91] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[91]),
        .Q(p_0_in0_in[27]));
  FDCE \others_value_reg[92] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[92]),
        .Q(p_0_in0_in[28]));
  FDCE \others_value_reg[93] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[93]),
        .Q(p_0_in0_in[29]));
  FDCE \others_value_reg[94] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[94]),
        .Q(p_0_in0_in[30]));
  FDCE \others_value_reg[95] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[95]),
        .CLR(SR),
        .D(p_6_out0_in[95]),
        .Q(p_0_in0_in[31]));
  FDCE \others_value_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_6_out[31]),
        .CLR(SR),
        .D(p_6_out0_in[9]),
        .Q(\others_value_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \valid[0]_i_1 
       (.I0(DST_VALID_1),
        .I1(Q[0]),
        .I2(\valid_reg_n_0_[0] ),
        .O(\valid[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \valid[1]_i_1 
       (.I0(DST_VALID_2),
        .I1(Q[0]),
        .I2(\valid_reg_n_0_[1] ),
        .O(\valid[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \valid[2]_i_1 
       (.I0(DST_VALID_3),
        .I1(Q[0]),
        .I2(\valid_reg_n_0_[2] ),
        .O(\valid[2]_i_1_n_0 ));
  FDCE \valid_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\valid[0]_i_1_n_0 ),
        .Q(\valid_reg_n_0_[0] ));
  FDCE \valid_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\valid[1]_i_1_n_0 ),
        .Q(\valid_reg_n_0_[1] ));
  FDCE \valid_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(\valid[2]_i_1_n_0 ),
        .Q(\valid_reg_n_0_[2] ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
