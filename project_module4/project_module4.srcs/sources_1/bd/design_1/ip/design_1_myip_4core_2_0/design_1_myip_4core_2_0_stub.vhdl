-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Tue Jan 21 16:47:33 2020
-- Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_myip_4core_2_0 -prefix
--               design_1_myip_4core_2_0_ design_1_myip_4core_0_1_stub.vhdl
-- Design      : design_1_myip_4core_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu9p-fsgd2104-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_myip_4core_2_0 is
  Port ( 
    DST_VALID_1 : in STD_LOGIC;
    DST_DATA_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_1 : out STD_LOGIC;
    DST_FIN_1 : in STD_LOGIC;
    SRC_VALID_1 : out STD_LOGIC;
    SRC_DATA_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_ACK_1 : in STD_LOGIC;
    SRC_FIN_1 : out STD_LOGIC;
    DST_VALID_2 : in STD_LOGIC;
    DST_DATA_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_2 : out STD_LOGIC;
    DST_FIN_2 : in STD_LOGIC;
    SRC_VALID_2 : out STD_LOGIC;
    SRC_DATA_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_ACK_2 : in STD_LOGIC;
    SRC_FIN_2 : out STD_LOGIC;
    DST_VALID_3 : in STD_LOGIC;
    DST_DATA_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    DST_ACK_3 : out STD_LOGIC;
    DST_FIN_3 : in STD_LOGIC;
    SRC_VALID_3 : out STD_LOGIC;
    SRC_DATA_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRC_ACK_3 : in STD_LOGIC;
    SRC_FIN_3 : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );

end design_1_myip_4core_2_0;

architecture stub of design_1_myip_4core_2_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "DST_VALID_1,DST_DATA_1[31:0],DST_ACK_1,DST_FIN_1,SRC_VALID_1,SRC_DATA_1[31:0],SRC_ACK_1,SRC_FIN_1,DST_VALID_2,DST_DATA_2[31:0],DST_ACK_2,DST_FIN_2,SRC_VALID_2,SRC_DATA_2[31:0],SRC_ACK_2,SRC_FIN_2,DST_VALID_3,DST_DATA_3[31:0],DST_ACK_3,DST_FIN_3,SRC_VALID_3,SRC_DATA_3[31:0],SRC_ACK_3,SRC_FIN_3,s00_axi_awaddr[4:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[4:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,s00_axi_aclk,s00_axi_aresetn";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "myip_4core_v1_0,Vivado 2018.3";
begin
end;
