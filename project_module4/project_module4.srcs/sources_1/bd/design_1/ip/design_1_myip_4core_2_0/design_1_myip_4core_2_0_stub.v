// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Tue Jan 21 16:47:33 2020
// Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top design_1_myip_4core_2_0 -prefix
//               design_1_myip_4core_2_0_ design_1_myip_4core_0_1_stub.v
// Design      : design_1_myip_4core_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "myip_4core_v1_0,Vivado 2018.3" *)
module design_1_myip_4core_2_0(DST_VALID_1, DST_DATA_1, DST_ACK_1, DST_FIN_1, 
  SRC_VALID_1, SRC_DATA_1, SRC_ACK_1, SRC_FIN_1, DST_VALID_2, DST_DATA_2, DST_ACK_2, DST_FIN_2, 
  SRC_VALID_2, SRC_DATA_2, SRC_ACK_2, SRC_FIN_2, DST_VALID_3, DST_DATA_3, DST_ACK_3, DST_FIN_3, 
  SRC_VALID_3, SRC_DATA_3, SRC_ACK_3, SRC_FIN_3, s00_axi_awaddr, s00_axi_awprot, 
  s00_axi_awvalid, s00_axi_awready, s00_axi_wdata, s00_axi_wstrb, s00_axi_wvalid, 
  s00_axi_wready, s00_axi_bresp, s00_axi_bvalid, s00_axi_bready, s00_axi_araddr, 
  s00_axi_arprot, s00_axi_arvalid, s00_axi_arready, s00_axi_rdata, s00_axi_rresp, 
  s00_axi_rvalid, s00_axi_rready, s00_axi_aclk, s00_axi_aresetn)
/* synthesis syn_black_box black_box_pad_pin="DST_VALID_1,DST_DATA_1[31:0],DST_ACK_1,DST_FIN_1,SRC_VALID_1,SRC_DATA_1[31:0],SRC_ACK_1,SRC_FIN_1,DST_VALID_2,DST_DATA_2[31:0],DST_ACK_2,DST_FIN_2,SRC_VALID_2,SRC_DATA_2[31:0],SRC_ACK_2,SRC_FIN_2,DST_VALID_3,DST_DATA_3[31:0],DST_ACK_3,DST_FIN_3,SRC_VALID_3,SRC_DATA_3[31:0],SRC_ACK_3,SRC_FIN_3,s00_axi_awaddr[4:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[4:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,s00_axi_aclk,s00_axi_aresetn" */;
  input DST_VALID_1;
  input [31:0]DST_DATA_1;
  output DST_ACK_1;
  input DST_FIN_1;
  output SRC_VALID_1;
  output [31:0]SRC_DATA_1;
  input SRC_ACK_1;
  output SRC_FIN_1;
  input DST_VALID_2;
  input [31:0]DST_DATA_2;
  output DST_ACK_2;
  input DST_FIN_2;
  output SRC_VALID_2;
  output [31:0]SRC_DATA_2;
  input SRC_ACK_2;
  output SRC_FIN_2;
  input DST_VALID_3;
  input [31:0]DST_DATA_3;
  output DST_ACK_3;
  input DST_FIN_3;
  output SRC_VALID_3;
  output [31:0]SRC_DATA_3;
  input SRC_ACK_3;
  output SRC_FIN_3;
  input [4:0]s00_axi_awaddr;
  input [2:0]s00_axi_awprot;
  input s00_axi_awvalid;
  output s00_axi_awready;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  output s00_axi_wready;
  output [1:0]s00_axi_bresp;
  output s00_axi_bvalid;
  input s00_axi_bready;
  input [4:0]s00_axi_araddr;
  input [2:0]s00_axi_arprot;
  input s00_axi_arvalid;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output [1:0]s00_axi_rresp;
  output s00_axi_rvalid;
  input s00_axi_rready;
  input s00_axi_aclk;
  input s00_axi_aresetn;
endmodule
