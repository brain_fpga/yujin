/*
 * test.c
 *
 *  Created on: 2020. 1. 20.
 *      Author: HPCS
 */

#include "myip_4core.h"
#include "xparameters.h"
#include "xuartns550.h"
#include "xil_io.h"

#define BASE_ADDR_0 0x44A10000
#define BASE_ADDR_1 0x44A20000
#define BASE_ADDR_2 0x44A30000
#define BASE_ADDR_3 0x44A40000

int main(void){

	u32 lock0 = 0;
	u32 lock1 = 0;
	u32 lock2 = 0;
	u32 lock3 = 0;
	u32 result0 = 0;
	u32 result1 = 0;
	u32 result2 = 0;
	u32 result3 = 0;

	// input values
	MYIP_4CORE_mWriteReg(BASE_ADDR_0, MYIP_4CORE_S00_AXI_SLV_REG1_OFFSET, 2);
	MYIP_4CORE_mWriteReg(BASE_ADDR_1, MYIP_4CORE_S00_AXI_SLV_REG1_OFFSET, 5);
	MYIP_4CORE_mWriteReg(BASE_ADDR_2, MYIP_4CORE_S00_AXI_SLV_REG1_OFFSET, 3);
	MYIP_4CORE_mWriteReg(BASE_ADDR_3, MYIP_4CORE_S00_AXI_SLV_REG1_OFFSET, 4294967295);

	// start signal
	MYIP_4CORE_mWriteReg(BASE_ADDR_2, MYIP_4CORE_S00_AXI_SLV_REG0_OFFSET, 1);
	MYIP_4CORE_mWriteReg(BASE_ADDR_3, MYIP_4CORE_S00_AXI_SLV_REG0_OFFSET, 1);
	MYIP_4CORE_mWriteReg(BASE_ADDR_0, MYIP_4CORE_S00_AXI_SLV_REG0_OFFSET, 1);
	MYIP_4CORE_mWriteReg(BASE_ADDR_1, MYIP_4CORE_S00_AXI_SLV_REG0_OFFSET, 1);

	while(lock0 == 0){
		lock0 = MYIP_4CORE_mReadReg(BASE_ADDR_0, MYIP_4CORE_S00_AXI_SLV_REG3_OFFSET);
	}
	result0 = MYIP_4CORE_mReadReg(BASE_ADDR_0, MYIP_4CORE_S00_AXI_SLV_REG2_OFFSET);

	while(lock1 == 0){
		lock1 = MYIP_4CORE_mReadReg(BASE_ADDR_1, MYIP_4CORE_S00_AXI_SLV_REG3_OFFSET);
	}
	result1 = MYIP_4CORE_mReadReg(BASE_ADDR_1, MYIP_4CORE_S00_AXI_SLV_REG2_OFFSET);

	while(lock2 == 0){
		lock2 = MYIP_4CORE_mReadReg(BASE_ADDR_2, MYIP_4CORE_S00_AXI_SLV_REG3_OFFSET);
	}
	result2 = MYIP_4CORE_mReadReg(BASE_ADDR_2, MYIP_4CORE_S00_AXI_SLV_REG2_OFFSET);

	while(lock3 == 0){
		lock3 = MYIP_4CORE_mReadReg(BASE_ADDR_3, MYIP_4CORE_S00_AXI_SLV_REG3_OFFSET);
	}
	result3 = MYIP_4CORE_mReadReg(BASE_ADDR_3, MYIP_4CORE_S00_AXI_SLV_REG2_OFFSET);

	xil_printf("Result of core 0 : %d\r\n", result0);
	xil_printf("Result of core 1 : %d\r\n", result1);
	xil_printf("Result of core 2 : %d\r\n", result2);
	xil_printf("Result of core 3 : %d\r\n", result3);

	return 0;

}
