-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Mon Jan 20 13:58:10 2020
-- Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_myip_for2_1_0_sim_netlist.vhdl
-- Design      : design_1_myip_for2_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu9p-fsgd2104-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    dst_ack : out STD_LOGIC;
    src_init : out STD_LOGIC;
    src_fin : out STD_LOGIC;
    src_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    src_ack : in STD_LOGIC;
    dst_fin : in STD_LOGIC;
    dst_init : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    dst_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \my_value_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample is
  signal DST_ACK_i_1_n_0 : STD_LOGIC;
  signal \FSM_sequential_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal SRC_DATA0_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \SRC_DATA[31]_i_1_n_0\ : STD_LOGIC;
  signal SRC_FIN_i_1_n_0 : STD_LOGIC;
  signal SRC_INIT_i_1_n_0 : STD_LOGIC;
  signal ack_i_1_n_0 : STD_LOGIC;
  signal ack_reg_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal done : STD_LOGIC;
  signal done_i_1_n_0 : STD_LOGIC;
  signal dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \dout0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_n_0\ : STD_LOGIC;
  signal \dout0_carry__0_n_1\ : STD_LOGIC;
  signal \dout0_carry__0_n_2\ : STD_LOGIC;
  signal \dout0_carry__0_n_3\ : STD_LOGIC;
  signal \dout0_carry__0_n_4\ : STD_LOGIC;
  signal \dout0_carry__0_n_5\ : STD_LOGIC;
  signal \dout0_carry__0_n_6\ : STD_LOGIC;
  signal \dout0_carry__0_n_7\ : STD_LOGIC;
  signal \dout0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_n_0\ : STD_LOGIC;
  signal \dout0_carry__1_n_1\ : STD_LOGIC;
  signal \dout0_carry__1_n_2\ : STD_LOGIC;
  signal \dout0_carry__1_n_3\ : STD_LOGIC;
  signal \dout0_carry__1_n_4\ : STD_LOGIC;
  signal \dout0_carry__1_n_5\ : STD_LOGIC;
  signal \dout0_carry__1_n_6\ : STD_LOGIC;
  signal \dout0_carry__1_n_7\ : STD_LOGIC;
  signal \dout0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \dout0_carry__2_n_1\ : STD_LOGIC;
  signal \dout0_carry__2_n_2\ : STD_LOGIC;
  signal \dout0_carry__2_n_3\ : STD_LOGIC;
  signal \dout0_carry__2_n_4\ : STD_LOGIC;
  signal \dout0_carry__2_n_5\ : STD_LOGIC;
  signal \dout0_carry__2_n_6\ : STD_LOGIC;
  signal \dout0_carry__2_n_7\ : STD_LOGIC;
  signal dout0_carry_i_1_n_0 : STD_LOGIC;
  signal dout0_carry_i_2_n_0 : STD_LOGIC;
  signal dout0_carry_i_3_n_0 : STD_LOGIC;
  signal dout0_carry_i_4_n_0 : STD_LOGIC;
  signal dout0_carry_i_5_n_0 : STD_LOGIC;
  signal dout0_carry_i_6_n_0 : STD_LOGIC;
  signal dout0_carry_i_7_n_0 : STD_LOGIC;
  signal dout0_carry_i_8_n_0 : STD_LOGIC;
  signal dout0_carry_n_0 : STD_LOGIC;
  signal dout0_carry_n_1 : STD_LOGIC;
  signal dout0_carry_n_2 : STD_LOGIC;
  signal dout0_carry_n_3 : STD_LOGIC;
  signal dout0_carry_n_4 : STD_LOGIC;
  signal dout0_carry_n_5 : STD_LOGIC;
  signal dout0_carry_n_6 : STD_LOGIC;
  signal dout0_carry_n_7 : STD_LOGIC;
  signal dout0_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \dout[31]_i_1_n_0\ : STD_LOGIC;
  signal \^dst_ack\ : STD_LOGIC;
  signal fin : STD_LOGIC;
  signal fin_i_1_n_0 : STD_LOGIC;
  signal in6 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal my_value : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \my_value[31]_i_1_n_0\ : STD_LOGIC;
  signal \my_value_reg_n_0_[0]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[10]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[11]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[12]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[13]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[14]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[15]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[16]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[17]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[18]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[19]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[1]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[20]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[21]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[22]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[23]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[24]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[25]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[26]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[27]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[28]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[29]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[2]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[30]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[31]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[3]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[4]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[5]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[6]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[7]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[8]\ : STD_LOGIC;
  signal \my_value_reg_n_0_[9]\ : STD_LOGIC;
  signal others_value : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^src_fin\ : STD_LOGIC;
  signal \^src_init\ : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_dout0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_state[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_sequential_state[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_sequential_state[2]_i_2\ : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[0]\ : label is "STATE_IDLE:000,STATE_STORE:001,STATE_SEND:010,STATE_ACK_WAIT:011,STATE_FIN:100,STATE_CAL:101,STATE_DONE:110,";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[1]\ : label is "STATE_IDLE:000,STATE_STORE:001,STATE_SEND:010,STATE_ACK_WAIT:011,STATE_FIN:100,STATE_CAL:101,STATE_DONE:110,";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[2]\ : label is "STATE_IDLE:000,STATE_STORE:001,STATE_SEND:010,STATE_ACK_WAIT:011,STATE_FIN:100,STATE_CAL:101,STATE_DONE:110,";
  attribute SOFT_HLUTNM of \SRC_DATA[0]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \SRC_DATA[10]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \SRC_DATA[11]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \SRC_DATA[12]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \SRC_DATA[13]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \SRC_DATA[14]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \SRC_DATA[15]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \SRC_DATA[16]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \SRC_DATA[17]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \SRC_DATA[18]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \SRC_DATA[19]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \SRC_DATA[1]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \SRC_DATA[20]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \SRC_DATA[21]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \SRC_DATA[22]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \SRC_DATA[23]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \SRC_DATA[24]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \SRC_DATA[25]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \SRC_DATA[26]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \SRC_DATA[27]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \SRC_DATA[28]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \SRC_DATA[29]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \SRC_DATA[2]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \SRC_DATA[30]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \SRC_DATA[31]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \SRC_DATA[3]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \SRC_DATA[4]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \SRC_DATA[5]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \SRC_DATA[6]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \SRC_DATA[7]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \SRC_DATA[8]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \SRC_DATA[9]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of SRC_FIN_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of SRC_INIT_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_4\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[1]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \axi_rdata[2]_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \dout[0]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \dout[10]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \dout[11]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \dout[12]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \dout[13]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \dout[14]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \dout[15]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \dout[16]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \dout[17]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \dout[18]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \dout[19]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \dout[1]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \dout[20]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \dout[21]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \dout[22]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \dout[23]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \dout[24]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \dout[25]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \dout[26]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \dout[27]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \dout[28]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \dout[29]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \dout[2]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \dout[30]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \dout[31]_i_2\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \dout[3]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \dout[4]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \dout[5]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \dout[6]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \dout[7]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \dout[8]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \dout[9]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \my_value[0]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \my_value[10]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \my_value[11]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \my_value[12]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \my_value[13]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \my_value[14]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \my_value[15]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \my_value[16]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \my_value[17]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \my_value[18]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \my_value[19]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \my_value[1]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \my_value[20]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \my_value[21]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \my_value[22]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \my_value[23]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \my_value[24]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \my_value[25]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \my_value[26]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \my_value[27]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \my_value[28]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \my_value[29]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \my_value[2]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \my_value[30]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \my_value[31]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \my_value[3]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \my_value[4]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \my_value[5]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \my_value[6]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \my_value[7]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \my_value[8]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \my_value[9]_i_1\ : label is "soft_lutpair16";
begin
  SR(0) <= \^sr\(0);
  dst_ack <= \^dst_ack\;
  src_fin <= \^src_fin\;
  src_init <= \^src_init\;
DST_ACK_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEAAAA"
    )
        port map (
      I0 => dst_init,
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      I4 => \^dst_ack\,
      O => DST_ACK_i_1_n_0
    );
DST_ACK_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \^sr\(0)
    );
DST_ACK_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \^sr\(0),
      D => DST_ACK_i_1_n_0,
      Q => \^dst_ack\
    );
\FSM_sequential_state[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => state(0),
      O => \FSM_sequential_state[0]_i_1_n_0\
    );
\FSM_sequential_state[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      O => \FSM_sequential_state[1]_i_1_n_0\
    );
\FSM_sequential_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3F3F0F0F3B383B38"
    )
        port map (
      I0 => fin,
      I1 => state(2),
      I2 => state(1),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => ack_reg_n_0,
      I5 => state(0),
      O => \FSM_sequential_state[2]_i_1_n_0\
    );
\FSM_sequential_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"38"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(2),
      O => \FSM_sequential_state[2]_i_2_n_0\
    );
\FSM_sequential_state_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \FSM_sequential_state[2]_i_1_n_0\,
      CLR => \^sr\(0),
      D => \FSM_sequential_state[0]_i_1_n_0\,
      Q => state(0)
    );
\FSM_sequential_state_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \FSM_sequential_state[2]_i_1_n_0\,
      CLR => \^sr\(0),
      D => \FSM_sequential_state[1]_i_1_n_0\,
      Q => state(1)
    );
\FSM_sequential_state_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \FSM_sequential_state[2]_i_1_n_0\,
      CLR => \^sr\(0),
      D => \FSM_sequential_state[2]_i_2_n_0\,
      Q => state(2)
    );
\SRC_DATA[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[0]\,
      I1 => state(1),
      O => SRC_DATA0_in(0)
    );
\SRC_DATA[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[10]\,
      I1 => state(1),
      O => SRC_DATA0_in(10)
    );
\SRC_DATA[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[11]\,
      I1 => state(1),
      O => SRC_DATA0_in(11)
    );
\SRC_DATA[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[12]\,
      I1 => state(1),
      O => SRC_DATA0_in(12)
    );
\SRC_DATA[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[13]\,
      I1 => state(1),
      O => SRC_DATA0_in(13)
    );
\SRC_DATA[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[14]\,
      I1 => state(1),
      O => SRC_DATA0_in(14)
    );
\SRC_DATA[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[15]\,
      I1 => state(1),
      O => SRC_DATA0_in(15)
    );
\SRC_DATA[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[16]\,
      I1 => state(1),
      O => SRC_DATA0_in(16)
    );
\SRC_DATA[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[17]\,
      I1 => state(1),
      O => SRC_DATA0_in(17)
    );
\SRC_DATA[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[18]\,
      I1 => state(1),
      O => SRC_DATA0_in(18)
    );
\SRC_DATA[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[19]\,
      I1 => state(1),
      O => SRC_DATA0_in(19)
    );
\SRC_DATA[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[1]\,
      I1 => state(1),
      O => SRC_DATA0_in(1)
    );
\SRC_DATA[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[20]\,
      I1 => state(1),
      O => SRC_DATA0_in(20)
    );
\SRC_DATA[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[21]\,
      I1 => state(1),
      O => SRC_DATA0_in(21)
    );
\SRC_DATA[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[22]\,
      I1 => state(1),
      O => SRC_DATA0_in(22)
    );
\SRC_DATA[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[23]\,
      I1 => state(1),
      O => SRC_DATA0_in(23)
    );
\SRC_DATA[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[24]\,
      I1 => state(1),
      O => SRC_DATA0_in(24)
    );
\SRC_DATA[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[25]\,
      I1 => state(1),
      O => SRC_DATA0_in(25)
    );
\SRC_DATA[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[26]\,
      I1 => state(1),
      O => SRC_DATA0_in(26)
    );
\SRC_DATA[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[27]\,
      I1 => state(1),
      O => SRC_DATA0_in(27)
    );
\SRC_DATA[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[28]\,
      I1 => state(1),
      O => SRC_DATA0_in(28)
    );
\SRC_DATA[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[29]\,
      I1 => state(1),
      O => SRC_DATA0_in(29)
    );
\SRC_DATA[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[2]\,
      I1 => state(1),
      O => SRC_DATA0_in(2)
    );
\SRC_DATA[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[30]\,
      I1 => state(1),
      O => SRC_DATA0_in(30)
    );
\SRC_DATA[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => state(0),
      I1 => state(2),
      O => \SRC_DATA[31]_i_1_n_0\
    );
\SRC_DATA[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[31]\,
      I1 => state(1),
      O => SRC_DATA0_in(31)
    );
\SRC_DATA[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[3]\,
      I1 => state(1),
      O => SRC_DATA0_in(3)
    );
\SRC_DATA[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[4]\,
      I1 => state(1),
      O => SRC_DATA0_in(4)
    );
\SRC_DATA[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[5]\,
      I1 => state(1),
      O => SRC_DATA0_in(5)
    );
\SRC_DATA[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[6]\,
      I1 => state(1),
      O => SRC_DATA0_in(6)
    );
\SRC_DATA[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[7]\,
      I1 => state(1),
      O => SRC_DATA0_in(7)
    );
\SRC_DATA[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[8]\,
      I1 => state(1),
      O => SRC_DATA0_in(8)
    );
\SRC_DATA[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg_n_0_[9]\,
      I1 => state(1),
      O => SRC_DATA0_in(9)
    );
\SRC_DATA_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(0),
      Q => src_data(0)
    );
\SRC_DATA_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(10),
      Q => src_data(10)
    );
\SRC_DATA_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(11),
      Q => src_data(11)
    );
\SRC_DATA_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(12),
      Q => src_data(12)
    );
\SRC_DATA_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(13),
      Q => src_data(13)
    );
\SRC_DATA_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(14),
      Q => src_data(14)
    );
\SRC_DATA_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(15),
      Q => src_data(15)
    );
\SRC_DATA_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(16),
      Q => src_data(16)
    );
\SRC_DATA_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(17),
      Q => src_data(17)
    );
\SRC_DATA_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(18),
      Q => src_data(18)
    );
\SRC_DATA_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(19),
      Q => src_data(19)
    );
\SRC_DATA_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(1),
      Q => src_data(1)
    );
\SRC_DATA_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(20),
      Q => src_data(20)
    );
\SRC_DATA_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(21),
      Q => src_data(21)
    );
\SRC_DATA_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(22),
      Q => src_data(22)
    );
\SRC_DATA_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(23),
      Q => src_data(23)
    );
\SRC_DATA_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(24),
      Q => src_data(24)
    );
\SRC_DATA_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(25),
      Q => src_data(25)
    );
\SRC_DATA_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(26),
      Q => src_data(26)
    );
\SRC_DATA_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(27),
      Q => src_data(27)
    );
\SRC_DATA_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(28),
      Q => src_data(28)
    );
\SRC_DATA_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(29),
      Q => src_data(29)
    );
\SRC_DATA_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(2),
      Q => src_data(2)
    );
\SRC_DATA_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(30),
      Q => src_data(30)
    );
\SRC_DATA_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(31),
      Q => src_data(31)
    );
\SRC_DATA_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(3),
      Q => src_data(3)
    );
\SRC_DATA_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(4),
      Q => src_data(4)
    );
\SRC_DATA_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(5),
      Q => src_data(5)
    );
\SRC_DATA_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(6),
      Q => src_data(6)
    );
\SRC_DATA_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(7),
      Q => src_data(7)
    );
\SRC_DATA_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(8),
      Q => src_data(8)
    );
\SRC_DATA_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \SRC_DATA[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => SRC_DATA0_in(9),
      Q => src_data(9)
    );
SRC_FIN_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => state(2),
      I1 => state(0),
      I2 => state(1),
      I3 => \^src_fin\,
      O => SRC_FIN_i_1_n_0
    );
SRC_FIN_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \^sr\(0),
      D => SRC_FIN_i_1_n_0,
      Q => \^src_fin\
    );
SRC_INIT_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BE04"
    )
        port map (
      I0 => state(2),
      I1 => state(1),
      I2 => state(0),
      I3 => \^src_init\,
      O => SRC_INIT_i_1_n_0
    );
SRC_INIT_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \^sr\(0),
      D => SRC_INIT_i_1_n_0,
      Q => \^src_init\
    );
ack_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFE4000"
    )
        port map (
      I0 => state(2),
      I1 => state(1),
      I2 => state(0),
      I3 => src_ack,
      I4 => ack_reg_n_0,
      O => ack_i_1_n_0
    );
ack_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \^sr\(0),
      D => ack_i_1_n_0,
      Q => ack_reg_n_0
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => done,
      I1 => dout(0),
      I2 => Q(1),
      I3 => \my_value_reg[31]_0\(0),
      I4 => Q(0),
      I5 => \axi_rdata_reg[31]_0\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[0]_i_4_n_0\,
      I1 => Q(1),
      I2 => others_value(0),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[0]\,
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88B8B8B8"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(0),
      I1 => Q(0),
      I2 => state(0),
      I3 => state(2),
      I4 => state(1),
      O => \axi_rdata[0]_i_4_n_0\
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(10),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(10),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(10),
      I1 => Q(1),
      I2 => others_value(10),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[10]\,
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(11),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(11),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(11),
      I1 => Q(1),
      I2 => others_value(11),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[11]\,
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(12),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(12),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(12),
      I1 => Q(1),
      I2 => others_value(12),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[12]\,
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(13),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(13),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(13),
      I1 => Q(1),
      I2 => others_value(13),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[13]\,
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(14),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(14),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(14),
      I1 => Q(1),
      I2 => others_value(14),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[14]\,
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(15),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(15),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(15),
      I1 => Q(1),
      I2 => others_value(15),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[15]\,
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(16),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(16),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(16),
      I1 => Q(1),
      I2 => others_value(16),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[16]\,
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(17),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(17),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(17),
      I1 => Q(1),
      I2 => others_value(17),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[17]\,
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(18),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(18),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(18),
      I1 => Q(1),
      I2 => others_value(18),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[18]\,
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(19),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(19),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(19),
      I1 => Q(1),
      I2 => others_value(19),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[19]\,
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(1),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(1),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[1]_i_4_n_0\,
      I1 => Q(1),
      I2 => others_value(1),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[1]\,
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88B8B8B8"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(1),
      I1 => Q(0),
      I2 => state(1),
      I3 => state(2),
      I4 => state(0),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(20),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(20),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(20),
      I1 => Q(1),
      I2 => others_value(20),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[20]\,
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(21),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(21),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(21),
      I1 => Q(1),
      I2 => others_value(21),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[21]\,
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(22),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(22),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(22),
      I1 => Q(1),
      I2 => others_value(22),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[22]\,
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(23),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(23),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(23),
      I1 => Q(1),
      I2 => others_value(23),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[23]\,
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(24),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(24),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(24),
      I1 => Q(1),
      I2 => others_value(24),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[24]\,
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(25),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(25),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(25),
      I1 => Q(1),
      I2 => others_value(25),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[25]\,
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(26),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(26),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(26),
      I1 => Q(1),
      I2 => others_value(26),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[26]\,
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(27),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(27),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(27),
      I1 => Q(1),
      I2 => others_value(27),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[27]\,
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(28),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(28),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(28),
      I1 => Q(1),
      I2 => others_value(28),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[28]\,
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(29),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(29),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(29),
      I1 => Q(1),
      I2 => others_value(29),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[29]\,
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(2),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(2),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[2]_i_4_n_0\,
      I1 => Q(1),
      I2 => others_value(2),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[2]\,
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88B8B8B8"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(2),
      I1 => Q(0),
      I2 => state(2),
      I3 => state(1),
      I4 => state(0),
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(30),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(30),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(30),
      I1 => Q(1),
      I2 => others_value(30),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[30]\,
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(31),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(31),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(31),
      O => \axi_rdata[31]_i_2_n_0\
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(31),
      I1 => Q(1),
      I2 => others_value(31),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[31]\,
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(3),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(3),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(3),
      I1 => Q(1),
      I2 => others_value(3),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[3]\,
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(4),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(4),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(4),
      I1 => Q(1),
      I2 => others_value(4),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[4]\,
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(5),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(5),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(5),
      I1 => Q(1),
      I2 => others_value(5),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[5]\,
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(6),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(6),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(6),
      I1 => Q(1),
      I2 => others_value(6),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[6]\,
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(7),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(7),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(7),
      I1 => Q(1),
      I2 => others_value(7),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[7]\,
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(8),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(8),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(8),
      I1 => Q(1),
      I2 => others_value(8),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[8]\,
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => dout(9),
      I1 => Q(1),
      I2 => \my_value_reg[31]_0\(9),
      I3 => Q(0),
      I4 => \axi_rdata_reg[31]_0\(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B833B800"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(9),
      I1 => Q(1),
      I2 => others_value(9),
      I3 => Q(0),
      I4 => \my_value_reg_n_0_[9]\,
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => \axi_rdata[0]_i_3_n_0\,
      O => D(0),
      S => Q(2)
    );
\axi_rdata_reg[10]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => \axi_rdata[10]_i_3_n_0\,
      O => D(10),
      S => Q(2)
    );
\axi_rdata_reg[11]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => \axi_rdata[11]_i_3_n_0\,
      O => D(11),
      S => Q(2)
    );
\axi_rdata_reg[12]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => \axi_rdata[12]_i_3_n_0\,
      O => D(12),
      S => Q(2)
    );
\axi_rdata_reg[13]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => \axi_rdata[13]_i_3_n_0\,
      O => D(13),
      S => Q(2)
    );
\axi_rdata_reg[14]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => \axi_rdata[14]_i_3_n_0\,
      O => D(14),
      S => Q(2)
    );
\axi_rdata_reg[15]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => \axi_rdata[15]_i_3_n_0\,
      O => D(15),
      S => Q(2)
    );
\axi_rdata_reg[16]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => \axi_rdata[16]_i_3_n_0\,
      O => D(16),
      S => Q(2)
    );
\axi_rdata_reg[17]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => \axi_rdata[17]_i_3_n_0\,
      O => D(17),
      S => Q(2)
    );
\axi_rdata_reg[18]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => \axi_rdata[18]_i_3_n_0\,
      O => D(18),
      S => Q(2)
    );
\axi_rdata_reg[19]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => \axi_rdata[19]_i_3_n_0\,
      O => D(19),
      S => Q(2)
    );
\axi_rdata_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \axi_rdata[1]_i_3_n_0\,
      O => D(1),
      S => Q(2)
    );
\axi_rdata_reg[20]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => \axi_rdata[20]_i_3_n_0\,
      O => D(20),
      S => Q(2)
    );
\axi_rdata_reg[21]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => \axi_rdata[21]_i_3_n_0\,
      O => D(21),
      S => Q(2)
    );
\axi_rdata_reg[22]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => \axi_rdata[22]_i_3_n_0\,
      O => D(22),
      S => Q(2)
    );
\axi_rdata_reg[23]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => \axi_rdata[23]_i_3_n_0\,
      O => D(23),
      S => Q(2)
    );
\axi_rdata_reg[24]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => \axi_rdata[24]_i_3_n_0\,
      O => D(24),
      S => Q(2)
    );
\axi_rdata_reg[25]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => \axi_rdata[25]_i_3_n_0\,
      O => D(25),
      S => Q(2)
    );
\axi_rdata_reg[26]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => \axi_rdata[26]_i_3_n_0\,
      O => D(26),
      S => Q(2)
    );
\axi_rdata_reg[27]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => \axi_rdata[27]_i_3_n_0\,
      O => D(27),
      S => Q(2)
    );
\axi_rdata_reg[28]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => \axi_rdata[28]_i_3_n_0\,
      O => D(28),
      S => Q(2)
    );
\axi_rdata_reg[29]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => \axi_rdata[29]_i_3_n_0\,
      O => D(29),
      S => Q(2)
    );
\axi_rdata_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => \axi_rdata[2]_i_3_n_0\,
      O => D(2),
      S => Q(2)
    );
\axi_rdata_reg[30]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => \axi_rdata[30]_i_3_n_0\,
      O => D(30),
      S => Q(2)
    );
\axi_rdata_reg[31]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_2_n_0\,
      I1 => \axi_rdata[31]_i_3_n_0\,
      O => D(31),
      S => Q(2)
    );
\axi_rdata_reg[3]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => \axi_rdata[3]_i_3_n_0\,
      O => D(3),
      S => Q(2)
    );
\axi_rdata_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => \axi_rdata[4]_i_3_n_0\,
      O => D(4),
      S => Q(2)
    );
\axi_rdata_reg[5]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => \axi_rdata[5]_i_3_n_0\,
      O => D(5),
      S => Q(2)
    );
\axi_rdata_reg[6]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => \axi_rdata[6]_i_3_n_0\,
      O => D(6),
      S => Q(2)
    );
\axi_rdata_reg[7]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => \axi_rdata[7]_i_3_n_0\,
      O => D(7),
      S => Q(2)
    );
\axi_rdata_reg[8]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => \axi_rdata[8]_i_3_n_0\,
      O => D(8),
      S => Q(2)
    );
\axi_rdata_reg[9]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => \axi_rdata[9]_i_3_n_0\,
      O => D(9),
      S => Q(2)
    );
done_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE40"
    )
        port map (
      I0 => state(0),
      I1 => state(2),
      I2 => state(1),
      I3 => done,
      O => done_i_1_n_0
    );
done_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \^sr\(0),
      D => done_i_1_n_0,
      Q => done
    );
dout0_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => dout0_carry_n_0,
      CO(6) => dout0_carry_n_1,
      CO(5) => dout0_carry_n_2,
      CO(4) => dout0_carry_n_3,
      CO(3) => dout0_carry_n_4,
      CO(2) => dout0_carry_n_5,
      CO(1) => dout0_carry_n_6,
      CO(0) => dout0_carry_n_7,
      DI(7) => \my_value_reg_n_0_[7]\,
      DI(6) => \my_value_reg_n_0_[6]\,
      DI(5) => \my_value_reg_n_0_[5]\,
      DI(4) => \my_value_reg_n_0_[4]\,
      DI(3) => \my_value_reg_n_0_[3]\,
      DI(2) => \my_value_reg_n_0_[2]\,
      DI(1) => \my_value_reg_n_0_[1]\,
      DI(0) => \my_value_reg_n_0_[0]\,
      O(7 downto 0) => in6(7 downto 0),
      S(7) => dout0_carry_i_1_n_0,
      S(6) => dout0_carry_i_2_n_0,
      S(5) => dout0_carry_i_3_n_0,
      S(4) => dout0_carry_i_4_n_0,
      S(3) => dout0_carry_i_5_n_0,
      S(2) => dout0_carry_i_6_n_0,
      S(1) => dout0_carry_i_7_n_0,
      S(0) => dout0_carry_i_8_n_0
    );
\dout0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => dout0_carry_n_0,
      CI_TOP => '0',
      CO(7) => \dout0_carry__0_n_0\,
      CO(6) => \dout0_carry__0_n_1\,
      CO(5) => \dout0_carry__0_n_2\,
      CO(4) => \dout0_carry__0_n_3\,
      CO(3) => \dout0_carry__0_n_4\,
      CO(2) => \dout0_carry__0_n_5\,
      CO(1) => \dout0_carry__0_n_6\,
      CO(0) => \dout0_carry__0_n_7\,
      DI(7) => \my_value_reg_n_0_[15]\,
      DI(6) => \my_value_reg_n_0_[14]\,
      DI(5) => \my_value_reg_n_0_[13]\,
      DI(4) => \my_value_reg_n_0_[12]\,
      DI(3) => \my_value_reg_n_0_[11]\,
      DI(2) => \my_value_reg_n_0_[10]\,
      DI(1) => \my_value_reg_n_0_[9]\,
      DI(0) => \my_value_reg_n_0_[8]\,
      O(7 downto 0) => in6(15 downto 8),
      S(7) => \dout0_carry__0_i_1_n_0\,
      S(6) => \dout0_carry__0_i_2_n_0\,
      S(5) => \dout0_carry__0_i_3_n_0\,
      S(4) => \dout0_carry__0_i_4_n_0\,
      S(3) => \dout0_carry__0_i_5_n_0\,
      S(2) => \dout0_carry__0_i_6_n_0\,
      S(1) => \dout0_carry__0_i_7_n_0\,
      S(0) => \dout0_carry__0_i_8_n_0\
    );
\dout0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[15]\,
      I1 => others_value(15),
      O => \dout0_carry__0_i_1_n_0\
    );
\dout0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[14]\,
      I1 => others_value(14),
      O => \dout0_carry__0_i_2_n_0\
    );
\dout0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[13]\,
      I1 => others_value(13),
      O => \dout0_carry__0_i_3_n_0\
    );
\dout0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[12]\,
      I1 => others_value(12),
      O => \dout0_carry__0_i_4_n_0\
    );
\dout0_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[11]\,
      I1 => others_value(11),
      O => \dout0_carry__0_i_5_n_0\
    );
\dout0_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[10]\,
      I1 => others_value(10),
      O => \dout0_carry__0_i_6_n_0\
    );
\dout0_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[9]\,
      I1 => others_value(9),
      O => \dout0_carry__0_i_7_n_0\
    );
\dout0_carry__0_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[8]\,
      I1 => others_value(8),
      O => \dout0_carry__0_i_8_n_0\
    );
\dout0_carry__1\: unisim.vcomponents.CARRY8
     port map (
      CI => \dout0_carry__0_n_0\,
      CI_TOP => '0',
      CO(7) => \dout0_carry__1_n_0\,
      CO(6) => \dout0_carry__1_n_1\,
      CO(5) => \dout0_carry__1_n_2\,
      CO(4) => \dout0_carry__1_n_3\,
      CO(3) => \dout0_carry__1_n_4\,
      CO(2) => \dout0_carry__1_n_5\,
      CO(1) => \dout0_carry__1_n_6\,
      CO(0) => \dout0_carry__1_n_7\,
      DI(7) => \my_value_reg_n_0_[23]\,
      DI(6) => \my_value_reg_n_0_[22]\,
      DI(5) => \my_value_reg_n_0_[21]\,
      DI(4) => \my_value_reg_n_0_[20]\,
      DI(3) => \my_value_reg_n_0_[19]\,
      DI(2) => \my_value_reg_n_0_[18]\,
      DI(1) => \my_value_reg_n_0_[17]\,
      DI(0) => \my_value_reg_n_0_[16]\,
      O(7 downto 0) => in6(23 downto 16),
      S(7) => \dout0_carry__1_i_1_n_0\,
      S(6) => \dout0_carry__1_i_2_n_0\,
      S(5) => \dout0_carry__1_i_3_n_0\,
      S(4) => \dout0_carry__1_i_4_n_0\,
      S(3) => \dout0_carry__1_i_5_n_0\,
      S(2) => \dout0_carry__1_i_6_n_0\,
      S(1) => \dout0_carry__1_i_7_n_0\,
      S(0) => \dout0_carry__1_i_8_n_0\
    );
\dout0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[23]\,
      I1 => others_value(23),
      O => \dout0_carry__1_i_1_n_0\
    );
\dout0_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[22]\,
      I1 => others_value(22),
      O => \dout0_carry__1_i_2_n_0\
    );
\dout0_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[21]\,
      I1 => others_value(21),
      O => \dout0_carry__1_i_3_n_0\
    );
\dout0_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[20]\,
      I1 => others_value(20),
      O => \dout0_carry__1_i_4_n_0\
    );
\dout0_carry__1_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[19]\,
      I1 => others_value(19),
      O => \dout0_carry__1_i_5_n_0\
    );
\dout0_carry__1_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[18]\,
      I1 => others_value(18),
      O => \dout0_carry__1_i_6_n_0\
    );
\dout0_carry__1_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[17]\,
      I1 => others_value(17),
      O => \dout0_carry__1_i_7_n_0\
    );
\dout0_carry__1_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[16]\,
      I1 => others_value(16),
      O => \dout0_carry__1_i_8_n_0\
    );
\dout0_carry__2\: unisim.vcomponents.CARRY8
     port map (
      CI => \dout0_carry__1_n_0\,
      CI_TOP => '0',
      CO(7) => \NLW_dout0_carry__2_CO_UNCONNECTED\(7),
      CO(6) => \dout0_carry__2_n_1\,
      CO(5) => \dout0_carry__2_n_2\,
      CO(4) => \dout0_carry__2_n_3\,
      CO(3) => \dout0_carry__2_n_4\,
      CO(2) => \dout0_carry__2_n_5\,
      CO(1) => \dout0_carry__2_n_6\,
      CO(0) => \dout0_carry__2_n_7\,
      DI(7) => '0',
      DI(6) => \my_value_reg_n_0_[30]\,
      DI(5) => \my_value_reg_n_0_[29]\,
      DI(4) => \my_value_reg_n_0_[28]\,
      DI(3) => \my_value_reg_n_0_[27]\,
      DI(2) => \my_value_reg_n_0_[26]\,
      DI(1) => \my_value_reg_n_0_[25]\,
      DI(0) => \my_value_reg_n_0_[24]\,
      O(7 downto 0) => in6(31 downto 24),
      S(7) => \dout0_carry__2_i_1_n_0\,
      S(6) => \dout0_carry__2_i_2_n_0\,
      S(5) => \dout0_carry__2_i_3_n_0\,
      S(4) => \dout0_carry__2_i_4_n_0\,
      S(3) => \dout0_carry__2_i_5_n_0\,
      S(2) => \dout0_carry__2_i_6_n_0\,
      S(1) => \dout0_carry__2_i_7_n_0\,
      S(0) => \dout0_carry__2_i_8_n_0\
    );
\dout0_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[31]\,
      I1 => others_value(31),
      O => \dout0_carry__2_i_1_n_0\
    );
\dout0_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[30]\,
      I1 => others_value(30),
      O => \dout0_carry__2_i_2_n_0\
    );
\dout0_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[29]\,
      I1 => others_value(29),
      O => \dout0_carry__2_i_3_n_0\
    );
\dout0_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[28]\,
      I1 => others_value(28),
      O => \dout0_carry__2_i_4_n_0\
    );
\dout0_carry__2_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[27]\,
      I1 => others_value(27),
      O => \dout0_carry__2_i_5_n_0\
    );
\dout0_carry__2_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[26]\,
      I1 => others_value(26),
      O => \dout0_carry__2_i_6_n_0\
    );
\dout0_carry__2_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[25]\,
      I1 => others_value(25),
      O => \dout0_carry__2_i_7_n_0\
    );
\dout0_carry__2_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[24]\,
      I1 => others_value(24),
      O => \dout0_carry__2_i_8_n_0\
    );
dout0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[7]\,
      I1 => others_value(7),
      O => dout0_carry_i_1_n_0
    );
dout0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[6]\,
      I1 => others_value(6),
      O => dout0_carry_i_2_n_0
    );
dout0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[5]\,
      I1 => others_value(5),
      O => dout0_carry_i_3_n_0
    );
dout0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[4]\,
      I1 => others_value(4),
      O => dout0_carry_i_4_n_0
    );
dout0_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[3]\,
      I1 => others_value(3),
      O => dout0_carry_i_5_n_0
    );
dout0_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[2]\,
      I1 => others_value(2),
      O => dout0_carry_i_6_n_0
    );
dout0_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[1]\,
      I1 => others_value(1),
      O => dout0_carry_i_7_n_0
    );
dout0_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \my_value_reg_n_0_[0]\,
      I1 => others_value(0),
      O => dout0_carry_i_8_n_0
    );
\dout[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(0),
      O => dout0_in(0)
    );
\dout[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(10),
      O => dout0_in(10)
    );
\dout[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(11),
      O => dout0_in(11)
    );
\dout[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(12),
      O => dout0_in(12)
    );
\dout[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(13),
      O => dout0_in(13)
    );
\dout[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(14),
      O => dout0_in(14)
    );
\dout[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(15),
      O => dout0_in(15)
    );
\dout[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(16),
      O => dout0_in(16)
    );
\dout[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(17),
      O => dout0_in(17)
    );
\dout[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(18),
      O => dout0_in(18)
    );
\dout[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(19),
      O => dout0_in(19)
    );
\dout[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(1),
      O => dout0_in(1)
    );
\dout[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(20),
      O => dout0_in(20)
    );
\dout[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(21),
      O => dout0_in(21)
    );
\dout[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(22),
      O => dout0_in(22)
    );
\dout[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(23),
      O => dout0_in(23)
    );
\dout[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(24),
      O => dout0_in(24)
    );
\dout[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(25),
      O => dout0_in(25)
    );
\dout[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(26),
      O => dout0_in(26)
    );
\dout[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(27),
      O => dout0_in(27)
    );
\dout[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(28),
      O => dout0_in(28)
    );
\dout[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(29),
      O => dout0_in(29)
    );
\dout[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(2),
      O => dout0_in(2)
    );
\dout[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(30),
      O => dout0_in(30)
    );
\dout[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"41"
    )
        port map (
      I0 => state(1),
      I1 => state(2),
      I2 => state(0),
      O => \dout[31]_i_1_n_0\
    );
\dout[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(31),
      O => dout0_in(31)
    );
\dout[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(3),
      O => dout0_in(3)
    );
\dout[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(4),
      O => dout0_in(4)
    );
\dout[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(5),
      O => dout0_in(5)
    );
\dout[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(6),
      O => dout0_in(6)
    );
\dout[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(7),
      O => dout0_in(7)
    );
\dout[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(8),
      O => dout0_in(8)
    );
\dout[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(2),
      I1 => in6(9),
      O => dout0_in(9)
    );
\dout_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(0),
      Q => dout(0)
    );
\dout_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(10),
      Q => dout(10)
    );
\dout_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(11),
      Q => dout(11)
    );
\dout_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(12),
      Q => dout(12)
    );
\dout_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(13),
      Q => dout(13)
    );
\dout_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(14),
      Q => dout(14)
    );
\dout_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(15),
      Q => dout(15)
    );
\dout_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(16),
      Q => dout(16)
    );
\dout_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(17),
      Q => dout(17)
    );
\dout_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(18),
      Q => dout(18)
    );
\dout_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(19),
      Q => dout(19)
    );
\dout_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(1),
      Q => dout(1)
    );
\dout_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(20),
      Q => dout(20)
    );
\dout_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(21),
      Q => dout(21)
    );
\dout_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(22),
      Q => dout(22)
    );
\dout_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(23),
      Q => dout(23)
    );
\dout_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(24),
      Q => dout(24)
    );
\dout_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(25),
      Q => dout(25)
    );
\dout_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(26),
      Q => dout(26)
    );
\dout_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(27),
      Q => dout(27)
    );
\dout_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(28),
      Q => dout(28)
    );
\dout_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(29),
      Q => dout(29)
    );
\dout_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(2),
      Q => dout(2)
    );
\dout_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(30),
      Q => dout(30)
    );
\dout_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(31),
      Q => dout(31)
    );
\dout_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(3),
      Q => dout(3)
    );
\dout_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(4),
      Q => dout(4)
    );
\dout_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(5),
      Q => dout(5)
    );
\dout_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(6),
      Q => dout(6)
    );
\dout_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(7),
      Q => dout(7)
    );
\dout_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(8),
      Q => dout(8)
    );
\dout_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \dout[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => dout0_in(9),
      Q => dout(9)
    );
fin_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEAAAA"
    )
        port map (
      I0 => dst_fin,
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      I4 => fin,
      O => fin_i_1_n_0
    );
fin_reg: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \^sr\(0),
      D => fin_i_1_n_0,
      Q => fin
    );
\my_value[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(0),
      I1 => state(0),
      O => my_value(0)
    );
\my_value[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(10),
      I1 => state(0),
      O => my_value(10)
    );
\my_value[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(11),
      I1 => state(0),
      O => my_value(11)
    );
\my_value[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(12),
      I1 => state(0),
      O => my_value(12)
    );
\my_value[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(13),
      I1 => state(0),
      O => my_value(13)
    );
\my_value[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(14),
      I1 => state(0),
      O => my_value(14)
    );
\my_value[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(15),
      I1 => state(0),
      O => my_value(15)
    );
\my_value[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(16),
      I1 => state(0),
      O => my_value(16)
    );
\my_value[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(17),
      I1 => state(0),
      O => my_value(17)
    );
\my_value[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(18),
      I1 => state(0),
      O => my_value(18)
    );
\my_value[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(19),
      I1 => state(0),
      O => my_value(19)
    );
\my_value[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(1),
      I1 => state(0),
      O => my_value(1)
    );
\my_value[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(20),
      I1 => state(0),
      O => my_value(20)
    );
\my_value[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(21),
      I1 => state(0),
      O => my_value(21)
    );
\my_value[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(22),
      I1 => state(0),
      O => my_value(22)
    );
\my_value[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(23),
      I1 => state(0),
      O => my_value(23)
    );
\my_value[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(24),
      I1 => state(0),
      O => my_value(24)
    );
\my_value[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(25),
      I1 => state(0),
      O => my_value(25)
    );
\my_value[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(26),
      I1 => state(0),
      O => my_value(26)
    );
\my_value[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(27),
      I1 => state(0),
      O => my_value(27)
    );
\my_value[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(28),
      I1 => state(0),
      O => my_value(28)
    );
\my_value[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(29),
      I1 => state(0),
      O => my_value(29)
    );
\my_value[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(2),
      I1 => state(0),
      O => my_value(2)
    );
\my_value[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(30),
      I1 => state(0),
      O => my_value(30)
    );
\my_value[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => state(1),
      I1 => state(2),
      O => \my_value[31]_i_1_n_0\
    );
\my_value[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(31),
      I1 => state(0),
      O => my_value(31)
    );
\my_value[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(3),
      I1 => state(0),
      O => my_value(3)
    );
\my_value[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(4),
      I1 => state(0),
      O => my_value(4)
    );
\my_value[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(5),
      I1 => state(0),
      O => my_value(5)
    );
\my_value[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(6),
      I1 => state(0),
      O => my_value(6)
    );
\my_value[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(7),
      I1 => state(0),
      O => my_value(7)
    );
\my_value[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(8),
      I1 => state(0),
      O => my_value(8)
    );
\my_value[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \my_value_reg[31]_0\(9),
      I1 => state(0),
      O => my_value(9)
    );
\my_value_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(0),
      Q => \my_value_reg_n_0_[0]\
    );
\my_value_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(10),
      Q => \my_value_reg_n_0_[10]\
    );
\my_value_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(11),
      Q => \my_value_reg_n_0_[11]\
    );
\my_value_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(12),
      Q => \my_value_reg_n_0_[12]\
    );
\my_value_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(13),
      Q => \my_value_reg_n_0_[13]\
    );
\my_value_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(14),
      Q => \my_value_reg_n_0_[14]\
    );
\my_value_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(15),
      Q => \my_value_reg_n_0_[15]\
    );
\my_value_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(16),
      Q => \my_value_reg_n_0_[16]\
    );
\my_value_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(17),
      Q => \my_value_reg_n_0_[17]\
    );
\my_value_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(18),
      Q => \my_value_reg_n_0_[18]\
    );
\my_value_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(19),
      Q => \my_value_reg_n_0_[19]\
    );
\my_value_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(1),
      Q => \my_value_reg_n_0_[1]\
    );
\my_value_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(20),
      Q => \my_value_reg_n_0_[20]\
    );
\my_value_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(21),
      Q => \my_value_reg_n_0_[21]\
    );
\my_value_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(22),
      Q => \my_value_reg_n_0_[22]\
    );
\my_value_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(23),
      Q => \my_value_reg_n_0_[23]\
    );
\my_value_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(24),
      Q => \my_value_reg_n_0_[24]\
    );
\my_value_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(25),
      Q => \my_value_reg_n_0_[25]\
    );
\my_value_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(26),
      Q => \my_value_reg_n_0_[26]\
    );
\my_value_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(27),
      Q => \my_value_reg_n_0_[27]\
    );
\my_value_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(28),
      Q => \my_value_reg_n_0_[28]\
    );
\my_value_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(29),
      Q => \my_value_reg_n_0_[29]\
    );
\my_value_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(2),
      Q => \my_value_reg_n_0_[2]\
    );
\my_value_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(30),
      Q => \my_value_reg_n_0_[30]\
    );
\my_value_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(31),
      Q => \my_value_reg_n_0_[31]\
    );
\my_value_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(3),
      Q => \my_value_reg_n_0_[3]\
    );
\my_value_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(4),
      Q => \my_value_reg_n_0_[4]\
    );
\my_value_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(5),
      Q => \my_value_reg_n_0_[5]\
    );
\my_value_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(6),
      Q => \my_value_reg_n_0_[6]\
    );
\my_value_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(7),
      Q => \my_value_reg_n_0_[7]\
    );
\my_value_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(8),
      Q => \my_value_reg_n_0_[8]\
    );
\my_value_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => \my_value[31]_i_1_n_0\,
      CLR => \^sr\(0),
      D => my_value(9),
      Q => \my_value_reg_n_0_[9]\
    );
\others_value_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(0),
      Q => others_value(0)
    );
\others_value_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(10),
      Q => others_value(10)
    );
\others_value_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(11),
      Q => others_value(11)
    );
\others_value_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(12),
      Q => others_value(12)
    );
\others_value_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(13),
      Q => others_value(13)
    );
\others_value_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(14),
      Q => others_value(14)
    );
\others_value_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(15),
      Q => others_value(15)
    );
\others_value_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(16),
      Q => others_value(16)
    );
\others_value_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(17),
      Q => others_value(17)
    );
\others_value_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(18),
      Q => others_value(18)
    );
\others_value_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(19),
      Q => others_value(19)
    );
\others_value_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(1),
      Q => others_value(1)
    );
\others_value_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(20),
      Q => others_value(20)
    );
\others_value_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(21),
      Q => others_value(21)
    );
\others_value_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(22),
      Q => others_value(22)
    );
\others_value_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(23),
      Q => others_value(23)
    );
\others_value_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(24),
      Q => others_value(24)
    );
\others_value_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(25),
      Q => others_value(25)
    );
\others_value_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(26),
      Q => others_value(26)
    );
\others_value_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(27),
      Q => others_value(27)
    );
\others_value_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(28),
      Q => others_value(28)
    );
\others_value_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(29),
      Q => others_value(29)
    );
\others_value_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(2),
      Q => others_value(2)
    );
\others_value_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(30),
      Q => others_value(30)
    );
\others_value_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(31),
      Q => others_value(31)
    );
\others_value_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(3),
      Q => others_value(3)
    );
\others_value_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(4),
      Q => others_value(4)
    );
\others_value_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(5),
      Q => others_value(5)
    );
\others_value_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(6),
      Q => others_value(6)
    );
\others_value_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(7),
      Q => others_value(7)
    );
\others_value_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(8),
      Q => others_value(8)
    );
\others_value_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => dst_init,
      CLR => \^sr\(0),
      D => dst_data(9),
      Q => others_value(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0_S00_AXI is
  port (
    src_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    dst_ack : out STD_LOGIC;
    src_init : out STD_LOGIC;
    src_fin : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dst_init : in STD_LOGIC;
    dst_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    src_ack : in STD_LOGIC;
    dst_fin : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg0_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[9]\ : STD_LOGIC;
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg7[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal \slv_reg_wren__0\ : STD_LOGIC;
  signal u_sample_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \slv_reg1[31]_i_2\ : label is "soft_lutpair52";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC4CCC4CCC4CC"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => aw_en_reg_n_0,
      I2 => \^s_axi_awready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => u_sample_n_0
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      R => u_sample_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      R => u_sample_n_0
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      R => u_sample_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => u_sample_n_0
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => u_sample_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => u_sample_n_0
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => u_sample_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => \^s_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => s00_axi_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => u_sample_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_awready\,
      I3 => \^s_axi_wready\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => u_sample_n_0
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => u_sample_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => u_sample_n_0
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => u_sample_n_0
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => u_sample_n_0
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => u_sample_n_0
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => u_sample_n_0
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => u_sample_n_0
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => u_sample_n_0
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => u_sample_n_0
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => u_sample_n_0
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => u_sample_n_0
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => u_sample_n_0
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => u_sample_n_0
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => u_sample_n_0
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => u_sample_n_0
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => u_sample_n_0
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => u_sample_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => u_sample_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => u_sample_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => u_sample_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => u_sample_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => u_sample_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => u_sample_n_0
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => u_sample_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => u_sample_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => u_sample_n_0
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => u_sample_n_0
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => u_sample_n_0
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => u_sample_n_0
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => u_sample_n_0
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => u_sample_n_0
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => u_sample_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => u_sample_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_wready\,
      I3 => aw_en_reg_n_0,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => u_sample_n_0
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg0(0),
      R => u_sample_n_0
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => \slv_reg0_reg_n_0_[10]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => \slv_reg0_reg_n_0_[11]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => \slv_reg0_reg_n_0_[12]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => \slv_reg0_reg_n_0_[13]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => \slv_reg0_reg_n_0_[14]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => \slv_reg0_reg_n_0_[15]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => \slv_reg0_reg_n_0_[16]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => \slv_reg0_reg_n_0_[17]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => \slv_reg0_reg_n_0_[18]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => \slv_reg0_reg_n_0_[19]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => \slv_reg0_reg_n_0_[1]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => \slv_reg0_reg_n_0_[20]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => \slv_reg0_reg_n_0_[21]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => \slv_reg0_reg_n_0_[22]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => \slv_reg0_reg_n_0_[23]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => \slv_reg0_reg_n_0_[24]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => \slv_reg0_reg_n_0_[25]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => \slv_reg0_reg_n_0_[26]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => \slv_reg0_reg_n_0_[27]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => \slv_reg0_reg_n_0_[28]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => \slv_reg0_reg_n_0_[29]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => \slv_reg0_reg_n_0_[2]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => \slv_reg0_reg_n_0_[30]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => \slv_reg0_reg_n_0_[31]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => \slv_reg0_reg_n_0_[3]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => \slv_reg0_reg_n_0_[4]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => \slv_reg0_reg_n_0_[5]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => \slv_reg0_reg_n_0_[6]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => \slv_reg0_reg_n_0_[7]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => \slv_reg0_reg_n_0_[8]\,
      R => u_sample_n_0
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => \slv_reg0_reg_n_0_[9]\,
      R => u_sample_n_0
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => \^s_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => u_sample_n_0
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => u_sample_n_0
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => u_sample_n_0
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => u_sample_n_0
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => u_sample_n_0
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => u_sample_n_0
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => u_sample_n_0
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => u_sample_n_0
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => u_sample_n_0
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => u_sample_n_0
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => u_sample_n_0
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => u_sample_n_0
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => u_sample_n_0
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => u_sample_n_0
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => u_sample_n_0
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => u_sample_n_0
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => u_sample_n_0
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => u_sample_n_0
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => u_sample_n_0
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => u_sample_n_0
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => u_sample_n_0
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => u_sample_n_0
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => u_sample_n_0
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => u_sample_n_0
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => u_sample_n_0
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => u_sample_n_0
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => u_sample_n_0
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => u_sample_n_0
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => u_sample_n_0
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => u_sample_n_0
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => u_sample_n_0
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => u_sample_n_0
    );
\slv_reg7[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg7[15]_i_1_n_0\
    );
\slv_reg7[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg7[23]_i_1_n_0\
    );
\slv_reg7[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg7[31]_i_1_n_0\
    );
\slv_reg7[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg7[7]_i_1_n_0\
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg7(0),
      R => u_sample_n_0
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg7(10),
      R => u_sample_n_0
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg7(11),
      R => u_sample_n_0
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg7(12),
      R => u_sample_n_0
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg7(13),
      R => u_sample_n_0
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg7(14),
      R => u_sample_n_0
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg7(15),
      R => u_sample_n_0
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg7(16),
      R => u_sample_n_0
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg7(17),
      R => u_sample_n_0
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg7(18),
      R => u_sample_n_0
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg7(19),
      R => u_sample_n_0
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg7(1),
      R => u_sample_n_0
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg7(20),
      R => u_sample_n_0
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg7(21),
      R => u_sample_n_0
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg7(22),
      R => u_sample_n_0
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg7(23),
      R => u_sample_n_0
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg7(24),
      R => u_sample_n_0
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg7(25),
      R => u_sample_n_0
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg7(26),
      R => u_sample_n_0
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg7(27),
      R => u_sample_n_0
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg7(28),
      R => u_sample_n_0
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg7(29),
      R => u_sample_n_0
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg7(2),
      R => u_sample_n_0
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg7(30),
      R => u_sample_n_0
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg7(31),
      R => u_sample_n_0
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg7(3),
      R => u_sample_n_0
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg7(4),
      R => u_sample_n_0
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg7(5),
      R => u_sample_n_0
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg7(6),
      R => u_sample_n_0
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg7(7),
      R => u_sample_n_0
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg7(8),
      R => u_sample_n_0
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg7(9),
      R => u_sample_n_0
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_rvalid\,
      I2 => \^s_axi_arready\,
      O => \slv_reg_rden__0\
    );
u_sample: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample
     port map (
      D(31 downto 0) => reg_data_out(31 downto 0),
      Q(2 downto 0) => sel0(2 downto 0),
      SR(0) => u_sample_n_0,
      \axi_rdata_reg[31]\(31 downto 0) => slv_reg7(31 downto 0),
      \axi_rdata_reg[31]_0\(31) => \slv_reg0_reg_n_0_[31]\,
      \axi_rdata_reg[31]_0\(30) => \slv_reg0_reg_n_0_[30]\,
      \axi_rdata_reg[31]_0\(29) => \slv_reg0_reg_n_0_[29]\,
      \axi_rdata_reg[31]_0\(28) => \slv_reg0_reg_n_0_[28]\,
      \axi_rdata_reg[31]_0\(27) => \slv_reg0_reg_n_0_[27]\,
      \axi_rdata_reg[31]_0\(26) => \slv_reg0_reg_n_0_[26]\,
      \axi_rdata_reg[31]_0\(25) => \slv_reg0_reg_n_0_[25]\,
      \axi_rdata_reg[31]_0\(24) => \slv_reg0_reg_n_0_[24]\,
      \axi_rdata_reg[31]_0\(23) => \slv_reg0_reg_n_0_[23]\,
      \axi_rdata_reg[31]_0\(22) => \slv_reg0_reg_n_0_[22]\,
      \axi_rdata_reg[31]_0\(21) => \slv_reg0_reg_n_0_[21]\,
      \axi_rdata_reg[31]_0\(20) => \slv_reg0_reg_n_0_[20]\,
      \axi_rdata_reg[31]_0\(19) => \slv_reg0_reg_n_0_[19]\,
      \axi_rdata_reg[31]_0\(18) => \slv_reg0_reg_n_0_[18]\,
      \axi_rdata_reg[31]_0\(17) => \slv_reg0_reg_n_0_[17]\,
      \axi_rdata_reg[31]_0\(16) => \slv_reg0_reg_n_0_[16]\,
      \axi_rdata_reg[31]_0\(15) => \slv_reg0_reg_n_0_[15]\,
      \axi_rdata_reg[31]_0\(14) => \slv_reg0_reg_n_0_[14]\,
      \axi_rdata_reg[31]_0\(13) => \slv_reg0_reg_n_0_[13]\,
      \axi_rdata_reg[31]_0\(12) => \slv_reg0_reg_n_0_[12]\,
      \axi_rdata_reg[31]_0\(11) => \slv_reg0_reg_n_0_[11]\,
      \axi_rdata_reg[31]_0\(10) => \slv_reg0_reg_n_0_[10]\,
      \axi_rdata_reg[31]_0\(9) => \slv_reg0_reg_n_0_[9]\,
      \axi_rdata_reg[31]_0\(8) => \slv_reg0_reg_n_0_[8]\,
      \axi_rdata_reg[31]_0\(7) => \slv_reg0_reg_n_0_[7]\,
      \axi_rdata_reg[31]_0\(6) => \slv_reg0_reg_n_0_[6]\,
      \axi_rdata_reg[31]_0\(5) => \slv_reg0_reg_n_0_[5]\,
      \axi_rdata_reg[31]_0\(4) => \slv_reg0_reg_n_0_[4]\,
      \axi_rdata_reg[31]_0\(3) => \slv_reg0_reg_n_0_[3]\,
      \axi_rdata_reg[31]_0\(2) => \slv_reg0_reg_n_0_[2]\,
      \axi_rdata_reg[31]_0\(1) => \slv_reg0_reg_n_0_[1]\,
      \axi_rdata_reg[31]_0\(0) => slv_reg0(0),
      dst_ack => dst_ack,
      dst_data(31 downto 0) => dst_data(31 downto 0),
      dst_fin => dst_fin,
      dst_init => dst_init,
      \my_value_reg[31]_0\(31 downto 0) => slv_reg1(31 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      src_ack => src_ack,
      src_data(31 downto 0) => src_data(31 downto 0),
      src_fin => src_fin,
      src_init => src_init
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0 is
  port (
    src_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    dst_ack : out STD_LOGIC;
    src_init : out STD_LOGIC;
    src_fin : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dst_init : in STD_LOGIC;
    dst_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    src_ack : in STD_LOGIC;
    dst_fin : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0 is
begin
myip_for2_v1_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      dst_ack => dst_ack,
      dst_data(31 downto 0) => dst_data(31 downto 0),
      dst_fin => dst_fin,
      dst_init => dst_init,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      src_ack => src_ack,
      src_data(31 downto 0) => src_data(31 downto 0),
      src_fin => src_fin,
      src_init => src_init
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    dst_init : in STD_LOGIC;
    dst_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dst_ack : out STD_LOGIC;
    dst_fin : in STD_LOGIC;
    src_init : out STD_LOGIC;
    src_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    src_ack : in STD_LOGIC;
    src_fin : out STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_myip_for2_1_0,myip_for2_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "myip_for2_v1_0,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      dst_ack => dst_ack,
      dst_data(31 downto 0) => dst_data(31 downto 0),
      dst_fin => dst_fin,
      dst_init => dst_init,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      src_ack => src_ack,
      src_data(31 downto 0) => src_data(31 downto 0),
      src_fin => src_fin,
      src_init => src_init
    );
end STRUCTURE;
