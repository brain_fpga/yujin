// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Jan 20 11:41:44 2020
// Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_myip_for2_1_0_sim_netlist.v
// Design      : design_1_myip_for2_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_myip_for2_1_0,myip_for2_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "myip_for2_v1_0,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (dst_init,
    dst_data,
    dst_ack,
    dst_fin,
    src_init,
    src_data,
    src_ack,
    src_fin,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input dst_init;
  input [31:0]dst_data;
  output dst_ack;
  input dst_fin;
  output src_init;
  output [31:0]src_data;
  input src_ack;
  output src_fin;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [4:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 8, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_1_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire dst_ack;
  wire [31:0]dst_data;
  wire dst_init;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire src_ack;
  wire [31:0]src_data;
  wire src_fin;
  wire src_init;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0 inst
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .dst_ack(dst_ack),
        .dst_data(dst_data),
        .dst_init(dst_init),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .src_ack(src_ack),
        .src_data(src_data),
        .src_fin(src_fin),
        .src_init(src_init));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0
   (src_data,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    dst_ack,
    src_fin,
    src_init,
    s00_axi_bvalid,
    dst_init,
    dst_data,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    src_ack,
    s00_axi_bready,
    s00_axi_rready,
    s00_axi_aresetn);
  output [31:0]src_data;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output dst_ack;
  output src_fin;
  output src_init;
  output s00_axi_bvalid;
  input dst_init;
  input [31:0]dst_data;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input src_ack;
  input s00_axi_bready;
  input s00_axi_rready;
  input s00_axi_aresetn;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire dst_ack;
  wire [31:0]dst_data;
  wire dst_init;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire src_ack;
  wire [31:0]src_data;
  wire src_fin;
  wire src_init;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0_S00_AXI myip_for2_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .dst_ack(dst_ack),
        .dst_data(dst_data),
        .dst_init(dst_init),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .src_ack(src_ack),
        .src_data(src_data),
        .src_fin(src_fin),
        .src_init(src_init));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_myip_for2_v1_0_S00_AXI
   (src_data,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    dst_ack,
    src_fin,
    src_init,
    s00_axi_bvalid,
    dst_init,
    dst_data,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    src_ack,
    s00_axi_bready,
    s00_axi_rready,
    s00_axi_aresetn);
  output [31:0]src_data;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output dst_ack;
  output src_fin;
  output src_init;
  output s00_axi_bvalid;
  input dst_init;
  input [31:0]dst_data;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input src_ack;
  input s00_axi_bready;
  input s00_axi_rready;
  input s00_axi_aresetn;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire dst_ack;
  wire [31:0]dst_data;
  wire dst_init;
  wire [2:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire [0:0]slv_reg0;
  wire \slv_reg0_reg_n_0_[10] ;
  wire \slv_reg0_reg_n_0_[11] ;
  wire \slv_reg0_reg_n_0_[12] ;
  wire \slv_reg0_reg_n_0_[13] ;
  wire \slv_reg0_reg_n_0_[14] ;
  wire \slv_reg0_reg_n_0_[15] ;
  wire \slv_reg0_reg_n_0_[16] ;
  wire \slv_reg0_reg_n_0_[17] ;
  wire \slv_reg0_reg_n_0_[18] ;
  wire \slv_reg0_reg_n_0_[19] ;
  wire \slv_reg0_reg_n_0_[1] ;
  wire \slv_reg0_reg_n_0_[20] ;
  wire \slv_reg0_reg_n_0_[21] ;
  wire \slv_reg0_reg_n_0_[22] ;
  wire \slv_reg0_reg_n_0_[23] ;
  wire \slv_reg0_reg_n_0_[24] ;
  wire \slv_reg0_reg_n_0_[25] ;
  wire \slv_reg0_reg_n_0_[26] ;
  wire \slv_reg0_reg_n_0_[27] ;
  wire \slv_reg0_reg_n_0_[28] ;
  wire \slv_reg0_reg_n_0_[29] ;
  wire \slv_reg0_reg_n_0_[2] ;
  wire \slv_reg0_reg_n_0_[30] ;
  wire \slv_reg0_reg_n_0_[31] ;
  wire \slv_reg0_reg_n_0_[3] ;
  wire \slv_reg0_reg_n_0_[4] ;
  wire \slv_reg0_reg_n_0_[5] ;
  wire \slv_reg0_reg_n_0_[6] ;
  wire \slv_reg0_reg_n_0_[7] ;
  wire \slv_reg0_reg_n_0_[8] ;
  wire \slv_reg0_reg_n_0_[9] ;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;
  wire src_ack;
  wire [31:0]src_data;
  wire src_fin;
  wire src_init;
  wire u_sample_n_1;

  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(aw_en_reg_n_0),
        .I2(S_AXI_AWREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(u_sample_n_1));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .R(u_sample_n_1));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .R(u_sample_n_1));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .R(u_sample_n_1));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(u_sample_n_1));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(u_sample_n_1));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(u_sample_n_1));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(u_sample_n_1));
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_1
       (.I0(s00_axi_wvalid),
        .I1(S_AXI_AWREADY),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(u_sample_n_1));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WREADY),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(u_sample_n_1));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(u_sample_n_1));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(u_sample_n_1));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_WREADY),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(u_sample_n_1));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg0_reg_n_0_[10] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg0_reg_n_0_[11] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg0_reg_n_0_[12] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg0_reg_n_0_[13] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg0_reg_n_0_[14] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg0_reg_n_0_[15] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg0_reg_n_0_[16] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg0_reg_n_0_[17] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg0_reg_n_0_[18] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg0_reg_n_0_[19] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg0_reg_n_0_[1] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg0_reg_n_0_[20] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg0_reg_n_0_[21] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg0_reg_n_0_[22] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg0_reg_n_0_[23] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg0_reg_n_0_[24] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg0_reg_n_0_[25] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg0_reg_n_0_[26] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg0_reg_n_0_[27] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg0_reg_n_0_[28] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg0_reg_n_0_[29] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg0_reg_n_0_[2] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg0_reg_n_0_[30] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg0_reg_n_0_[31] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg0_reg_n_0_[3] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg0_reg_n_0_[4] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg0_reg_n_0_[5] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg0_reg_n_0_[6] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg0_reg_n_0_[7] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg0_reg_n_0_[8] ),
        .R(u_sample_n_1));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg0_reg_n_0_[9] ),
        .R(u_sample_n_1));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg1[31]_i_2 
       (.I0(S_AXI_WREADY),
        .I1(S_AXI_AWREADY),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(u_sample_n_1));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(u_sample_n_1));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg7[10]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg7[11]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg7[12]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg7[13]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg7[14]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg7[15]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg7[16]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg7[1]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg7[2]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg7[3]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg7[4]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg7[5]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg7[6]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg7[7]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg7[8]),
        .R(u_sample_n_1));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg7[9]),
        .R(u_sample_n_1));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(S_AXI_ARREADY),
        .O(slv_reg_rden__0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample u_sample
       (.D(reg_data_out),
        .Q(sel0),
        .SR(u_sample_n_1),
        .\axi_rdata_reg[31] (slv_reg7),
        .\axi_rdata_reg[31]_0 ({\slv_reg0_reg_n_0_[31] ,\slv_reg0_reg_n_0_[30] ,\slv_reg0_reg_n_0_[29] ,\slv_reg0_reg_n_0_[28] ,\slv_reg0_reg_n_0_[27] ,\slv_reg0_reg_n_0_[26] ,\slv_reg0_reg_n_0_[25] ,\slv_reg0_reg_n_0_[24] ,\slv_reg0_reg_n_0_[23] ,\slv_reg0_reg_n_0_[22] ,\slv_reg0_reg_n_0_[21] ,\slv_reg0_reg_n_0_[20] ,\slv_reg0_reg_n_0_[19] ,\slv_reg0_reg_n_0_[18] ,\slv_reg0_reg_n_0_[17] ,\slv_reg0_reg_n_0_[16] ,\slv_reg0_reg_n_0_[15] ,\slv_reg0_reg_n_0_[14] ,\slv_reg0_reg_n_0_[13] ,\slv_reg0_reg_n_0_[12] ,\slv_reg0_reg_n_0_[11] ,\slv_reg0_reg_n_0_[10] ,\slv_reg0_reg_n_0_[9] ,\slv_reg0_reg_n_0_[8] ,\slv_reg0_reg_n_0_[7] ,\slv_reg0_reg_n_0_[6] ,\slv_reg0_reg_n_0_[5] ,\slv_reg0_reg_n_0_[4] ,\slv_reg0_reg_n_0_[3] ,\slv_reg0_reg_n_0_[2] ,\slv_reg0_reg_n_0_[1] ,slv_reg0}),
        .dst_ack(dst_ack),
        .dst_data(dst_data),
        .dst_init(dst_init),
        .\my_value_reg[31]_0 (slv_reg1),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .src_ack(src_ack),
        .src_data(src_data),
        .src_fin(src_fin),
        .src_init(src_init));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sample
   (dst_ack,
    SR,
    src_fin,
    src_init,
    src_data,
    D,
    dst_init,
    s00_axi_aclk,
    dst_data,
    src_ack,
    s00_axi_aresetn,
    Q,
    \axi_rdata_reg[31] ,
    \axi_rdata_reg[31]_0 ,
    \my_value_reg[31]_0 );
  output dst_ack;
  output [0:0]SR;
  output src_fin;
  output src_init;
  output [31:0]src_data;
  output [31:0]D;
  input dst_init;
  input s00_axi_aclk;
  input [31:0]dst_data;
  input src_ack;
  input s00_axi_aresetn;
  input [2:0]Q;
  input [31:0]\axi_rdata_reg[31] ;
  input [31:0]\axi_rdata_reg[31]_0 ;
  input [31:0]\my_value_reg[31]_0 ;

  wire [31:0]D;
  wire DST_ACK_i_1_n_0;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire [2:0]Q;
  wire [0:0]SR;
  wire [31:0]SRC_DATA0_in;
  wire \SRC_DATA[31]_i_1_n_0 ;
  wire SRC_FIN_i_1_n_0;
  wire SRC_INIT_i_1_n_0;
  wire ack_i_1_n_0;
  wire ack_reg_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire [31:0]\axi_rdata_reg[31] ;
  wire [31:0]\axi_rdata_reg[31]_0 ;
  wire done;
  wire done_i_1_n_0;
  wire [31:0]dout;
  wire dout0_carry__0_i_1_n_0;
  wire dout0_carry__0_i_2_n_0;
  wire dout0_carry__0_i_3_n_0;
  wire dout0_carry__0_i_4_n_0;
  wire dout0_carry__0_i_5_n_0;
  wire dout0_carry__0_i_6_n_0;
  wire dout0_carry__0_i_7_n_0;
  wire dout0_carry__0_i_8_n_0;
  wire dout0_carry__0_n_0;
  wire dout0_carry__0_n_1;
  wire dout0_carry__0_n_2;
  wire dout0_carry__0_n_3;
  wire dout0_carry__0_n_4;
  wire dout0_carry__0_n_5;
  wire dout0_carry__0_n_6;
  wire dout0_carry__0_n_7;
  wire dout0_carry__1_i_1_n_0;
  wire dout0_carry__1_i_2_n_0;
  wire dout0_carry__1_i_3_n_0;
  wire dout0_carry__1_i_4_n_0;
  wire dout0_carry__1_i_5_n_0;
  wire dout0_carry__1_i_6_n_0;
  wire dout0_carry__1_i_7_n_0;
  wire dout0_carry__1_i_8_n_0;
  wire dout0_carry__1_n_0;
  wire dout0_carry__1_n_1;
  wire dout0_carry__1_n_2;
  wire dout0_carry__1_n_3;
  wire dout0_carry__1_n_4;
  wire dout0_carry__1_n_5;
  wire dout0_carry__1_n_6;
  wire dout0_carry__1_n_7;
  wire dout0_carry__2_i_1_n_0;
  wire dout0_carry__2_i_2_n_0;
  wire dout0_carry__2_i_3_n_0;
  wire dout0_carry__2_i_4_n_0;
  wire dout0_carry__2_i_5_n_0;
  wire dout0_carry__2_i_6_n_0;
  wire dout0_carry__2_i_7_n_0;
  wire dout0_carry__2_i_8_n_0;
  wire dout0_carry__2_n_1;
  wire dout0_carry__2_n_2;
  wire dout0_carry__2_n_3;
  wire dout0_carry__2_n_4;
  wire dout0_carry__2_n_5;
  wire dout0_carry__2_n_6;
  wire dout0_carry__2_n_7;
  wire dout0_carry_i_1_n_0;
  wire dout0_carry_i_2_n_0;
  wire dout0_carry_i_3_n_0;
  wire dout0_carry_i_4_n_0;
  wire dout0_carry_i_5_n_0;
  wire dout0_carry_i_6_n_0;
  wire dout0_carry_i_7_n_0;
  wire dout0_carry_i_8_n_0;
  wire dout0_carry_n_0;
  wire dout0_carry_n_1;
  wire dout0_carry_n_2;
  wire dout0_carry_n_3;
  wire dout0_carry_n_4;
  wire dout0_carry_n_5;
  wire dout0_carry_n_6;
  wire dout0_carry_n_7;
  wire [31:0]dout0_in;
  wire \dout[31]_i_1_n_0 ;
  wire dst_ack;
  wire [31:0]dst_data;
  wire dst_init;
  wire fin;
  wire fin_i_1_n_0;
  wire [31:0]in6;
  wire [31:0]my_value;
  wire \my_value[31]_i_1_n_0 ;
  wire [31:0]\my_value_reg[31]_0 ;
  wire \my_value_reg_n_0_[0] ;
  wire \my_value_reg_n_0_[10] ;
  wire \my_value_reg_n_0_[11] ;
  wire \my_value_reg_n_0_[12] ;
  wire \my_value_reg_n_0_[13] ;
  wire \my_value_reg_n_0_[14] ;
  wire \my_value_reg_n_0_[15] ;
  wire \my_value_reg_n_0_[16] ;
  wire \my_value_reg_n_0_[17] ;
  wire \my_value_reg_n_0_[18] ;
  wire \my_value_reg_n_0_[19] ;
  wire \my_value_reg_n_0_[1] ;
  wire \my_value_reg_n_0_[20] ;
  wire \my_value_reg_n_0_[21] ;
  wire \my_value_reg_n_0_[22] ;
  wire \my_value_reg_n_0_[23] ;
  wire \my_value_reg_n_0_[24] ;
  wire \my_value_reg_n_0_[25] ;
  wire \my_value_reg_n_0_[26] ;
  wire \my_value_reg_n_0_[27] ;
  wire \my_value_reg_n_0_[28] ;
  wire \my_value_reg_n_0_[29] ;
  wire \my_value_reg_n_0_[2] ;
  wire \my_value_reg_n_0_[30] ;
  wire \my_value_reg_n_0_[31] ;
  wire \my_value_reg_n_0_[3] ;
  wire \my_value_reg_n_0_[4] ;
  wire \my_value_reg_n_0_[5] ;
  wire \my_value_reg_n_0_[6] ;
  wire \my_value_reg_n_0_[7] ;
  wire \my_value_reg_n_0_[8] ;
  wire \my_value_reg_n_0_[9] ;
  wire [31:0]others_value;
  wire \others_value[0]_i_1_n_0 ;
  wire \others_value[10]_i_1_n_0 ;
  wire \others_value[11]_i_1_n_0 ;
  wire \others_value[12]_i_1_n_0 ;
  wire \others_value[13]_i_1_n_0 ;
  wire \others_value[14]_i_1_n_0 ;
  wire \others_value[15]_i_1_n_0 ;
  wire \others_value[16]_i_1_n_0 ;
  wire \others_value[17]_i_1_n_0 ;
  wire \others_value[18]_i_1_n_0 ;
  wire \others_value[19]_i_1_n_0 ;
  wire \others_value[1]_i_1_n_0 ;
  wire \others_value[20]_i_1_n_0 ;
  wire \others_value[21]_i_1_n_0 ;
  wire \others_value[22]_i_1_n_0 ;
  wire \others_value[23]_i_1_n_0 ;
  wire \others_value[24]_i_1_n_0 ;
  wire \others_value[25]_i_1_n_0 ;
  wire \others_value[26]_i_1_n_0 ;
  wire \others_value[27]_i_1_n_0 ;
  wire \others_value[28]_i_1_n_0 ;
  wire \others_value[29]_i_1_n_0 ;
  wire \others_value[2]_i_1_n_0 ;
  wire \others_value[30]_i_1_n_0 ;
  wire \others_value[31]_i_1_n_0 ;
  wire \others_value[3]_i_1_n_0 ;
  wire \others_value[4]_i_1_n_0 ;
  wire \others_value[5]_i_1_n_0 ;
  wire \others_value[6]_i_1_n_0 ;
  wire \others_value[7]_i_1_n_0 ;
  wire \others_value[8]_i_1_n_0 ;
  wire \others_value[9]_i_1_n_0 ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire src_ack;
  wire [31:0]src_data;
  wire src_fin;
  wire src_init;
  wire [2:0]state;
  wire [7:7]NLW_dout0_carry__2_CO_UNCONNECTED;

  LUT4 #(
    .INIT(16'hAAAB)) 
    DST_ACK_i_1
       (.I0(dst_init),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .O(DST_ACK_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    DST_ACK_i_2
       (.I0(s00_axi_aresetn),
        .O(SR));
  FDCE DST_ACK_reg
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(dst_init),
        .Q(dst_ack));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(state[0]),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3F3F0F0F3B383B38)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(fin),
        .I1(state[2]),
        .I2(state[1]),
        .I3(\axi_rdata_reg[31]_0 [0]),
        .I4(ack_reg_n_0),
        .I5(state[0]),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h38)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(state[0]),
        .I1(state[1]),
        .I2(state[2]),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  (* FSM_ENCODED_STATES = "STATE_IDLE:000,STATE_STORE:001,STATE_SEND:010,STATE_ACK_WAIT:011,STATE_FIN:100,STATE_CAL:101,STATE_DONE:110," *) 
  FDCE \FSM_sequential_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\FSM_sequential_state[2]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(state[0]));
  (* FSM_ENCODED_STATES = "STATE_IDLE:000,STATE_STORE:001,STATE_SEND:010,STATE_ACK_WAIT:011,STATE_FIN:100,STATE_CAL:101,STATE_DONE:110," *) 
  FDCE \FSM_sequential_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\FSM_sequential_state[2]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state[1]));
  (* FSM_ENCODED_STATES = "STATE_IDLE:000,STATE_STORE:001,STATE_SEND:010,STATE_ACK_WAIT:011,STATE_FIN:100,STATE_CAL:101,STATE_DONE:110," *) 
  FDCE \FSM_sequential_state_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\FSM_sequential_state[2]_i_1_n_0 ),
        .CLR(SR),
        .D(\FSM_sequential_state[2]_i_2_n_0 ),
        .Q(state[2]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[0]_i_1 
       (.I0(\my_value_reg_n_0_[0] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[10]_i_1 
       (.I0(\my_value_reg_n_0_[10] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[11]_i_1 
       (.I0(\my_value_reg_n_0_[11] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[12]_i_1 
       (.I0(\my_value_reg_n_0_[12] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[13]_i_1 
       (.I0(\my_value_reg_n_0_[13] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[14]_i_1 
       (.I0(\my_value_reg_n_0_[14] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[15]_i_1 
       (.I0(\my_value_reg_n_0_[15] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[16]_i_1 
       (.I0(\my_value_reg_n_0_[16] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[17]_i_1 
       (.I0(\my_value_reg_n_0_[17] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[18]_i_1 
       (.I0(\my_value_reg_n_0_[18] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[19]_i_1 
       (.I0(\my_value_reg_n_0_[19] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[1]_i_1 
       (.I0(\my_value_reg_n_0_[1] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[20]_i_1 
       (.I0(\my_value_reg_n_0_[20] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[21]_i_1 
       (.I0(\my_value_reg_n_0_[21] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[22]_i_1 
       (.I0(\my_value_reg_n_0_[22] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[23]_i_1 
       (.I0(\my_value_reg_n_0_[23] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[24]_i_1 
       (.I0(\my_value_reg_n_0_[24] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[25]_i_1 
       (.I0(\my_value_reg_n_0_[25] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[26]_i_1 
       (.I0(\my_value_reg_n_0_[26] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[27]_i_1 
       (.I0(\my_value_reg_n_0_[27] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[28]_i_1 
       (.I0(\my_value_reg_n_0_[28] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[29]_i_1 
       (.I0(\my_value_reg_n_0_[29] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[2]_i_1 
       (.I0(\my_value_reg_n_0_[2] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[30]_i_1 
       (.I0(\my_value_reg_n_0_[30] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[30]));
  LUT2 #(
    .INIT(4'h1)) 
    \SRC_DATA[31]_i_1 
       (.I0(state[0]),
        .I1(state[2]),
        .O(\SRC_DATA[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[31]_i_2 
       (.I0(\my_value_reg_n_0_[31] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[3]_i_1 
       (.I0(\my_value_reg_n_0_[3] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[4]_i_1 
       (.I0(\my_value_reg_n_0_[4] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[5]_i_1 
       (.I0(\my_value_reg_n_0_[5] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[6]_i_1 
       (.I0(\my_value_reg_n_0_[6] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[7]_i_1 
       (.I0(\my_value_reg_n_0_[7] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[8]_i_1 
       (.I0(\my_value_reg_n_0_[8] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \SRC_DATA[9]_i_1 
       (.I0(\my_value_reg_n_0_[9] ),
        .I1(state[1]),
        .O(SRC_DATA0_in[9]));
  FDCE \SRC_DATA_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[0]),
        .Q(src_data[0]));
  FDCE \SRC_DATA_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[10]),
        .Q(src_data[10]));
  FDCE \SRC_DATA_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[11]),
        .Q(src_data[11]));
  FDCE \SRC_DATA_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[12]),
        .Q(src_data[12]));
  FDCE \SRC_DATA_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[13]),
        .Q(src_data[13]));
  FDCE \SRC_DATA_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[14]),
        .Q(src_data[14]));
  FDCE \SRC_DATA_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[15]),
        .Q(src_data[15]));
  FDCE \SRC_DATA_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[16]),
        .Q(src_data[16]));
  FDCE \SRC_DATA_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[17]),
        .Q(src_data[17]));
  FDCE \SRC_DATA_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[18]),
        .Q(src_data[18]));
  FDCE \SRC_DATA_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[19]),
        .Q(src_data[19]));
  FDCE \SRC_DATA_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[1]),
        .Q(src_data[1]));
  FDCE \SRC_DATA_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[20]),
        .Q(src_data[20]));
  FDCE \SRC_DATA_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[21]),
        .Q(src_data[21]));
  FDCE \SRC_DATA_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[22]),
        .Q(src_data[22]));
  FDCE \SRC_DATA_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[23]),
        .Q(src_data[23]));
  FDCE \SRC_DATA_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[24]),
        .Q(src_data[24]));
  FDCE \SRC_DATA_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[25]),
        .Q(src_data[25]));
  FDCE \SRC_DATA_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[26]),
        .Q(src_data[26]));
  FDCE \SRC_DATA_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[27]),
        .Q(src_data[27]));
  FDCE \SRC_DATA_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[28]),
        .Q(src_data[28]));
  FDCE \SRC_DATA_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[29]),
        .Q(src_data[29]));
  FDCE \SRC_DATA_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[2]),
        .Q(src_data[2]));
  FDCE \SRC_DATA_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[30]),
        .Q(src_data[30]));
  FDCE \SRC_DATA_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[31]),
        .Q(src_data[31]));
  FDCE \SRC_DATA_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[3]),
        .Q(src_data[3]));
  FDCE \SRC_DATA_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[4]),
        .Q(src_data[4]));
  FDCE \SRC_DATA_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[5]),
        .Q(src_data[5]));
  FDCE \SRC_DATA_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[6]),
        .Q(src_data[6]));
  FDCE \SRC_DATA_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[7]),
        .Q(src_data[7]));
  FDCE \SRC_DATA_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[8]),
        .Q(src_data[8]));
  FDCE \SRC_DATA_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\SRC_DATA[31]_i_1_n_0 ),
        .CLR(SR),
        .D(SRC_DATA0_in[9]),
        .Q(src_data[9]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFE02)) 
    SRC_FIN_i_1
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(src_fin),
        .O(SRC_FIN_i_1_n_0));
  FDCE SRC_FIN_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(SRC_FIN_i_1_n_0),
        .Q(src_fin));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hBE04)) 
    SRC_INIT_i_1
       (.I0(state[2]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(src_init),
        .O(SRC_INIT_i_1_n_0));
  FDCE SRC_INIT_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(SRC_INIT_i_1_n_0),
        .Q(src_init));
  LUT5 #(
    .INIT(32'hFEFE4000)) 
    ack_i_1
       (.I0(state[2]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(src_ack),
        .I4(ack_reg_n_0),
        .O(ack_i_1_n_0));
  FDCE ack_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(ack_i_1_n_0),
        .Q(ack_reg_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(done),
        .I1(dout[0]),
        .I2(Q[1]),
        .I3(\my_value_reg[31]_0 [0]),
        .I4(Q[0]),
        .I5(\axi_rdata_reg[31]_0 [0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[0]_i_3 
       (.I0(\axi_rdata[0]_i_4_n_0 ),
        .I1(Q[1]),
        .I2(others_value[0]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[0] ),
        .O(\axi_rdata[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h88B8B8B8)) 
    \axi_rdata[0]_i_4 
       (.I0(\axi_rdata_reg[31] [0]),
        .I1(Q[0]),
        .I2(state[0]),
        .I3(state[2]),
        .I4(state[1]),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_2 
       (.I0(dout[10]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [10]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[10]_i_3 
       (.I0(\axi_rdata_reg[31] [10]),
        .I1(Q[1]),
        .I2(others_value[10]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[10] ),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_2 
       (.I0(dout[11]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [11]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[11]_i_3 
       (.I0(\axi_rdata_reg[31] [11]),
        .I1(Q[1]),
        .I2(others_value[11]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[11] ),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_2 
       (.I0(dout[12]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [12]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[12]_i_3 
       (.I0(\axi_rdata_reg[31] [12]),
        .I1(Q[1]),
        .I2(others_value[12]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[12] ),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_2 
       (.I0(dout[13]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [13]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[13]_i_3 
       (.I0(\axi_rdata_reg[31] [13]),
        .I1(Q[1]),
        .I2(others_value[13]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[13] ),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_2 
       (.I0(dout[14]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [14]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[14]_i_3 
       (.I0(\axi_rdata_reg[31] [14]),
        .I1(Q[1]),
        .I2(others_value[14]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[14] ),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_2 
       (.I0(dout[15]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [15]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[15]_i_3 
       (.I0(\axi_rdata_reg[31] [15]),
        .I1(Q[1]),
        .I2(others_value[15]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[15] ),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_2 
       (.I0(dout[16]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [16]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[16]_i_3 
       (.I0(\axi_rdata_reg[31] [16]),
        .I1(Q[1]),
        .I2(others_value[16]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[16] ),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_2 
       (.I0(dout[17]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [17]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[17]_i_3 
       (.I0(\axi_rdata_reg[31] [17]),
        .I1(Q[1]),
        .I2(others_value[17]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[17] ),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_2 
       (.I0(dout[18]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [18]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[18]_i_3 
       (.I0(\axi_rdata_reg[31] [18]),
        .I1(Q[1]),
        .I2(others_value[18]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[18] ),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_2 
       (.I0(dout[19]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [19]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[19]_i_3 
       (.I0(\axi_rdata_reg[31] [19]),
        .I1(Q[1]),
        .I2(others_value[19]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[19] ),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_2 
       (.I0(dout[1]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [1]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[1]_i_3 
       (.I0(\axi_rdata[1]_i_4_n_0 ),
        .I1(Q[1]),
        .I2(others_value[1]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[1] ),
        .O(\axi_rdata[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h88B8B8B8)) 
    \axi_rdata[1]_i_4 
       (.I0(\axi_rdata_reg[31] [1]),
        .I1(Q[0]),
        .I2(state[1]),
        .I3(state[2]),
        .I4(state[0]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_2 
       (.I0(dout[20]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [20]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[20]_i_3 
       (.I0(\axi_rdata_reg[31] [20]),
        .I1(Q[1]),
        .I2(others_value[20]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[20] ),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_2 
       (.I0(dout[21]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [21]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[21]_i_3 
       (.I0(\axi_rdata_reg[31] [21]),
        .I1(Q[1]),
        .I2(others_value[21]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[21] ),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_2 
       (.I0(dout[22]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [22]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[22]_i_3 
       (.I0(\axi_rdata_reg[31] [22]),
        .I1(Q[1]),
        .I2(others_value[22]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[22] ),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_2 
       (.I0(dout[23]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [23]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[23]_i_3 
       (.I0(\axi_rdata_reg[31] [23]),
        .I1(Q[1]),
        .I2(others_value[23]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[23] ),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_2 
       (.I0(dout[24]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [24]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[24]_i_3 
       (.I0(\axi_rdata_reg[31] [24]),
        .I1(Q[1]),
        .I2(others_value[24]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[24] ),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_2 
       (.I0(dout[25]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [25]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[25]_i_3 
       (.I0(\axi_rdata_reg[31] [25]),
        .I1(Q[1]),
        .I2(others_value[25]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[25] ),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_2 
       (.I0(dout[26]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [26]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[26]_i_3 
       (.I0(\axi_rdata_reg[31] [26]),
        .I1(Q[1]),
        .I2(others_value[26]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[26] ),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_2 
       (.I0(dout[27]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [27]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[27]_i_3 
       (.I0(\axi_rdata_reg[31] [27]),
        .I1(Q[1]),
        .I2(others_value[27]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[27] ),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_2 
       (.I0(dout[28]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [28]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[28]_i_3 
       (.I0(\axi_rdata_reg[31] [28]),
        .I1(Q[1]),
        .I2(others_value[28]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[28] ),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_2 
       (.I0(dout[29]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [29]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[29]_i_3 
       (.I0(\axi_rdata_reg[31] [29]),
        .I1(Q[1]),
        .I2(others_value[29]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[29] ),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_2 
       (.I0(dout[2]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [2]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[2]_i_3 
       (.I0(\axi_rdata[2]_i_4_n_0 ),
        .I1(Q[1]),
        .I2(others_value[2]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[2] ),
        .O(\axi_rdata[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h88B8B8B8)) 
    \axi_rdata[2]_i_4 
       (.I0(\axi_rdata_reg[31] [2]),
        .I1(Q[0]),
        .I2(state[2]),
        .I3(state[1]),
        .I4(state[0]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_2 
       (.I0(dout[30]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [30]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[30]_i_3 
       (.I0(\axi_rdata_reg[31] [30]),
        .I1(Q[1]),
        .I2(others_value[30]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[30] ),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_2 
       (.I0(dout[31]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [31]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [31]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[31]_i_3 
       (.I0(\axi_rdata_reg[31] [31]),
        .I1(Q[1]),
        .I2(others_value[31]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[31] ),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_2 
       (.I0(dout[3]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [3]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[3]_i_3 
       (.I0(\axi_rdata_reg[31] [3]),
        .I1(Q[1]),
        .I2(others_value[3]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[3] ),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_2 
       (.I0(dout[4]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [4]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[4]_i_3 
       (.I0(\axi_rdata_reg[31] [4]),
        .I1(Q[1]),
        .I2(others_value[4]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[4] ),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_2 
       (.I0(dout[5]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [5]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[5]_i_3 
       (.I0(\axi_rdata_reg[31] [5]),
        .I1(Q[1]),
        .I2(others_value[5]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[5] ),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_2 
       (.I0(dout[6]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [6]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[6]_i_3 
       (.I0(\axi_rdata_reg[31] [6]),
        .I1(Q[1]),
        .I2(others_value[6]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[6] ),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_2 
       (.I0(dout[7]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [7]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[7]_i_3 
       (.I0(\axi_rdata_reg[31] [7]),
        .I1(Q[1]),
        .I2(others_value[7]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[7] ),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_2 
       (.I0(dout[8]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [8]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[8]_i_3 
       (.I0(\axi_rdata_reg[31] [8]),
        .I1(Q[1]),
        .I2(others_value[8]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[8] ),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_2 
       (.I0(dout[9]),
        .I1(Q[1]),
        .I2(\my_value_reg[31]_0 [9]),
        .I3(Q[0]),
        .I4(\axi_rdata_reg[31]_0 [9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \axi_rdata[9]_i_3 
       (.I0(\axi_rdata_reg[31] [9]),
        .I1(Q[1]),
        .I2(others_value[9]),
        .I3(Q[0]),
        .I4(\my_value_reg_n_0_[9] ),
        .O(\axi_rdata[9]_i_3_n_0 ));
  MUXF7 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .O(D[0]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(\axi_rdata[10]_i_3_n_0 ),
        .O(D[10]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(\axi_rdata[11]_i_3_n_0 ),
        .O(D[11]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(\axi_rdata[12]_i_3_n_0 ),
        .O(D[12]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(\axi_rdata[13]_i_3_n_0 ),
        .O(D[13]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(\axi_rdata[14]_i_3_n_0 ),
        .O(D[14]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(\axi_rdata[15]_i_3_n_0 ),
        .O(D[15]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(\axi_rdata[16]_i_3_n_0 ),
        .O(D[16]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(\axi_rdata[17]_i_3_n_0 ),
        .O(D[17]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(\axi_rdata[18]_i_3_n_0 ),
        .O(D[18]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(\axi_rdata[19]_i_3_n_0 ),
        .O(D[19]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata[1]_i_3_n_0 ),
        .O(D[1]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(\axi_rdata[20]_i_3_n_0 ),
        .O(D[20]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(\axi_rdata[21]_i_3_n_0 ),
        .O(D[21]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(\axi_rdata[22]_i_3_n_0 ),
        .O(D[22]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(\axi_rdata[23]_i_3_n_0 ),
        .O(D[23]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(\axi_rdata[24]_i_3_n_0 ),
        .O(D[24]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(\axi_rdata[25]_i_3_n_0 ),
        .O(D[25]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(\axi_rdata[26]_i_3_n_0 ),
        .O(D[26]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(\axi_rdata[27]_i_3_n_0 ),
        .O(D[27]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(\axi_rdata[28]_i_3_n_0 ),
        .O(D[28]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(\axi_rdata[29]_i_3_n_0 ),
        .O(D[29]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata[2]_i_3_n_0 ),
        .O(D[2]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(\axi_rdata[30]_i_3_n_0 ),
        .O(D[30]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[31]_i_1 
       (.I0(\axi_rdata[31]_i_2_n_0 ),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .O(D[31]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata[3]_i_3_n_0 ),
        .O(D[3]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata[4]_i_3_n_0 ),
        .O(D[4]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .O(D[5]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\axi_rdata[6]_i_3_n_0 ),
        .O(D[6]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(\axi_rdata[7]_i_3_n_0 ),
        .O(D[7]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(\axi_rdata[8]_i_3_n_0 ),
        .O(D[8]),
        .S(Q[2]));
  MUXF7 \axi_rdata_reg[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(\axi_rdata[9]_i_3_n_0 ),
        .O(D[9]),
        .S(Q[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFE40)) 
    done_i_1
       (.I0(state[0]),
        .I1(state[2]),
        .I2(state[1]),
        .I3(done),
        .O(done_i_1_n_0));
  FDCE done_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(done_i_1_n_0),
        .Q(done));
  CARRY8 dout0_carry
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({dout0_carry_n_0,dout0_carry_n_1,dout0_carry_n_2,dout0_carry_n_3,dout0_carry_n_4,dout0_carry_n_5,dout0_carry_n_6,dout0_carry_n_7}),
        .DI({\my_value_reg_n_0_[7] ,\my_value_reg_n_0_[6] ,\my_value_reg_n_0_[5] ,\my_value_reg_n_0_[4] ,\my_value_reg_n_0_[3] ,\my_value_reg_n_0_[2] ,\my_value_reg_n_0_[1] ,\my_value_reg_n_0_[0] }),
        .O(in6[7:0]),
        .S({dout0_carry_i_1_n_0,dout0_carry_i_2_n_0,dout0_carry_i_3_n_0,dout0_carry_i_4_n_0,dout0_carry_i_5_n_0,dout0_carry_i_6_n_0,dout0_carry_i_7_n_0,dout0_carry_i_8_n_0}));
  CARRY8 dout0_carry__0
       (.CI(dout0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({dout0_carry__0_n_0,dout0_carry__0_n_1,dout0_carry__0_n_2,dout0_carry__0_n_3,dout0_carry__0_n_4,dout0_carry__0_n_5,dout0_carry__0_n_6,dout0_carry__0_n_7}),
        .DI({\my_value_reg_n_0_[15] ,\my_value_reg_n_0_[14] ,\my_value_reg_n_0_[13] ,\my_value_reg_n_0_[12] ,\my_value_reg_n_0_[11] ,\my_value_reg_n_0_[10] ,\my_value_reg_n_0_[9] ,\my_value_reg_n_0_[8] }),
        .O(in6[15:8]),
        .S({dout0_carry__0_i_1_n_0,dout0_carry__0_i_2_n_0,dout0_carry__0_i_3_n_0,dout0_carry__0_i_4_n_0,dout0_carry__0_i_5_n_0,dout0_carry__0_i_6_n_0,dout0_carry__0_i_7_n_0,dout0_carry__0_i_8_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_1
       (.I0(\my_value_reg_n_0_[15] ),
        .I1(others_value[15]),
        .O(dout0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_2
       (.I0(\my_value_reg_n_0_[14] ),
        .I1(others_value[14]),
        .O(dout0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_3
       (.I0(\my_value_reg_n_0_[13] ),
        .I1(others_value[13]),
        .O(dout0_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_4
       (.I0(\my_value_reg_n_0_[12] ),
        .I1(others_value[12]),
        .O(dout0_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_5
       (.I0(\my_value_reg_n_0_[11] ),
        .I1(others_value[11]),
        .O(dout0_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_6
       (.I0(\my_value_reg_n_0_[10] ),
        .I1(others_value[10]),
        .O(dout0_carry__0_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_7
       (.I0(\my_value_reg_n_0_[9] ),
        .I1(others_value[9]),
        .O(dout0_carry__0_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__0_i_8
       (.I0(\my_value_reg_n_0_[8] ),
        .I1(others_value[8]),
        .O(dout0_carry__0_i_8_n_0));
  CARRY8 dout0_carry__1
       (.CI(dout0_carry__0_n_0),
        .CI_TOP(1'b0),
        .CO({dout0_carry__1_n_0,dout0_carry__1_n_1,dout0_carry__1_n_2,dout0_carry__1_n_3,dout0_carry__1_n_4,dout0_carry__1_n_5,dout0_carry__1_n_6,dout0_carry__1_n_7}),
        .DI({\my_value_reg_n_0_[23] ,\my_value_reg_n_0_[22] ,\my_value_reg_n_0_[21] ,\my_value_reg_n_0_[20] ,\my_value_reg_n_0_[19] ,\my_value_reg_n_0_[18] ,\my_value_reg_n_0_[17] ,\my_value_reg_n_0_[16] }),
        .O(in6[23:16]),
        .S({dout0_carry__1_i_1_n_0,dout0_carry__1_i_2_n_0,dout0_carry__1_i_3_n_0,dout0_carry__1_i_4_n_0,dout0_carry__1_i_5_n_0,dout0_carry__1_i_6_n_0,dout0_carry__1_i_7_n_0,dout0_carry__1_i_8_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_1
       (.I0(\my_value_reg_n_0_[23] ),
        .I1(others_value[23]),
        .O(dout0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_2
       (.I0(\my_value_reg_n_0_[22] ),
        .I1(others_value[22]),
        .O(dout0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_3
       (.I0(\my_value_reg_n_0_[21] ),
        .I1(others_value[21]),
        .O(dout0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_4
       (.I0(\my_value_reg_n_0_[20] ),
        .I1(others_value[20]),
        .O(dout0_carry__1_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_5
       (.I0(\my_value_reg_n_0_[19] ),
        .I1(others_value[19]),
        .O(dout0_carry__1_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_6
       (.I0(\my_value_reg_n_0_[18] ),
        .I1(others_value[18]),
        .O(dout0_carry__1_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_7
       (.I0(\my_value_reg_n_0_[17] ),
        .I1(others_value[17]),
        .O(dout0_carry__1_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__1_i_8
       (.I0(\my_value_reg_n_0_[16] ),
        .I1(others_value[16]),
        .O(dout0_carry__1_i_8_n_0));
  CARRY8 dout0_carry__2
       (.CI(dout0_carry__1_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_dout0_carry__2_CO_UNCONNECTED[7],dout0_carry__2_n_1,dout0_carry__2_n_2,dout0_carry__2_n_3,dout0_carry__2_n_4,dout0_carry__2_n_5,dout0_carry__2_n_6,dout0_carry__2_n_7}),
        .DI({1'b0,\my_value_reg_n_0_[30] ,\my_value_reg_n_0_[29] ,\my_value_reg_n_0_[28] ,\my_value_reg_n_0_[27] ,\my_value_reg_n_0_[26] ,\my_value_reg_n_0_[25] ,\my_value_reg_n_0_[24] }),
        .O(in6[31:24]),
        .S({dout0_carry__2_i_1_n_0,dout0_carry__2_i_2_n_0,dout0_carry__2_i_3_n_0,dout0_carry__2_i_4_n_0,dout0_carry__2_i_5_n_0,dout0_carry__2_i_6_n_0,dout0_carry__2_i_7_n_0,dout0_carry__2_i_8_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_1
       (.I0(\my_value_reg_n_0_[31] ),
        .I1(others_value[31]),
        .O(dout0_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_2
       (.I0(\my_value_reg_n_0_[30] ),
        .I1(others_value[30]),
        .O(dout0_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_3
       (.I0(\my_value_reg_n_0_[29] ),
        .I1(others_value[29]),
        .O(dout0_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_4
       (.I0(\my_value_reg_n_0_[28] ),
        .I1(others_value[28]),
        .O(dout0_carry__2_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_5
       (.I0(\my_value_reg_n_0_[27] ),
        .I1(others_value[27]),
        .O(dout0_carry__2_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_6
       (.I0(\my_value_reg_n_0_[26] ),
        .I1(others_value[26]),
        .O(dout0_carry__2_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_7
       (.I0(\my_value_reg_n_0_[25] ),
        .I1(others_value[25]),
        .O(dout0_carry__2_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry__2_i_8
       (.I0(\my_value_reg_n_0_[24] ),
        .I1(others_value[24]),
        .O(dout0_carry__2_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_1
       (.I0(\my_value_reg_n_0_[7] ),
        .I1(others_value[7]),
        .O(dout0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_2
       (.I0(\my_value_reg_n_0_[6] ),
        .I1(others_value[6]),
        .O(dout0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_3
       (.I0(\my_value_reg_n_0_[5] ),
        .I1(others_value[5]),
        .O(dout0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_4
       (.I0(\my_value_reg_n_0_[4] ),
        .I1(others_value[4]),
        .O(dout0_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_5
       (.I0(\my_value_reg_n_0_[3] ),
        .I1(others_value[3]),
        .O(dout0_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_6
       (.I0(\my_value_reg_n_0_[2] ),
        .I1(others_value[2]),
        .O(dout0_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_7
       (.I0(\my_value_reg_n_0_[1] ),
        .I1(others_value[1]),
        .O(dout0_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    dout0_carry_i_8
       (.I0(\my_value_reg_n_0_[0] ),
        .I1(others_value[0]),
        .O(dout0_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[0]_i_1 
       (.I0(state[2]),
        .I1(in6[0]),
        .O(dout0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[10]_i_1 
       (.I0(state[2]),
        .I1(in6[10]),
        .O(dout0_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[11]_i_1 
       (.I0(state[2]),
        .I1(in6[11]),
        .O(dout0_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[12]_i_1 
       (.I0(state[2]),
        .I1(in6[12]),
        .O(dout0_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[13]_i_1 
       (.I0(state[2]),
        .I1(in6[13]),
        .O(dout0_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[14]_i_1 
       (.I0(state[2]),
        .I1(in6[14]),
        .O(dout0_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[15]_i_1 
       (.I0(state[2]),
        .I1(in6[15]),
        .O(dout0_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[16]_i_1 
       (.I0(state[2]),
        .I1(in6[16]),
        .O(dout0_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[17]_i_1 
       (.I0(state[2]),
        .I1(in6[17]),
        .O(dout0_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[18]_i_1 
       (.I0(state[2]),
        .I1(in6[18]),
        .O(dout0_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[19]_i_1 
       (.I0(state[2]),
        .I1(in6[19]),
        .O(dout0_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[1]_i_1 
       (.I0(state[2]),
        .I1(in6[1]),
        .O(dout0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[20]_i_1 
       (.I0(state[2]),
        .I1(in6[20]),
        .O(dout0_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[21]_i_1 
       (.I0(state[2]),
        .I1(in6[21]),
        .O(dout0_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[22]_i_1 
       (.I0(state[2]),
        .I1(in6[22]),
        .O(dout0_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[23]_i_1 
       (.I0(state[2]),
        .I1(in6[23]),
        .O(dout0_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[24]_i_1 
       (.I0(state[2]),
        .I1(in6[24]),
        .O(dout0_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[25]_i_1 
       (.I0(state[2]),
        .I1(in6[25]),
        .O(dout0_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[26]_i_1 
       (.I0(state[2]),
        .I1(in6[26]),
        .O(dout0_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[27]_i_1 
       (.I0(state[2]),
        .I1(in6[27]),
        .O(dout0_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[28]_i_1 
       (.I0(state[2]),
        .I1(in6[28]),
        .O(dout0_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[29]_i_1 
       (.I0(state[2]),
        .I1(in6[29]),
        .O(dout0_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[2]_i_1 
       (.I0(state[2]),
        .I1(in6[2]),
        .O(dout0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[30]_i_1 
       (.I0(state[2]),
        .I1(in6[30]),
        .O(dout0_in[30]));
  LUT3 #(
    .INIT(8'h41)) 
    \dout[31]_i_1 
       (.I0(state[1]),
        .I1(state[2]),
        .I2(state[0]),
        .O(\dout[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[31]_i_2 
       (.I0(state[2]),
        .I1(in6[31]),
        .O(dout0_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[3]_i_1 
       (.I0(state[2]),
        .I1(in6[3]),
        .O(dout0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[4]_i_1 
       (.I0(state[2]),
        .I1(in6[4]),
        .O(dout0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[5]_i_1 
       (.I0(state[2]),
        .I1(in6[5]),
        .O(dout0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[6]_i_1 
       (.I0(state[2]),
        .I1(in6[6]),
        .O(dout0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[7]_i_1 
       (.I0(state[2]),
        .I1(in6[7]),
        .O(dout0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[8]_i_1 
       (.I0(state[2]),
        .I1(in6[8]),
        .O(dout0_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \dout[9]_i_1 
       (.I0(state[2]),
        .I1(in6[9]),
        .O(dout0_in[9]));
  FDCE \dout_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[0]),
        .Q(dout[0]));
  FDCE \dout_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[10]),
        .Q(dout[10]));
  FDCE \dout_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[11]),
        .Q(dout[11]));
  FDCE \dout_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[12]),
        .Q(dout[12]));
  FDCE \dout_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[13]),
        .Q(dout[13]));
  FDCE \dout_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[14]),
        .Q(dout[14]));
  FDCE \dout_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[15]),
        .Q(dout[15]));
  FDCE \dout_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[16]),
        .Q(dout[16]));
  FDCE \dout_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[17]),
        .Q(dout[17]));
  FDCE \dout_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[18]),
        .Q(dout[18]));
  FDCE \dout_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[19]),
        .Q(dout[19]));
  FDCE \dout_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[1]),
        .Q(dout[1]));
  FDCE \dout_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[20]),
        .Q(dout[20]));
  FDCE \dout_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[21]),
        .Q(dout[21]));
  FDCE \dout_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[22]),
        .Q(dout[22]));
  FDCE \dout_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[23]),
        .Q(dout[23]));
  FDCE \dout_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[24]),
        .Q(dout[24]));
  FDCE \dout_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[25]),
        .Q(dout[25]));
  FDCE \dout_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[26]),
        .Q(dout[26]));
  FDCE \dout_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[27]),
        .Q(dout[27]));
  FDCE \dout_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[28]),
        .Q(dout[28]));
  FDCE \dout_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[29]),
        .Q(dout[29]));
  FDCE \dout_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[2]),
        .Q(dout[2]));
  FDCE \dout_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[30]),
        .Q(dout[30]));
  FDCE \dout_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[31]),
        .Q(dout[31]));
  FDCE \dout_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[3]),
        .Q(dout[3]));
  FDCE \dout_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[4]),
        .Q(dout[4]));
  FDCE \dout_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[5]),
        .Q(dout[5]));
  FDCE \dout_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[6]),
        .Q(dout[6]));
  FDCE \dout_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[7]),
        .Q(dout[7]));
  FDCE \dout_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[8]),
        .Q(dout[8]));
  FDCE \dout_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\dout[31]_i_1_n_0 ),
        .CLR(SR),
        .D(dout0_in[9]),
        .Q(dout[9]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFEAAAA)) 
    fin_i_1
       (.I0(src_fin),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(fin),
        .O(fin_i_1_n_0));
  FDCE fin_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(SR),
        .D(fin_i_1_n_0),
        .Q(fin));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[0]_i_1 
       (.I0(\my_value_reg[31]_0 [0]),
        .I1(state[0]),
        .O(my_value[0]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[10]_i_1 
       (.I0(\my_value_reg[31]_0 [10]),
        .I1(state[0]),
        .O(my_value[10]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[11]_i_1 
       (.I0(\my_value_reg[31]_0 [11]),
        .I1(state[0]),
        .O(my_value[11]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[12]_i_1 
       (.I0(\my_value_reg[31]_0 [12]),
        .I1(state[0]),
        .O(my_value[12]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[13]_i_1 
       (.I0(\my_value_reg[31]_0 [13]),
        .I1(state[0]),
        .O(my_value[13]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[14]_i_1 
       (.I0(\my_value_reg[31]_0 [14]),
        .I1(state[0]),
        .O(my_value[14]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[15]_i_1 
       (.I0(\my_value_reg[31]_0 [15]),
        .I1(state[0]),
        .O(my_value[15]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[16]_i_1 
       (.I0(\my_value_reg[31]_0 [16]),
        .I1(state[0]),
        .O(my_value[16]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[17]_i_1 
       (.I0(\my_value_reg[31]_0 [17]),
        .I1(state[0]),
        .O(my_value[17]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[18]_i_1 
       (.I0(\my_value_reg[31]_0 [18]),
        .I1(state[0]),
        .O(my_value[18]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[19]_i_1 
       (.I0(\my_value_reg[31]_0 [19]),
        .I1(state[0]),
        .O(my_value[19]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[1]_i_1 
       (.I0(\my_value_reg[31]_0 [1]),
        .I1(state[0]),
        .O(my_value[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[20]_i_1 
       (.I0(\my_value_reg[31]_0 [20]),
        .I1(state[0]),
        .O(my_value[20]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[21]_i_1 
       (.I0(\my_value_reg[31]_0 [21]),
        .I1(state[0]),
        .O(my_value[21]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[22]_i_1 
       (.I0(\my_value_reg[31]_0 [22]),
        .I1(state[0]),
        .O(my_value[22]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[23]_i_1 
       (.I0(\my_value_reg[31]_0 [23]),
        .I1(state[0]),
        .O(my_value[23]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[24]_i_1 
       (.I0(\my_value_reg[31]_0 [24]),
        .I1(state[0]),
        .O(my_value[24]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[25]_i_1 
       (.I0(\my_value_reg[31]_0 [25]),
        .I1(state[0]),
        .O(my_value[25]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[26]_i_1 
       (.I0(\my_value_reg[31]_0 [26]),
        .I1(state[0]),
        .O(my_value[26]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[27]_i_1 
       (.I0(\my_value_reg[31]_0 [27]),
        .I1(state[0]),
        .O(my_value[27]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[28]_i_1 
       (.I0(\my_value_reg[31]_0 [28]),
        .I1(state[0]),
        .O(my_value[28]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[29]_i_1 
       (.I0(\my_value_reg[31]_0 [29]),
        .I1(state[0]),
        .O(my_value[29]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[2]_i_1 
       (.I0(\my_value_reg[31]_0 [2]),
        .I1(state[0]),
        .O(my_value[2]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[30]_i_1 
       (.I0(\my_value_reg[31]_0 [30]),
        .I1(state[0]),
        .O(my_value[30]));
  LUT2 #(
    .INIT(4'h1)) 
    \my_value[31]_i_1 
       (.I0(state[1]),
        .I1(state[2]),
        .O(\my_value[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[31]_i_2 
       (.I0(\my_value_reg[31]_0 [31]),
        .I1(state[0]),
        .O(my_value[31]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[3]_i_1 
       (.I0(\my_value_reg[31]_0 [3]),
        .I1(state[0]),
        .O(my_value[3]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[4]_i_1 
       (.I0(\my_value_reg[31]_0 [4]),
        .I1(state[0]),
        .O(my_value[4]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[5]_i_1 
       (.I0(\my_value_reg[31]_0 [5]),
        .I1(state[0]),
        .O(my_value[5]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[6]_i_1 
       (.I0(\my_value_reg[31]_0 [6]),
        .I1(state[0]),
        .O(my_value[6]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[7]_i_1 
       (.I0(\my_value_reg[31]_0 [7]),
        .I1(state[0]),
        .O(my_value[7]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[8]_i_1 
       (.I0(\my_value_reg[31]_0 [8]),
        .I1(state[0]),
        .O(my_value[8]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \my_value[9]_i_1 
       (.I0(\my_value_reg[31]_0 [9]),
        .I1(state[0]),
        .O(my_value[9]));
  FDCE \my_value_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[0]),
        .Q(\my_value_reg_n_0_[0] ));
  FDCE \my_value_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[10]),
        .Q(\my_value_reg_n_0_[10] ));
  FDCE \my_value_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[11]),
        .Q(\my_value_reg_n_0_[11] ));
  FDCE \my_value_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[12]),
        .Q(\my_value_reg_n_0_[12] ));
  FDCE \my_value_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[13]),
        .Q(\my_value_reg_n_0_[13] ));
  FDCE \my_value_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[14]),
        .Q(\my_value_reg_n_0_[14] ));
  FDCE \my_value_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[15]),
        .Q(\my_value_reg_n_0_[15] ));
  FDCE \my_value_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[16]),
        .Q(\my_value_reg_n_0_[16] ));
  FDCE \my_value_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[17]),
        .Q(\my_value_reg_n_0_[17] ));
  FDCE \my_value_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[18]),
        .Q(\my_value_reg_n_0_[18] ));
  FDCE \my_value_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[19]),
        .Q(\my_value_reg_n_0_[19] ));
  FDCE \my_value_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[1]),
        .Q(\my_value_reg_n_0_[1] ));
  FDCE \my_value_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[20]),
        .Q(\my_value_reg_n_0_[20] ));
  FDCE \my_value_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[21]),
        .Q(\my_value_reg_n_0_[21] ));
  FDCE \my_value_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[22]),
        .Q(\my_value_reg_n_0_[22] ));
  FDCE \my_value_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[23]),
        .Q(\my_value_reg_n_0_[23] ));
  FDCE \my_value_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[24]),
        .Q(\my_value_reg_n_0_[24] ));
  FDCE \my_value_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[25]),
        .Q(\my_value_reg_n_0_[25] ));
  FDCE \my_value_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[26]),
        .Q(\my_value_reg_n_0_[26] ));
  FDCE \my_value_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[27]),
        .Q(\my_value_reg_n_0_[27] ));
  FDCE \my_value_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[28]),
        .Q(\my_value_reg_n_0_[28] ));
  FDCE \my_value_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[29]),
        .Q(\my_value_reg_n_0_[29] ));
  FDCE \my_value_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[2]),
        .Q(\my_value_reg_n_0_[2] ));
  FDCE \my_value_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[30]),
        .Q(\my_value_reg_n_0_[30] ));
  FDCE \my_value_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[31]),
        .Q(\my_value_reg_n_0_[31] ));
  FDCE \my_value_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[3]),
        .Q(\my_value_reg_n_0_[3] ));
  FDCE \my_value_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[4]),
        .Q(\my_value_reg_n_0_[4] ));
  FDCE \my_value_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[5]),
        .Q(\my_value_reg_n_0_[5] ));
  FDCE \my_value_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[6]),
        .Q(\my_value_reg_n_0_[6] ));
  FDCE \my_value_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[7]),
        .Q(\my_value_reg_n_0_[7] ));
  FDCE \my_value_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[8]),
        .Q(\my_value_reg_n_0_[8] ));
  FDCE \my_value_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\my_value[31]_i_1_n_0 ),
        .CLR(SR),
        .D(my_value[9]),
        .Q(\my_value_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[0]_i_1 
       (.I0(dst_init),
        .I1(dst_data[0]),
        .O(\others_value[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[10]_i_1 
       (.I0(dst_init),
        .I1(dst_data[10]),
        .O(\others_value[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[11]_i_1 
       (.I0(dst_init),
        .I1(dst_data[11]),
        .O(\others_value[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[12]_i_1 
       (.I0(dst_init),
        .I1(dst_data[12]),
        .O(\others_value[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[13]_i_1 
       (.I0(dst_init),
        .I1(dst_data[13]),
        .O(\others_value[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[14]_i_1 
       (.I0(dst_init),
        .I1(dst_data[14]),
        .O(\others_value[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[15]_i_1 
       (.I0(dst_init),
        .I1(dst_data[15]),
        .O(\others_value[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[16]_i_1 
       (.I0(dst_init),
        .I1(dst_data[16]),
        .O(\others_value[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[17]_i_1 
       (.I0(dst_init),
        .I1(dst_data[17]),
        .O(\others_value[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[18]_i_1 
       (.I0(dst_init),
        .I1(dst_data[18]),
        .O(\others_value[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[19]_i_1 
       (.I0(dst_init),
        .I1(dst_data[19]),
        .O(\others_value[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[1]_i_1 
       (.I0(dst_init),
        .I1(dst_data[1]),
        .O(\others_value[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[20]_i_1 
       (.I0(dst_init),
        .I1(dst_data[20]),
        .O(\others_value[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[21]_i_1 
       (.I0(dst_init),
        .I1(dst_data[21]),
        .O(\others_value[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[22]_i_1 
       (.I0(dst_init),
        .I1(dst_data[22]),
        .O(\others_value[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[23]_i_1 
       (.I0(dst_init),
        .I1(dst_data[23]),
        .O(\others_value[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[24]_i_1 
       (.I0(dst_init),
        .I1(dst_data[24]),
        .O(\others_value[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[25]_i_1 
       (.I0(dst_init),
        .I1(dst_data[25]),
        .O(\others_value[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[26]_i_1 
       (.I0(dst_init),
        .I1(dst_data[26]),
        .O(\others_value[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[27]_i_1 
       (.I0(dst_init),
        .I1(dst_data[27]),
        .O(\others_value[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[28]_i_1 
       (.I0(dst_init),
        .I1(dst_data[28]),
        .O(\others_value[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[29]_i_1 
       (.I0(dst_init),
        .I1(dst_data[29]),
        .O(\others_value[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[2]_i_1 
       (.I0(dst_init),
        .I1(dst_data[2]),
        .O(\others_value[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[30]_i_1 
       (.I0(dst_init),
        .I1(dst_data[30]),
        .O(\others_value[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[31]_i_1 
       (.I0(dst_init),
        .I1(dst_data[31]),
        .O(\others_value[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[3]_i_1 
       (.I0(dst_init),
        .I1(dst_data[3]),
        .O(\others_value[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[4]_i_1 
       (.I0(dst_init),
        .I1(dst_data[4]),
        .O(\others_value[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[5]_i_1 
       (.I0(dst_init),
        .I1(dst_data[5]),
        .O(\others_value[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[6]_i_1 
       (.I0(dst_init),
        .I1(dst_data[6]),
        .O(\others_value[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[7]_i_1 
       (.I0(dst_init),
        .I1(dst_data[7]),
        .O(\others_value[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[8]_i_1 
       (.I0(dst_init),
        .I1(dst_data[8]),
        .O(\others_value[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \others_value[9]_i_1 
       (.I0(dst_init),
        .I1(dst_data[9]),
        .O(\others_value[9]_i_1_n_0 ));
  FDCE \others_value_reg[0] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[0]_i_1_n_0 ),
        .Q(others_value[0]));
  FDCE \others_value_reg[10] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[10]_i_1_n_0 ),
        .Q(others_value[10]));
  FDCE \others_value_reg[11] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[11]_i_1_n_0 ),
        .Q(others_value[11]));
  FDCE \others_value_reg[12] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[12]_i_1_n_0 ),
        .Q(others_value[12]));
  FDCE \others_value_reg[13] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[13]_i_1_n_0 ),
        .Q(others_value[13]));
  FDCE \others_value_reg[14] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[14]_i_1_n_0 ),
        .Q(others_value[14]));
  FDCE \others_value_reg[15] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[15]_i_1_n_0 ),
        .Q(others_value[15]));
  FDCE \others_value_reg[16] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[16]_i_1_n_0 ),
        .Q(others_value[16]));
  FDCE \others_value_reg[17] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[17]_i_1_n_0 ),
        .Q(others_value[17]));
  FDCE \others_value_reg[18] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[18]_i_1_n_0 ),
        .Q(others_value[18]));
  FDCE \others_value_reg[19] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[19]_i_1_n_0 ),
        .Q(others_value[19]));
  FDCE \others_value_reg[1] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[1]_i_1_n_0 ),
        .Q(others_value[1]));
  FDCE \others_value_reg[20] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[20]_i_1_n_0 ),
        .Q(others_value[20]));
  FDCE \others_value_reg[21] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[21]_i_1_n_0 ),
        .Q(others_value[21]));
  FDCE \others_value_reg[22] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[22]_i_1_n_0 ),
        .Q(others_value[22]));
  FDCE \others_value_reg[23] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[23]_i_1_n_0 ),
        .Q(others_value[23]));
  FDCE \others_value_reg[24] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[24]_i_1_n_0 ),
        .Q(others_value[24]));
  FDCE \others_value_reg[25] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[25]_i_1_n_0 ),
        .Q(others_value[25]));
  FDCE \others_value_reg[26] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[26]_i_1_n_0 ),
        .Q(others_value[26]));
  FDCE \others_value_reg[27] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[27]_i_1_n_0 ),
        .Q(others_value[27]));
  FDCE \others_value_reg[28] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[28]_i_1_n_0 ),
        .Q(others_value[28]));
  FDCE \others_value_reg[29] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[29]_i_1_n_0 ),
        .Q(others_value[29]));
  FDCE \others_value_reg[2] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[2]_i_1_n_0 ),
        .Q(others_value[2]));
  FDCE \others_value_reg[30] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[30]_i_1_n_0 ),
        .Q(others_value[30]));
  FDCE \others_value_reg[31] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[31]_i_1_n_0 ),
        .Q(others_value[31]));
  FDCE \others_value_reg[3] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[3]_i_1_n_0 ),
        .Q(others_value[3]));
  FDCE \others_value_reg[4] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[4]_i_1_n_0 ),
        .Q(others_value[4]));
  FDCE \others_value_reg[5] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[5]_i_1_n_0 ),
        .Q(others_value[5]));
  FDCE \others_value_reg[6] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[6]_i_1_n_0 ),
        .Q(others_value[6]));
  FDCE \others_value_reg[7] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[7]_i_1_n_0 ),
        .Q(others_value[7]));
  FDCE \others_value_reg[8] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[8]_i_1_n_0 ),
        .Q(others_value[8]));
  FDCE \others_value_reg[9] 
       (.C(s00_axi_aclk),
        .CE(DST_ACK_i_1_n_0),
        .CLR(SR),
        .D(\others_value[9]_i_1_n_0 ),
        .Q(others_value[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
