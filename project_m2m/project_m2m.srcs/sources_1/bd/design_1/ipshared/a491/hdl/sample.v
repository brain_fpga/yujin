`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/01/17 17:53:49
// Design Name: 
// Module Name: sample
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sample #
    (
    parameter DATA_WIDTH = 32,
    // Width of S_AXI data bus
	parameter integer C_S_AXI_DATA_WIDTH	= 32
    )
    (
        input wire clk,
        input wire rstn,

        input wire DST_INIT,
		//output reg DST_INIT_ACK,
		input wire [DATA_WIDTH - 1:0] DST_DATA,
		output reg DST_ACK,
		input wire DST_FIN,
		
		output reg SRC_INIT,
		//input wire SRC_INIT_ACK,
		output reg [DATA_WIDTH - 1:0] SRC_DATA,
		input wire SRC_ACK,
		output reg SRC_FIN,
		
		input wire start,
		input wire [C_S_AXI_DATA_WIDTH - 1:0] din,
		output reg done,
		output reg [C_S_AXI_DATA_WIDTH - 1:0] dout,
		
		output wire [C_S_AXI_DATA_WIDTH - 1:0] debug_myval,
		output wire [C_S_AXI_DATA_WIDTH - 1:0] debug_others,
		output wire [C_S_AXI_DATA_WIDTH - 1:0] debug_state
    );
    
    localparam
        CORE_NUM = 2,
        
        STATE_IDLE = 4'd0,
        STATE_STORE = 4'd1,
        STATE_SEND = 4'd2,
        STATE_ACK_WAIT = 4'd3,
        STATE_FIN = 4'd4,
        STATE_CAL = 4'd5,
        STATE_DONE = 4'd6;
    
    
    reg [3:0] state;
    reg ack; // width should be modified if we connect more cores
    reg fin; // width should be modified if we connect more cores
    wire ack_all = &ack;
    wire fin_all = &fin;
    reg [DATA_WIDTH - 1:0] my_value;
    reg [(CORE_NUM - 1) * DATA_WIDTH - 1:0] others_value;
    
    assign debug_myval = my_value;
    assign debug_others = others_value;
    assign debug_state = {28'b0, state};
    
    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin
            state <= STATE_IDLE;
        end
        else begin
            case(state) 
                STATE_IDLE: begin
                    if(start) state <= STATE_STORE;
                end
                STATE_STORE: begin
                    state <= STATE_SEND;
                end
                STATE_SEND: begin
                    state <= STATE_ACK_WAIT;
                end
                STATE_ACK_WAIT: begin
                    if(ack) state <= STATE_FIN; 
                end
                STATE_FIN: begin
                    if(fin) state <= STATE_CAL;
                end
                STATE_CAL: begin
                    state <= STATE_DONE;
                end
                STATE_DONE: begin
                
                end
            endcase
        end
    end
    
    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin
            //DST_INIT_ACK <= 1'b0;
            DST_ACK <= 1'b0;
            SRC_INIT <= 1'b0;
            SRC_DATA <= {DATA_WIDTH{1'b0}};
            SRC_FIN <= 1'b0;
            done <= 1'b0;
            dout <= {DATA_WIDTH{1'b0}};
            my_value <= {DATA_WIDTH{1'b0}};
            others_value <= {((CORE_NUM - 1) * DATA_WIDTH){1'b0}};
            ack <= 1'b0;
            fin <= 1'b0;
        end
        else begin
            case(state) 
                STATE_IDLE: begin
                    //DST_INIT_ACK <= 1'b0;
                    DST_ACK <= 1'b0;
                    SRC_INIT <= 1'b0;
                    SRC_DATA <= {DATA_WIDTH{1'b0}};
                    SRC_FIN <= 1'b0;
                    done <= 1'b0;
                    dout <= {DATA_WIDTH{1'b0}};
                    my_value <= {DATA_WIDTH{1'b0}};
                    others_value <= {((CORE_NUM - 1) * DATA_WIDTH){1'b0}};
                    ack <= 1'b0;
                    fin <= 1'b0;
                end
                STATE_STORE: begin
                    my_value <= din;
                end
                STATE_SEND: begin
                    SRC_INIT <= 1'b1;
                    SRC_DATA <= my_value;
                end
                STATE_ACK_WAIT: begin
                    SRC_INIT <= 1'b0;
                    if(SRC_ACK) begin
                        ack <= 1'b1;
                    end
                end
                STATE_FIN: begin
                    SRC_FIN <= 1'b1;
                end
                STATE_CAL: begin
                    dout <= my_value + others_value;
                end
                STATE_DONE: begin
                    done <= 1'b1;
                end
            endcase
            
            // actions that are independent of state
            if(DST_INIT) begin // each if statement should exist for each interacting core
                others_value[DATA_WIDTH - 1:0] <= DST_DATA;
                DST_ACK <= 1'b1; // should be turned off or not?
            end
        
            if(DST_FIN) begin // if many cores, OR function would be possible (accept existing 1s and new 1s)
                fin <= 1'b1;
            end
        end
    end    
    
endmodule
