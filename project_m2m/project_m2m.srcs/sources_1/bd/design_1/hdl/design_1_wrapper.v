//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
//Date        : Mon Jan 20 14:51:10 2020
//Host        : DESKTOP-CIJA4NS running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (default_300mhz_clk0_clk_n,
    default_300mhz_clk0_clk_p,
    resetn,
    rs232_uart_rxd,
    rs232_uart_txd);
  input default_300mhz_clk0_clk_n;
  input default_300mhz_clk0_clk_p;
  input resetn;
  input rs232_uart_rxd;
  output rs232_uart_txd;

  wire default_300mhz_clk0_clk_n;
  wire default_300mhz_clk0_clk_p;
  wire resetn;
  wire rs232_uart_rxd;
  wire rs232_uart_txd;

  design_1 design_1_i
       (.default_300mhz_clk0_clk_n(default_300mhz_clk0_clk_n),
        .default_300mhz_clk0_clk_p(default_300mhz_clk0_clk_p),
        .resetn(resetn),
        .rs232_uart_rxd(rs232_uart_rxd),
        .rs232_uart_txd(rs232_uart_txd));
endmodule
